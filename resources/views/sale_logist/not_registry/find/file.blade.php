@component('modal')
    @slot('name', 'request-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'sale_logist.not_registry.find.file')
    @slot('enctype', 'enctype=multipart/form-data')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_request_accepted" name="id_request_accepted">
        <input type="hidden"  id="shipping" name="shipping">


        <div class="form-group">
            <label for="file_proxy">Файл доверенности</label>
            <input required id="file_proxy" class="form-control" name="file_proxy[]" multiple type="file">
        </div>

        <div class="form-group">
            <label for="file_vet">Файл ветеринарки</label>
            <input required id="file_vet" class="form-control" name="file_vet[]" multiple type="file">
        </div>

        <div class="text-center">
            <label for="check">Документы не нужны</label>
            <input id="check" name="check" type="checkbox">
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $("#check").change(function() {
                    if($(this).is(":checked")) {
                        $("#file_proxy").prop('required',false);
                        $("#file_vet").prop('required',false);
                    }
                    else {
                        $("#file_proxy").prop('required',true);
                        $("#file_vet").prop('required',true);
                    }
                });
            });
        })(jQuery);
    </script>
@endpush