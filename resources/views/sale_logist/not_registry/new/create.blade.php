@component('modal')
    @slot('name', 'request-sale_logist-modal')
    @slot('title', 'Новый запрос')
    @slot('route', 'sale_logist.not_registry.new.create')
    @slot('enctype', '')

        {!! csrf_field() !!}
        <div class="form-group">
            <label for="point_A">Точка А</label>
            <input maxlength="255" id="point_A" required class="form-control" name="point_A" type="text" placeholder="Введите адрес">
        </div>

        <div class="form-group">
            <label for="point_B">Точка Б</label>
            <input maxlength="255" id="point_B" required class="form-control" name="point_B" type="text" placeholder="Введите адрес">
        </div>

        <div class="form-group">
            <label for="farm_A">Название хоз-ва А</label>
            <input maxlength="255" id="farm_A" required class="form-control" name="farm_A" type="text" placeholder="Введите наименование организации">
        </div>

        <div class="form-group">
            <label for="farm_B">Название хоз-ва Б</label>
            <input maxlength="255" id="farm_B" required class="form-control" name="farm_B" type="text" placeholder="Введите наименование организации">
        </div>

        <div class="form-group">
            <label for="tonnage">Сколько кг везти</label>
            <input id="tonnage" required class="form-control" name="tonnage" type="number" placeholder="Введите сколько кг везти">
        </div>

        <div class="form-group">
            <label for="adr">ADR</label>
            <select id="adr" class="form-control" name="adr">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
            </select>
        </div>

        <div class="form-group">
            <label for="time_out">Планируемая дата вывоза</label>
            <input id="time_out" required class="form-control" name="time_out" type="datetime" placeholder="Выберите время вывоза">
        </div>

        <p>Поддоны</p>
        <label class="radio-inline"><input type="radio" name="pallet" value="1" checked>Да   </label>
        <label class="radio-inline"><input type="radio" name="pallet" value="0">Нет</label>

        <div id="add_field_area">
            <div id="add1" class="add">
                <div class="form-group">
                    <label for="product1">Наименование товара</label>
                    <input maxlength="100" id="product1" required class="form-control" type="text" name="product[]" placeholder="Введите товар">
                </div>
            </div>
        </div>
        <button id="newProduct" class="btn btn-light">Добавить еще один товар</button><br>



        <div class="form-group">
            <label for="package">Упаковка</label>
            <select id="package" class="form-control" name="package">
                @foreach ($package as $pack)
                    <option value="{{ $pack->id }}">{{ $pack->name }}</option>
                @endforeach;
            </select>
        </div>

        <div class="form-group">
            <label for="type_load">Тип загрузки</label>
            <select id="type_load" class="form-control" name="type_load">
                @foreach ($type_load as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endforeach;
            </select>
        </div>

        <div class="form-group">
            <label for="shipping">Тип перевозки</label>
            <select id="shipping" class="form-control" name="shipping">
                @foreach ($shipping as $ship)
                    <option value="{{ $ship->id }}">{{ $ship->name }}</option>
                @endforeach;
            </select>
        </div>

        <div class="form-group">
            <label for="comment">Комментарии</label>
            <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500"></textarea>
        </div>

        <div id="block_search">
            <div class="text-center">
                <label for="search">Сразу в "Поиск машины"</label>
                <input id="search" name="search" type="checkbox">
            </div>
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить запрос</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $("#point_A").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "ADDRESS",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#point_B").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "ADDRESS",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#farm_A").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "PARTY",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#farm_B").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "PARTY",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $.datetimepicker.setLocale('ru');
                $('#time_out').datetimepicker({
                    format:'Y-m-d H:i:s',
                });

                $('#newProduct').on('click', function(e) {
                    e.preventDefault();
                    var num_field = parseInt($("#add_field_area").find("div.add:last").attr("id").slice(3))+1;
                    $("div#add_field_area").append('<div id="add'+num_field+'" class="add" style="outline: 1px dotted #000;"> <p title="Удалить товар" style="cursor: pointer;" id="deleteProduct" class="add'+num_field+'">&#10007;</p>'+
                        '<div class="form-group"><label for="product'+num_field+'">Наименование товара</label><input maxlength="100" required id="product'+num_field+'" class="form-control" type="text" name="product[]" placeholder="Введите товар"></div>'+
                        '</div>');
                });

                $('div#add_field_area').on('click', 'p#deleteProduct',function() {
                    var id_del = $(this).attr("class");
                    $("div#"+id_del).remove();
                });

                $("#search").change(function() {
                    if($(this).is(":checked")) {
                        $("div#block_search").append('<div id="fields_search">\n' +
                            '<div class="form-group">\n' +
                            '<label for="price">Цена</label>\n' +
                            '<input id="price" type="number" class="form-control" name="price" placeholder="Введите цену" required autofocus>\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="distance">Километраж</label>\n' +
                            '<input id="distance" required class="form-control" name="distance" type="number">\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="fio_shipper">Грузоотправитель</label>\n' +
                            '<input maxlength="100" id="fio_shipper" required class="form-control" name="fio_shipper" type="text" placeholder="Введите ФИО представителя грузоотправителя">\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="phone_shipper">Контакты грузоотправителя</label>\n' +
                            '<input maxlength="255" id="phone_shipper" required class="form-control" name="phone_shipper" type="text" placeholder="Введите телефон представителя грузоотправителя">\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="comment_shipper">Комментарии по грузоотправителю</label>\n' +
                            '<textarea rows="3" id="comment_shipper" class="form-control" name="comment_shipper" placeholder="Комментарии по грузоотправителю"  maxlength="500"></textarea>\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="fio_consignee">Грузополучатель</label>\n' +
                            '<input maxlength="100" id="fio_consignee" required class="form-control" name="fio_consignee" type="text" placeholder="Введите ФИО представителя грузополучателя">\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="phone_consignee">Контакты грузополучателя</label>\n' +
                            '<input maxlength="255" id="phone_consignee" required class="form-control" name="phone_consignee" type="text" placeholder="Введите телефон представителя грузополучателя">\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                            '<label for="comment_consignee">Комментарии по грузополучателю</label>\n' +
                            '<textarea rows="3" id="comment_consignee" class="form-control" name="comment_consignee" placeholder="Комментарии по грузоотправителю"  maxlength="500"></textarea>\n' +
                            '</div>\n' +
                            '<div id="num_one_s">\n' +
                            '</div>\n'+
                            '</div>');
                        $('#time_fact').datetimepicker({
                            format:'Y-m-d H:i:s',
                        });
                        $("div#num_one_s").html('');
                        if ( $("#shipping").val() == 1) {
                            $("div#num_one_s").append('<div class="form-group">\n' +
                                '<label for="num_provider">№ заказа в 1С поставщика</label>\n' +
                                '<input maxlength="50" id="num_provider"  class="form-control" name="num_provider" type="text" placeholder="Введите номер заказа в 1С">\n' +
                                '</div>\n'+
                                '<div class="form-group">\n' +
                                '<label for="num_customer">№ заказа в 1С клиента</label>\n' +
                                '<input maxlength="50" id="num_customer"  class="form-control" name="num_customer" type="text" placeholder="Введите номер заказа в 1С">\n' +
                                '</div>');
                        }
                        if ( $("#shipping").val() == 2) {
                            $("div#num_one_s").append('<div class="form-group">\n' +
                                '<label for="num_provider">№ заказа в 1С поставщика</label>\n' +
                                '<input maxlength="50" id="num_provider"  class="form-control" name="num_provider" type="text" placeholder="Введите номер заказа в 1С">\n' +
                                '</div>\n');
                        }
                        if ( $("#shipping").val() == 3) {
                            $("div#num_one_s").append('<div class="form-group">\n' +
                                '<label for="num_customer">№ заказа в 1С клиента</label>\n' +
                                '<input maxlength="50" id="num_customer"  class="form-control" name="num_customer" type="text" placeholder="Введите номер заказа в 1С">\n' +
                                '</div>');
                        }
                    }
                    else {
                        $("#fields_search").detach();
                    }
                });

                $("div").on("change", '#shipping', function() {
                    $("div#num_one_s").html('');
                    if ( $("#shipping").val() == 1) {
                        $("div#num_one_s").append('<div class="form-group">\n' +
                            '<label for="num_provider">№ заказа в 1С поставщика</label>\n' +
                            '<input maxlength="50" id="num_provider" class="form-control" name="num_provider" type="text" placeholder="Введите номер заказа в 1С">\n' +
                            '</div>\n'+
                            '<div class="form-group">\n' +
                            '<label for="num_customer">№ заказа в 1С клиента</label>\n' +
                            '<input maxlength="50" id="num_customer" class="form-control" name="num_customer" type="text" placeholder="Введите номер заказа в 1С">\n' +
                            '</div>');
                    }
                    if ( $("#shipping").val() == 2) {
                        $("div#num_one_s").append('<div class="form-group">\n' +
                            '<label for="num_provider">№ заказа в 1С поставщика</label>\n' +
                            '<input maxlength="50" id="num_provider" class="form-control" name="num_provider" type="text" placeholder="Введите номер заказа в 1С">\n' +
                            '</div>\n');
                    }
                    if ( $("#shipping").val() == 3) {
                        $("div#num_one_s").append('<div class="form-group">\n' +
                            '<label for="num_customer">№ заказа в 1С клиента</label>\n' +
                            '<input maxlength="50" id="num_customer" class="form-control" name="num_customer" type="text" placeholder="Введите номер заказа в 1С">\n' +
                            '</div>');
                    }
                });
            });
        })(jQuery);
    </script>
@endpush