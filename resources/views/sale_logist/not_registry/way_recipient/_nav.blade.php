<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.new.index') }}">Ждет ответа</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.answer.index') }}">Ответ получен</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.take.index') }}">Везем</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.search.index') }}">Поиск машины</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.find.index') }}">Машина найдена</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.way_sender.index') }}">В пути к отправителю</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('sale_logist.not_registry.way_recipient.index') }}">В пути к получателю</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.in_recipient.index') }}">У получателя</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.archive_realized.index') }}">Реализованные запросы</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.archive_unrealized.index') }}">Нереализованные запросы</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale_logist.not_registry.problem_request.index') }}">Проблемные запросы</a></li>
</ul>