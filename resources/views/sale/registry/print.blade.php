<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Print</title>
    <style type="text/css">
        table {
            width:100%;
            border: 1px solid black;
            border-collapse:collapse;
        }
        th {
            text-align: left; /* Выравнивание по левому краю */
            background: #D3D3D3; /* Цвет фона ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
        td {
            text-align: left; /* Выравнивание по левому краю */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
    </style>

</head>
<body onload="setTimeout(function () { window.print(); }, 500); window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }">
</body>
</html>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th rowspan="2">№</th>
        <th rowspan="2">Дата</th>
        <th rowspan="2">Менеджер ОП</th>
        <th rowspan="2">Срочность</th>
        <th rowspan="2">Хозяйство</th>
        <th rowspan="2">Тип доставки</th>
        <th rowspan="2">Комментарий от ОП</th>
        <th colspan="19">Продукты</th>
        @if ($type == "archive")
            <th rowspan="2">Результат</th>
            <th rowspan="2">Комментарий по результатам</th>
        @endif
        @if ($type == "answers" || $type == "archive")
            <th rowspan="2">Коммуникации</th>
        @endif
    </tr>
    <tr>
        <th>Продукт</th>
        <th>Задача</th>
        <th>Необходимая цена</th>
        <th>Примечание</th>
        <th>Мес. потребление</th>
        <th>Необх. объем</th>
        <th>Конкурент</th>
        <th>Его цена</th>
        <th>Вид доставки</th>
        <th>Условия</th>
        <th>Дополнительно</th>
        @if ($type == "answers" || $type == "archive")
            <th>Ответ</th>
            <th>Стоимость доставки</th>
            <th>Место забора</th>
            <th>Планируемая валовая прибыль</th>
            <th>Срок действия цены (дн.)</th>
            <th>Комментарий от ОЗ</th>
            <th>Дата ответа</th>
            <th>Менеджер ОЗ</th>
        @endif
    </tr>
    </thead>
    <tbody>

    @foreach ($requests as $req)
        @php($kol=count($req->registryProducts))
        @php($i=0)
        <tr>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->id }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->userCreate->name }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->urgency }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->shipping['name'] }}</td>
            <td rowspan="{{$kol==0 ? 1 : $kol}}">{{ $req->comment_sale }}</td>
            @if ($kol > 0)
                @foreach($req->registryProducts as $product)
                    <td>{{ $product['product']['name'] }}</td>
                    <td>{{ $product['type_request'] }}. {{ $product['type_discount'] }}</td>
                    <td>{{ $product['price_sale'] }}</td>
                    <td>{{ $product['product_comment'] }}</td>
                    <td>{{ $product['product_monthly_need'] }}</td>
                    <td>{{ $product['product_value'] }}</td>
                    <td>{{ $product['competitor']['name'] }}</td>
                    <td>{{ $product['competitor_price'] }}</td>
                    <td>{{ $product['competitor_delivery'] }}</td>
                    <td>{{ $product['competitor_payment'] }}</td>
                    <td>{{ $product['competitor_comment'] }}</td>
                    @if ($type == "answers" || $type == "archive")
                        <td>{{ $product['answer'] }}</td>
                        <td>{{ $product['price_purchase'] }}</td>
                        <td>{{ $product['place_take'] }}</td>
                        <td>{{ $product['gross_profit'] }}</td>
                        <td>{{ $product['time_active_price'] }}</td>
                        <td>{{ $product['comment_purchase'] }}</td>
                        <td>@if (!empty($product['date_answer'])){{ date("d.m.y H:i:s", strtotime($product['date_answer'])) }} @endif</td>
                        <td>{{ $product['userPurchase']['name'] }}</td>
                        @if($i==0)
                            @if($type == "archive")
                                <td rowspan="{{$kol}}">{{ $req->result_sale }}</td>
                                <td rowspan="{{$kol}}">{{ $req->comment_sale_result }}</td>
                            @endif
                            <td rowspan="{{$kol}}">
                                @foreach ($req->registryCommunication as $message)
                                    <b>{{$message->user->name}}</b>: {{$message->message}} <i>({{$message->created_at}})</i><hr>
                                @endforeach
                            </td>
                        @endif
                    @endif
                    </tr>
                    @php($i++)
                @endforeach
            @else
                @if ($type == "new")
                    @for($i=0; $i<11; $i++)
                        <td>-</td>
                    @endfor
                @else
                    @for($i=0; $i<19; $i++)
                        <td>-</td>
                    @endfor
                    @if($type == "archive")
                        <td rowspan="{{$kol}}">{{ $req->result_sale }}</td>
                        <td rowspan="{{$kol}}">{{ $req->comment_sale_result }}</td>
                    @endif
                    <td rowspan="1">
                        @foreach ($req->registryCommunication as $message)
                        <b>{{$message->user->name}}</b>: {{$message->message}} <i>({{$message->created_at}})</i><hr>
                        @endforeach
                    </td>
                @endif
                </tr>
            @endif
        @endforeach
    </tbody>
</table>