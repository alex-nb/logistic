@component('modal')
    @slot('name', 'communicate-sale-modal')
    @slot('title', 'Оставить комментарий')
    @slot('route', 'sale.registry.answers.communicate')
    @slot('enctype', '')

        {!! csrf_field() !!}

    <input type="hidden"  id="id_request_comm" name="id_request_comm">

    <div class="form-group">
        <label for="comment">Комментарий</label>
        <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="250"></textarea>
    </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent