@component('modal')
    @slot('name', 'show_communicate-sale-modal')
    @slot('title', 'История сообщений')
    @slot('route', 'sale.registry.answers.index')
    @slot('enctype', '')
    <div id="show_message"></div>

    @slot('buttons')
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    @endslot
@endcomponent