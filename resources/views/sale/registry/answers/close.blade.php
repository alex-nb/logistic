@component('modal')
    @slot('name', 'close-sale-modal')
    @slot('title', 'Закрыть запрос')
    @slot('route', 'sale.registry.answers.close')
    @slot('enctype', '')

        {!! csrf_field() !!}

    <input type="hidden"  id="id_request_close" name="id_request_close">

    <div class="form-group">
        <label for="result_sale">Результат</label>
        <select id="result_sale" class="form-control" name="result_sale">
            <option>Сделка свершилась</option>
            <option>Сделка не свершилась</option>
        </select>
    </div>

    <div class="form-group">
        <label for="comment">Комментарий</label>
        <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="250"></textarea>
    </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Закрыть</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent