<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.registry.new.index') }}">Запросы</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.registry.answers.index') }}">Ответы на запросы</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('sale.registry.archive.index') }}">Архив</a></li>
</ul>