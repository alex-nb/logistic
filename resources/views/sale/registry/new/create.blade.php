@extends('layouts.app', ['navigator' => 'Отдел продаж'])
@section('menu')
    @include('sale.registry._vertical_nav')
@endsection
@section('content')

<form method="POST" id="form_modal" action="{{ route('sale.registry.new.create') }}" enctype="multipart/form-data">

    {!! csrf_field() !!}

    <input type="hidden"  id="id_request" name="id_request" value="{{$id}}">

    <div class="form-group">
        <label for="point_B">Адрес хозяйства</label>
        <input maxlength="255" id="point_B" required class="form-control" name="point_B" type="text" placeholder="Введите адрес"
        @if(!empty($request->farmsAddressB['address']))
            value="{{$request->farmsAddressB['address']}}"
        @endif
        >
    </div>

    <div class="form-group">
        <label for="farm_B">Название хозяйства</label>
        <input maxlength="255" id="farm_B" required class="form-control farms" name="farm_B" type="text" placeholder="Введите наименование организации"
        @if(!empty($request->farmsAddressB['name']))
            value="{{$request->farmsAddressB['name']}}"
        @endif
        >
    </div>

    <div class="form-group">
        <label for="urgency">Срочность: </label>
        <select class="form-control" id="urgency" name = "urgency">
            <option selected>В течение 15 минут</option>
            <option @if(isset($request->urgency) && $request->urgency == "В течение 30 минут") selected @endif>В течение 30 минут</option>
            <option @if(isset($request->urgency) && $request->urgency == "В течение 1 часа") selected @endif>В течение 1 часа</option>
            <option @if(isset($request->urgency) && $request->urgency == "В течение 1 дня") selected @endif>В течение 1 дня</option>
            <option @if(isset($request->urgency) && $request->urgency == "В течение 3 дней") selected @endif>В течение 3 дней</option>
        </select>
    </div>

    <div class="form-group">
        <label for="shipping">Тип перевозки</label>
        <select id="shipping" class="form-control" name="shipping">
            @foreach ($shipping as $ship)
                <option value="{{ $ship->id }}"  @if(isset($request->shipping['id']) && $request->shipping['id'] == $ship->id) selected @endif>{{ $ship->name }}</option>
            @endforeach;
        </select>
    </div>

    <div class="form-group">
        <label for="file_sale">Необходимые документы</label>
        <input id="file_sale" class="form-control" name="file_sale[]" type="file" multiple>
    </div>

    <div class="form-group">
        <label for="comment">Комментарии</label>
        <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500">@if(!empty($request->comment_sale)){{$request->comment_sale}}@endif</textarea>
    </div>


    <div id="add_field_area" style="background: #D8D8D8; padding: 5px; ">
        @if(!empty($request->registryProducts))
            @php($i=0)
            @foreach($request->registryProducts as $product)
                @php($i++)
                @if($i>1)
                    <div id="add{{$i}}" class="add" style="outline: 1px dotted #000;">
                        <p title="Удалить товар" style="cursor: pointer;" id="deleteProduct" class="add{{$i}}">&#10007;</p>
                @endif
                <div id="add{{$i}}" class="add">
                    <input type="hidden"  id="id_product{{$i}}" name="id_product[]" value="{{$product['id']}}">
                    <div class="form-group">
                        <label for="product{{$i}}">Наименование товара</label>
                        <input id="product{{$i}}" required maxlength="100" class="form-control" type="text" name="product[]" value="{{$product['product']['name']}}">
                    </div>
                    <div class="form-group">
                        <label for="deistvie{{$i}}">Действие</label>
                        <select class="form-control deistvie" id="deistvie{{$i}}" name = "deistvie[]">
                            <option selected>Запрос цены</option>
                            <option @if(isset($product['type_request']) && $product['type_request'] == "Необходимость скидки") selected @endif>Необходимость скидки</option>
                        </select>
                    </div>
                    <div id="skidochka{{$i}}" class="form-group" @if(empty($product['type_request']) || $product['type_request'] != "Необходимость скидки") style="display: none" @endif>
                        <label for="vid_skidki{{$i}}">Вид скидки</label>
                        <select class="form-control" id="vid_skidki{{$i}}" name = "vid_skidki[]">
                            <option>Вид скидки</option>
                            <option @if(isset($product['type_discount']) && $product['type_discount'] == "На лояльность") selected @endif>На лояльность</option>
                            <option @if(isset($product['type_discount']) && $product['type_discount'] == "Конкурент") selected @endif>Конкурент</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sale_price{{$i}}">Необходимая цена</label>
                        <input id="sale_price{{$i}}" class="form-control" type="text" name="sale_price[]" maxlength="25" placeholder="Введите цену"
                            @if(!empty($product['sale_price']))
                                value="{{$product['sale_price']}}"
                            @endif
                        >
                    </div>
                    <div class="form-group">
                        <label for="prim{{$i}}">Примечание</label>
                        <input maxlength="255" id="prim{{$i}}" class="form-control" type="text" name="prim[]" placeholder="Введите комментарий по товару"
                        @if(!empty($product['product_comment']))
                            value="{{$product['product_comment']}}"
                        @endif
                        >
                    </div>
                    <div class="form-group">
                        <label for="mounth{{$i}}">Мес. потребление</label>
                        <input maxlength="255" id="mounth{{$i}}" class="form-control" type="text" name="mounth[]" placeholder="Введите мес. потребление"
                       @if(!empty($product['product_monthly_need']))
                           value="{{$product['product_monthly_need']}}"
                        @endif
                        >
                    </div>
                    <div class="form-group">
                        <label for="need{{$i}}">Необх. объем</label>
                        <input onchange="check_need_value(this);" maxlength="255" id="need{{$i}}" required class="form-control" type="number" step="any" name="need[]" value="{{$product['product_value']}}">
                    </div>
                    <div class="form-group">
                        <label for="need_unit{{$i}}">Ед. измерения</label>
                        <select onchange="check_unit(this);" id="need_unit{{$i}}" class="form-control" name="need_unit[]">
                            @foreach ($units as $unit)
                                <option value="{{ $unit->id }}"  @if(isset($product['unit']) && $product['unit']['id'] == $unit->id) selected @endif>{{ $unit->name }}</option>
                            @endforeach;
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="conc{{$i}}">Конкурент</label>
                        <input maxlength="255" id="conc{{$i}}" class="form-control farms" type="text" name="conc[]" placeholder="Введите конкурента, если есть"
                       @if(!empty($product['competitor']['name']))
                            value="{{$product['competitor']['name']}}"
                        @endif
                        >
                    </div>
                    <div class="form-group">
                        <label for="price{{$i}}">Его цена</label>
                        <input maxlength="255" id="price{{$i}}" class="form-control" type="text" name="price[]" placeholder="Введите его цену"
                       @if(!empty($product['competitor_price']))
                           value="{{$product['competitor_price']}}"
                        @endif
                        >
                    </div>
                    <div class="form-group">
                        <label for="dostavka{{$i}}">Вид доставки</label>
                        <select class="form-control" id="dostavka{{$i}}" name = "dostavka[]">
                            <option>Вид доставки</option>
                            <option @if($product['competitor_delivery'] == "С доставкой") selected @endif>С доставкой</option>
                            <option @if($product['competitor_delivery'] == "Без доставки") selected @endif>Без доставки</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="uslov{{$i}}">Условия</label>
                        <select class="form-control" id="uslov{{$i}}" name = "uslov[]">
                            <option>Условия оплаты</option>
                            <option @if($product['competitor_payment'] == "По факту") selected @endif>По факту</option>
                            <option @if($product['competitor_payment'] == "Предоплата") selected @endif>Предоплата</option>
                            <option @if($product['competitor_payment'] == "Отсрочка") selected @endif>Отсрочка</option>
                            <option @if($product['competitor_payment'] == "Иное") selected @endif>Иное</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="commc{{$i}}">Комментарии по конкуренту</label>
                        <input maxlength="255" id="commc{{$i}}" class="form-control" type="text" name="commc[]" placeholder="Введите примечания по конкуренту"
                       @if(!empty($product['competitor_comment']))
                           value="{{$product['competitor_comment']}}"
                        @endif
                        >
                    </div>
                </div>
                @if($i>1)
                    </div>
                @endif
            @endforeach
        @else
            <div id="add1" class="add">
                <div class="form-group">
                    <label for="product1">Наименование товара</label>
                    <input id="product1" required maxlength="100" class="form-control" type="text" name="product[]" placeholder="Введите наименование">
                </div>
                <div class="form-group">
                    <label for="deistvie1">Действие</label>
                    <select class="form-control deistvie" id="deistvie1" name = "deistvie[]">
                        <option selected>Запрос цены</option>
                        <option>Необходимость скидки</option>
                    </select>
                </div>
                <div id="skidochka1" class="form-group" style="display: none">
                    <label for="vid_skidki1">Вид скидки</label>
                    <select class="form-control" id="vid_skidki1" name = "vid_skidki[]">
                        <option>Вид скидки</option>
                        <option>На лояльность</option>
                        <option>Конкурент</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sale_price1">Необходимая цена</label>
                    <input id="sale_price1" class="form-control" type="text" name="sale_price[]" maxlength="25" placeholder="Введите цену">
                </div>
                <div class="form-group">
                    <label for="prim1">Примечание</label>
                    <input maxlength="255" id="prim1" class="form-control" type="text" name="prim[]" placeholder="Введите комментарий по товару">
                </div>
                <div class="form-group">
                    <label for="mounth1">Мес. потребление</label>
                    <input maxlength="255" id="mounth1" class="form-control" type="text" name="mounth[]" placeholder="Введите мес. потребление">
                </div>
                <div class="form-group">
                    <label for="need1">Необх. объем</label>
                    <input onchange="check_need_value(this);" maxlength="255" id="need1" required class="form-control" type="number" step="any" name="need[]" placeholder="Введите необх. объем">
                </div>
                <div class="form-group">
                    <label for="need_unit1">Ед. измерения</label>
                    <select onchange="check_unit(this);" id="need_unit1" class="form-control" name="need_unit[]">
                        @foreach ($units as $unit)
                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                        @endforeach;
                    </select>
                </div>
                <div class="form-group">
                    <label for="conc1">Конкурент</label>
                    <input maxlength="255" id="conc1" class="form-control farms" type="text" name="conc[]" placeholder="Введите конкурента, если есть">
                </div>
                <div class="form-group">
                    <label for="price1">Его цена</label>
                    <input maxlength="255" id="price1" class="form-control" type="text" name="price[]" placeholder="Введите его цену">
                </div>
                <div class="form-group">
                    <label for="dostavka1">Вид доставки</label>
                    <select class="form-control" id="dostavka1" name = "dostavka[]">
                        <option>Вид доставки</option>
                        <option>С доставкой</option>
                        <option>Без доставки</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="uslov1">Условия</label>
                    <select class="form-control" id="uslov1" name = "uslov[]">
                        <option>Условия оплаты</option>
                        <option>По факту</option>
                        <option>Предоплата</option>
                        <option>Отсрочка</option>
                        <option>Иное</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="commc1">Комментарии по конкуренту</label>
                    <input id="commc1" class="form-control" type="text" name="commc[]" placeholder="Введите примечания по конкуренту">
                </div>
            </div>
        @endif
    </div>
    <p><button id="newProduct" class="btn btn-light">Добавить еще один товар</button></p><br>


    <button type="submit" class="btn btn-success" id="save">Отправить запрос</button>
    <a href="{{ route('sale.registry.new.index') }}" class="btn btn-danger mr-3">Отмена</a>

</form>

@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        
        function check_need_value(input) {
            var value = $(input).val();
            var id = $(input).attr("id").slice(4);
            var unit = $('#need_unit'+id).val();

            if (Number(value) > 20000 && unit==2) {
                if (id > 1) {
                    alert("Вы не можете добавить товар с весом больше 20 тонн. Такой товар оформляется отдельной заявкой.");
                    $(input).val(1);
                }
                else {
                    if($('#add2').length) {
                        alert("Вы не можете добавить товар с весом больше 20 тонн. Такой товар оформляется отдельной заявкой.");
                        $(input).val(1);
                    };
                }
            }
        }

        function check_unit(select) {
            var unit = $(select).val();
            var id = $(select).attr("id").slice(9);
            var value = $('#need'+id).val();

            if (Number(value) > 20000 && unit==2) {
                if (id > 1) {
                    alert("Вы не можете поменять ед. измерения на кг. Товар с весом больше 20 тонн оформляется отдельной заявкой.");
                    $(select).val(1);
                }
                else {
                    if($('#add2').length) {
                        alert("Вы не можете поменять ед. измерения на кг. Товар с весом больше 20 тонн оформляется отдельной заявкой.");
                        $(select).val(1);
                    };
                }
            }
        }
        
        (function( $ ) {
            $(function() {
                var units = <?=json_encode($units)?>;
                var options = '';
                $.each(units, function(key, value) {
                    options += '<option value="'+value.id+'">'+value.name+'</option>';
                });

                $("#point_B").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "ADDRESS",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $(".farms").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "PARTY",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });

                $('body').on('change', '.deistvie',function() {

                    var id_select = $(this).attr("id").slice(8);
                    console.log(id_select);
                    if ($(this).val() === "Необходимость скидки") {
                        $("#skidochka"+id_select).css("display", "block");
                    }
                    else {
                        $("#skidochka"+id_select).css("display", "none");
                    }
                });
                

                $('#newProduct').on('click', function(e) {
                    e.preventDefault();
                    var num_field = parseInt($("#add_field_area").find("div.add:last").attr("id").slice(3))+1;
                    var prev_field = num_field-1;
                    if(Number($('#need'+prev_field).val()) > 200000 && $('#need_unit'+prev_field).val() == 2) {
                        alert("У вас уже есть товар с весом больше 20 тонн. Такой товар оформляется отдельной заявкой.");
                    }
                    else {
                        $("div#add_field_area").append('<div id="add'+num_field+'" class="add" style="outline: 1px dotted #000;"> <p title="Удалить товар" style="cursor: pointer;" id="deleteProduct" class="add'+num_field+'">&#10007;</p>'+
                            '<div class="form-group"><label for="product'+num_field+'">Наименование товара</label><input required maxlength="100" id="product'+num_field+'" class="form-control" type="text" name="product[]" placeholder="Введите наименование"></div>' +
                            '<div class="form-group"><label for="deistvie'+num_field+'">Действие</label><select class="form-control deistvie" id="deistvie'+num_field+'" name = "deistvie[]">'+
                            '<option selected>Запрос цены</option><option>Необходимость скидки</option>'+
                            '</select></div>'+
                            '<div id="skidochka'+num_field+'" class="form-group" style="display: none"><label for="vid_skidki'+num_field+'">Вид скидки</label><select class="form-control" id="vid_skidki'+num_field+'" name = "vid_skidki[]">'+
                            '<option>Вид скидки</option><option>На лояльность</option><option selected>Конкурент</option>'+
                            '</select></div>'+
                            '<div class="form-group"><label for="sale_price'+num_field+'">Необходимая цена</label><input id="sale_price'+num_field+'" class="form-control" type="text" name="sale_price[]" maxlength="25" placeholder="Введите цену"></div>'+
                            '<div class="form-group"><label for="prim'+num_field+'">Примечание</label><input maxlength="255" id="prim'+num_field+'" class="form-control" type="text" name="prim[]" placeholder="Введите комментарий по товару"></div>'+
                            '<div class="form-group"><label for="mounth'+num_field+'">Мес. потребление</label><input maxlength="255" id="mounth'+num_field+'" class="form-control" type="text" name="mounth[]" placeholder="Введите мес. потребление"></div>'+
                            '<div class="form-group"><label for="need'+num_field+'">Необх. объем</label><input onchange="check_need_value(this);" maxlength="255" id="need'+num_field+'" required class="form-control" type="number" step="any" name="need[]" placeholder="Введите необх. объем"></div>'+
                            '<div class="form-group"><label for="need_unit'+num_field+'">Ед. измерения</label><select onchange="check_unit(this);" id="need_unit'+num_field+'" class="form-control" name="need_unit[]">'+options+'</select></div>'+
                            '<div class="form-group"><label for="conc'+num_field+'">Конкурент</label><input maxlength="255" id="conc'+num_field+'" class="form-control farms" type="text" name="conc[]" placeholder="Введите конкурента, если есть"></div>'+
                            '<div class="form-group"><label for="price'+num_field+'">Его цена</label><input maxlength="255" id="price'+num_field+'" class="form-control" type="text" name="price[]" placeholder="Введите его цену"></div>'+
                            '<div class="form-group"><label for="dostavka'+num_field+'">Вид доставки</label>' +
                            '<select class="form-control" id="dostavka'+num_field+'" name = "dostavka[]">' +
                            '<option>Вид доставки</option><option>С доставкой</option><option>Без доставки</option></select></div>'+
                            '<div class="form-group"><label for="uslov'+num_field+'">Условия</label>' +
                            '<select class="form-control" id="uslov'+num_field+'" name = "uslov[]">' +
                            '<option>Условия оплаты</option><option>По факту</option><option>Предоплата</option><option>Отсрочка</option><option>Иное</option></select></div>'+
                            '<div class="form-group"><label for="commc'+num_field+'">Комментарии по конкуренту</label><input id="commc'+num_field+'" class="form-control" type="text" name="commc[]" placeholder="Введите примечания по конкуренту"></div></div>');
                        $(".farms").suggestions({
                            token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                            type: "PARTY",
                            onSelect: function (suggestion) {
                                //console.log(suggestion);
                            }
                        });
                    }
                });

                $('div#add_field_area').on('click', 'p#deleteProduct',function() {
                    var id_del = $(this).attr("class");
                    $("div#"+id_del).remove();
                });
            });
        })(jQuery);
    </script>
@endpush