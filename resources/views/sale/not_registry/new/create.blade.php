@component('modal')
    @slot('name', 'request-sale-modal')
    @slot('title', 'Новый запрос')
    @slot('route', 'sale.not_registry.new.create')
    @slot('enctype', '')

        {!! csrf_field() !!}
        <div class="form-group">
            <label for="point_A">Точка А</label>
            <input maxlength="255" id="point_A" required class="form-control" name="point_A" type="text" placeholder="Введите адрес">
        </div>

        <div class="form-group">
            <label for="point_B">Точка Б</label>
            <input maxlength="255" id="point_B" required class="form-control" name="point_B" type="text" placeholder="Введите адрес">
        </div>

        <div class="form-group">
            <label for="farm_A">Название хоз-ва А</label>
            <input maxlength="255" id="farm_A" required class="form-control" name="farm_A" type="text" placeholder="Введите наименование организации">
        </div>

        <div class="form-group">
            <label for="farm_B">Название хоз-ва Б</label>
            <input maxlength="255" id="farm_B" required class="form-control" name="farm_B" type="text" placeholder="Введите наименование организации">
        </div>

        <div class="form-group">
            <label for="distance">Расстояние</label>
            <input id="distance" required class="form-control" name="distance" type="number" placeholder="Введите расстояние">
        </div>

        <div class="form-group">
            <label for="tonnage">Сколько кг везти</label>
            <input id="tonnage" required class="form-control" name="tonnage" type="number" placeholder="Введите сколько кг везти">
        </div>

        <div class="form-group">
            <label for="type_load">Тип загрузки</label>
            <select id="type_load" class="form-control" name="type_load">
                @foreach ($type_load as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endforeach;
            </select>
        </div>

        <div class="form-group">
            <label for="comment">Комментарии</label>
            <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500"></textarea>
        </div>
    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить запрос</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $("#point_A").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "ADDRESS",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#point_B").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "ADDRESS",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#farm_A").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "PARTY",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
                $("#farm_B").suggestions({
                    token: "0e86c6fe24ed0498f8f911c6230775b5b272c473",
                    type: "PARTY",
                    onSelect: function (suggestion) {
                        //console.log(suggestion);
                    }
                });
            });
        })(jQuery);
    </script>
@endpush