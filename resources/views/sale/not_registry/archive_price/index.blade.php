@extends('layouts.app', ['navigator' => 'Отдел продаж'])
@section('menu')
    @include('sale.not_registry._vertical_nav')
@endsection
@section('content')
    @include('sale.not_registry.archive_price._nav')
    @include('sale.not_registry.search',['type_load' => $type_load])

    @if (!$search)
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('sale.not_registry.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="page" name="page" value="archive_price">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    @endif

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_sale = [1=>'Запросы на расчет', 2=>'Ответы на запросы', 3=>'Архив запросов', 4=>'Архив готовых цен'])
            @else
                <th>Цена</th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                @if ($search && $status)
                    <td>{{ $status_sale[$req->id_status_sale] }}</td>
                @else
                    <td>{{ $req->price }}</td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->links() }}
    @endif
@endsection