<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.not_registry.new.index') }}">Запросы на расчет</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.not_registry.answer.index') }}">Ответы на запросы</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('sale.not_registry.archive_answer.index') }}">Архив запросов</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.not_registry.archive_price.index') }}">Архив готовых цен</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('sale.not_registry.requests.index') }}">Запросы в пути</a></li>
</ul>