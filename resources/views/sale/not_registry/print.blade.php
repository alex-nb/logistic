<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Print</title>
        <style type="text/css">
            table {
                width:100%;
                border: 1px solid black;
                border-collapse:collapse;
            }
            th {
                text-align: left; /* Выравнивание по левому краю */
                background: #D3D3D3; /* Цвет фона ячеек */
                border: 1px solid black; /* Граница вокруг ячеек */
            }
            td {
                border: 1px solid black; /* Граница вокруг ячеек */
            }
        </style>

    </head>
    <body onload="setTimeout(function () { window.print(); }, 500); window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }">
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Дата</th>
                <th>Менеджер</th>
                <th>Комментарий</th>
                <th>Точка А</th>
                <th>Точка Б</th>
                <th>Расстояние</th>
                <th>Тоннаж(кг)</th>
                <th>Тип загрузки</th>
                @if ($type == 'answer' || $type == 'archive_answer' || $type == 'archive_price')
                    <th>Цена</th>
                @endif
                @if ($type == 'answer' || $type == 'archive_answer')
                    <th>Комментарий</th>
                    <th>Дата ответа</th>
                    <th>Логист</th>
                @endif
            </tr>
            </thead>
            <tbody>

            @foreach ($requests as $req)
                <tr>
                    <td>{{ $req->id }}</td>
                    <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                    <td>{{ $req->userCreate->name }}</td>
                    <td>{{ $req->comment_create }}</td>
                    <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                    <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                    <td>{{ $req->distance }}</td>
                    <td>{{ $req->tonnage }}</td>
                    <td>{{ $req->typeLoad->name }}</td>
                    @if ($type == 'answer' || $type == 'archive_answer' || $type == 'archive_price')
                        <td>{{ $req->price }}</td>
                    @endif
                    @if ($type == 'answer' || $type == 'archive_answer')
                        <td>{{ $req->comment }}</td>
                        <td>
                            @if (!empty($req->date_answer))
                                {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                            @endif
                        </td>
                        <td>
                            @if (isset($req->userLogist))
                                @if (!empty($req->userLogist->last_name))
                                    {{$req->userLogist->last_name }},
                                @endif
                                {{$req->userLogist->name}}
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </body>
</html>