<div class="card mb-2" id="accordion">
    <!-- 1 панель -->

        <!-- Заголовок 1 панели -->
        <div class="card-header">
            <h4 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" data-parent="#accordion">
                    Поиск
                </button>
            </h4>
        </div>
        <div id="collapseOne" class="card-body collapse">
            <!-- Содержимое 1 панели -->

                <form action="?" method="GET">
                    <div class="row">
                        <div class="col-sm-1 form-group">
                            <label for="id" class="col-form-label">№</label>
                            <input id="id" type="number" class="form-control" name="id" value="{{ request('id') }}">
                        </div>

                        <div class="col-sm-1 form-group">
                            <label for="datecreate" class="col-form-label">Дата создания</label>
                            <input id="datecreate" class="form-control daterange" name="datecreate"
                                   @if(!empty(request('datecreate')))
                                   value="{{ date('d-m-Y', strtotime(request('datecreate'))) }}"
                                    @endif
                            >
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="creator">Кто создал</label>
                            <input id="creator" name="creator" type="text"  autocomplete="off" class="typeahead form-control" maxlength="255" value="{{ request('creator') }}">
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="logist">Ответсвенный логист</label>
                            <input id="logist" name="logist" type="text"  autocomplete="off" class="typeahead form-control" maxlength="255" value="{{ request('logist') }}">
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="typeLoad">Тип загрузки</label>
                            <select id="typeLoad" class="form-control" name="typeLoad">
                                <option value=""></option>
                                @foreach ($type_load as $type)
                                    <option value="{{ $type->id }}"{{ $type->id == request('typeLoad') ? ' selected' : '' }}>{{ $type->name }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="col-form-label">По совпадению</label>
                                <input maxlength="255" id="name" class="form-control" name="name" value="{{ request('name') }}">
                            </div>
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="field">В поле</label>
                            <select id="field" class="form-control" name="field">
                                <option value="farmA" {{ request('field') == "farmA" ? ' selected' : '' }}>Точка А</option>
                                <option value="farmB" {{ request('field') == "farmB" ? ' selected' : '' }}>Точка Б</option>
                                <option value="distance" {{ request('field') == "distance" ? ' selected' : '' }}>Расстояние</option>
                                <option value="comment_op" {{ request('field') == "comment_op" ? ' selected' : '' }}>Комментарий ОП</option>
                                <option value="comment_ol" {{ request('field') == "comment_ol" ? ' selected' : '' }}>Комментарий ОЛ</option>
                            </select>
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="status">Статус</label>
                            <select id="status" class="form-control" name="status">
                                <option value="this" {{ request('status') == "this" ? ' selected' : '' }}>По текущему</option>
                                <option value="all" {{ request('status') == "all" ? ' selected' : '' }}>По всем</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label">&nbsp;</label><br />
                                <button type="submit" class="btn btn-primary">Найти</button>
                            </div>
                        </div>

                    </div>
                </form>

        </div>

</div>

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                var path = "{{ route('purchase.not_registry.autocomplete') }}";
                $('input.typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 3,
                    source:  function (query, process) {
                        return $.get(path, { query: query }, function (data) {
                            return process(data);
                        });
                    }
                });
                $('.daterange').daterangepicker({
                    singleDatePicker: true,
                    autoUpdateInput: false,
                    "locale": {
                        "cancelLabel" : "Отмена",
                        "applyLabel" : "Выбрать",
                        "daysOfWeek": [
                            "Вс",
                            "Пн",
                            "Вт",
                            "Ср",
                            "Чт",
                            "Пт",
                            "Сб"
                        ],
                        "monthNames": [
                            "Январь",
                            "Февраль",
                            "Март",
                            "Апрель",
                            "Май",
                            "Июнь",
                            "Июль",
                            "Август",
                            "Сентябрь",
                            "Октябрь",
                            "Ноябрь",
                            "Декабрь"
                        ],
                        "firstDay": 1
                    }
                });
                $('.daterange').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY'));
                });
                $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
            });
        })(jQuery);
    </script>
@endpush
