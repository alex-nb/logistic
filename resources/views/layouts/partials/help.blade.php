@component('modal')
    @slot('name', 'ask-help-modal')
    @slot('title', 'Получить обратную связь')
    @slot('route', 'help')
    @slot('enctype', '')


        {!! csrf_field() !!}

        <div class="form-group">
            <label for="question">Что вы хотите сказать нам? </label>
            <textarea rows="10" id="question" class="form-control" name="question" placeholder="Я хочу сказать, что..."></textarea>
        </div>

        @slot('buttons')
            <button type="submit" class="btn btn-success" id="save" data-dismiss="modal">Отправить</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        @endslot

@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $('#save').on('click', function(e){
                    e.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: '/help',
                        data: $('#form_modal').serialize(),
                        success: function(){
                            $('#question').val('');
                        }
                    });
                });
            });
        })(jQuery);
    </script>
@endpush