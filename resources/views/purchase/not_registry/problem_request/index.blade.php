@extends('layouts.app', ['navigator' => 'Отдел закупок'])
@section('menu')
    @include('purchase.not_registry._vertical_nav')
@endsection
@section('content')
    @include('purchase.not_registry.problem_request._nav')
    @include('purchase.not_registry.history')
    @include('purchase.not_registry.search',['shipping' => $shipping, 'package' => $package])
    @if (!$search)
    <p>
    <div class="row">
        <div class="col-md-1">
            <form method="POST" action="{{ route('purchase.not_registry.problem_request.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="2">
                <button class="btn btn-secondary">Все запросы</button>
            </form>
        </div>
        <div class="col-md-1">
            <form method="POST" action="{{ route('purchase.not_registry.problem_request.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="1">
                <button class="btn btn-secondary">Только мои</button>
            </form>
        </div>
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('purchase.not_registry.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="my" name="my" value="{{$sort}}">
                <input type="hidden"  id="page" name="page" value="problem_request">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    </div>
    </p>
    @endif

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Тип перевозки</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            <th>Товары</th>
            <th>ADR</th>
            <th>Упаковка</th>
            <th>Дата вывоза</th>
            <th>Поддоны</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_purchase = ['Ждет ответа', 'Ответ получен', 'Везем', 'Поиск машины', 'В пути к отправителю', 'В пути к получаетелю',
                'У получателя', 'Реализованные запросы', 'Нереализованные запросы', 'Машина найдена', 'Проблемные запросы'])
            @else
                <th style="background: #c1e2b3;">Цена</th>
                <th style="background: #c1e2b3;">Комментарий</th>
                <th style="background: #c1e2b3;">Дата ответа</th>
                <th style="background: #c1e2b3;">Логист</th>
                <th>Причина попадания</th>
            @endif

        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->shipping->name }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                <td>
                    @foreach($req->products as $prod)
                        {{ $prod->name }} <br>
                    @endforeach
                </td>
                <td>{{ $req->adr }}</td>
                <td>{{ $req->packaging->name }}</td>
                <td>
                       @if (!empty($req->date_export))
                        {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                    @endif
                 </td>
                <td>
                    @if ($req->pallet == 0)
                        Нет
                    @else
                        Да
                    @endif
                </td>
                @if ($search && $status)
                    <td>{{ $status_purchase[$req->id_status_purchase-1] }}</td>
                @else
                    <td style="background: #c1e2b3;">
                        {{ $req->price }}
                        @if(!$req->requestHistory->isEmpty() && $req->requestHistory->count() > 1)
                            <p><h5><span style="cursor:pointer;" title="История цен" onclick="modal_history({{$req->requestHistory}})">&#128193;</span></h5></p>
                        @endif
                    </td>
                    <td style="background: #c1e2b3;">{{ $req->comment }}</td>
                    <td style="background: #c1e2b3;">
                        @if (!empty($req->date_answer))
                            {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                        @endif
                    </td>
                    <td style="background: #c1e2b3;">
                        @if (isset($req->userLogist))
                            @if (!empty($req->userLogist->last_name))
                                {{$req->userLogist->last_name }},
                            @endif
                            {{$req->userLogist->name}}
                        @endif
                    </td>
                    <td>{{ $req->comment_problem }}</td>
                @endif

            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->appends(array('my' => $sort))->links() }}
    @endif

@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_history (history) {
            history.pop();
            var html =
                '<table class="table table-bordered table-striped">'+
                '<thead>'+
                '<tr>'+
                '<th>Дата вывоза</th>'+
                '<th>Цена</th>'+
                '<th>Комментарий</th>'+
                '<th>Логист</th>'+
                '<th>Дата ответа</th>'+
                '</tr>'+
                '</thead>'+
                '<tbody>';
            $.each(history,function(index,value){
                html += '<tr>';
                if (value.date_export !== null) {
                    html += '<td>'+value.date_export+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.price !== null) {
                    html += '<td>'+value.price+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.comment_logist !== null) {
                    html += '<td>'+value.comment_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.logist !== null) {
                    html += '<td>'+value.logist.name+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.date_answer_logist !== null) {
                    html += '<td>'+value.date_answer_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                html += '</tr>';
            });
            html += '</tbody></table>';
            $("div#show_history").html(html);
            $('#history-modal').modal('show');
        };
    </script>
@endpush