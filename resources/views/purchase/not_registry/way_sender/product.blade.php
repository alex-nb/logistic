@component('modal')
    @slot('name', 'request-product-modal')
    @slot('title', 'Ввод данных о загрузке')
    @slot('route', 'purchase.not_registry.way_sender.product')
    @slot('enctype', 'enctype=multipart/form-data')

        {!! csrf_field() !!}

        <div class="form-group">
            <label for="file_shipping">Скан отгрузочных документов</label>
            <input id="file_shipping" class="form-control" name="file_shipping[]" type="file" multiple>
        </div>

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_request_accepted" name="id_request_accepted">
        <input type="hidden"  id="shipping" name="shipping">
        <input type="hidden"  id="num" name="num">

        <div id="add_field_area">
            <div id="add1" class="add">
                <div class="form-group">
                    <label for="product1">Наименование товара</label>
                    <input id="product1" required class="form-control" type="text" name="product[]" placeholder="Введите наименование">
                    <label for="unit1">Единица измерения</label>
                    <input id="unit1" required class="form-control" type="text" name="unit[]" placeholder="Введите единицу измерения">
                    <label for="weight1">Вес</label>
                    <input id="weight1" required class="form-control" type="number" name="weight[]" placeholder="Введите вес товара">
                </div>
            </div>
        </div>
        <button id="newProduct" class="btn btn-light">Добавить еще один товар</button><br>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $('#newProduct').on('click', function(e) {
                    e.preventDefault();
                    var num_field = parseInt($("#add_field_area").find("div.add:last").attr("id").slice(3))+1;
                    $("div#add_field_area").append('<div id="add'+num_field+'" class="add" style="outline: 1px dotted #000;"> <p title="Удалить товар" style="cursor: pointer;" id="deleteProduct" class="add'+num_field+'">&#10007;</p>'+
                        '<div class="form-group">' +
                        '<label for="product'+num_field+'">Наименование товара</label><input required id="product'+num_field+'" class="form-control" type="text" name="product[]" placeholder="Введите товар">' +
                        '<label for="unit'+num_field+'">Единица измерения</label><input required id="unit'+num_field+'" class="form-control" type="text" name="unit[]" placeholder="Введите единицу измерения">'+
                        '<label for="weight'+num_field+'">Вес</label><input required id="weight'+num_field+'" class="form-control" type="text" name="weight[]" placeholder="Введите вес товара">'+
                        '</div></div>');
                });

                $('div#add_field_area').on('click', 'p#deleteProduct',function() {
                    var id_del = $(this).attr("class");
                    $("div#"+id_del).remove();
                });
            });
        })(jQuery);
    </script>
@endpush