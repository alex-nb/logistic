@extends('layouts.app', ['navigator' => 'Отдел закупок'])
@section('menu')
    @include('purchase.not_registry._vertical_nav')
@endsection
@section('content')
    @include('purchase.not_registry.new._nav')
    @include('purchase.not_registry.new.create',['type_load' => $type_load, 'shipping' => $shipping, 'package' => $package, 'managers' => $mass_man])
    @include('purchase.not_registry.search',['shipping' => $shipping, 'package' => $package])
    <p>
        <div class="row">
            <div class="col-md-2">
                <button
                        type="button"
                        class="btn btn-success"
                        data-toggle="modal"
                        data-target="#request-purchase-modal">
                    Добавить запрос
                </button>
            </div>
            @if (!$search)
                <div class="col-md-1">
                    <form method="POST" action="{{ route('purchase.not_registry.new.index') }}">
                        @csrf
                        <input type="hidden"  id="my" name="my" value="2">
                        <button class="btn btn-secondary">Все запросы</button>
                    </form>
                </div>
                <div class="col-md-1">
                    <form method="POST" action="{{ route('purchase.not_registry.new.index') }}">
                        @csrf
                        <input type="hidden"  id="my" name="my" value="1">
                        <button class="btn btn-secondary">Только мои</button>
                    </form>
                </div>
                <div class="col-md-1">
                    <form id="print" method="POST" action="{{ route('purchase.not_registry.print') }}" target="_blank">
                        @csrf
                        <input type="hidden"  id="my" name="my" value="{{$sort}}">
                        <input type="hidden"  id="page" name="page" value="new">
                        <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
                    </form>
                </div>
            @endif
        </div>
    </p>


    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Тип перевозки</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            <th>Товары</th>
            <th>ADR</th>
            <th>Упаковка</th>
            <th>Дата вывоза</th>
            <th>Поддоны</th>
            <th>
            @if ($search && $status)
                Статус
                @php($status_purchase = ['Ждет ответа', 'Ответ получен', 'Везем', 'Поиск машины', 'В пути к отправителю', 'В пути к получаетелю',
                'У получателя', 'Реализованные запросы', 'Нереализованные запросы', 'Машина найдена', 'Проблемные запросы'])
            @endif
            </th>
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
                <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>
                <td>{{ $req->shipping->name }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                <td>
                    @foreach($req->products as $prod)
                        {{ $prod->name }} <br>
                    @endforeach
                </td>
                <td>{{ $req->adr }}</td>
                <td>{{ $req->packaging->name }}</td>
                <td>
                       @if (!empty($req->date_export))
                        {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                    @endif
                 </td>
                <td>
                    @if ($req->pallet == 0)
                        Нет
                    @else
                        Да
                    @endif
                </td>
                <td>
                    @if ($search && $status)
                        {{ $status_purchase[$req->id_status_purchase-1] }}
                    @else
                        <form method="POST" action="{{ route('purchase.not_registry.new.index') }}">
                            @csrf
                            <input type="hidden"  id="cancel" name="cancel" value="{{$req->id}}">
                            <button class="btn btn-danger">Отмена</button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->appends(array('my' => $sort))->links() }}
    @endif
@endsection