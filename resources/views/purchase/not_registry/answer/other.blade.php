@component('modal')
    @slot('name', 'answer-other-modal')
    @slot('title', 'Другая дата вывоза')
    @slot('route', 'purchase.not_registry.answer.other')
    @slot('enctype', '')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_req" name="id_req">

        <div class="form-group">
            <label for="other_date">Новая дата вывоза</label>
            <input id="other_date" class="form-control" name="other_date" type="text" placeholder="Введите новую дату">
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function () {
                $.datetimepicker.setLocale('ru');
                $('#other_date').datetimepicker({
                    format:'Y-m-d H:i:s',
                });
            });
        })(jQuery);
    </script>
@endpush