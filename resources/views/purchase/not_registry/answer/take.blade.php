@component('modal')
    @slot('name', 'request-take-modal')
    @slot('title', 'Запрос на поиск машины')
    @slot('route', 'purchase.not_registry.answer.take')
    @slot('enctype', '')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">

        <div class="form-group">
            <label for="time_out">Крайняя дата вывоза</label>
            <input id="time_out" required class="form-control" name="time_out" type="datetime" placeholder="Выберите время вывоза">
        </div>

        <div class="form-group">
            <label for="fio_shipper">Грузоотправитель</label>
            <input maxlength="100" id="fio_shipper" required class="form-control" name="fio_shipper" type="text" placeholder="Введите ФИО представителя грузоотправителя">
        </div>

        <div class="form-group">
            <label for="phone_shipper">Контакты грузоотправителя</label>
            <input maxlength="255" id="phone_shipper" required class="form-control" name="phone_shipper" type="text" placeholder="Введите телефон представителя грузоотправителя">
        </div>

        <div class="form-group">
            <label for="comment_shipper">Комментарии по грузоотправителю</label>
            <textarea rows="3" id="comment_shipper" class="form-control" name="comment_shipper" placeholder="Комментарии по грузоотправителю"  maxlength="500"></textarea>
        </div>

        <div class="form-group">
            <label for="fio_consignee">Грузополучатель</label>
            <input maxlength="100" id="fio_consignee" required class="form-control" name="fio_consignee" type="text" placeholder="Введите ФИО представителя грузополучателя">
        </div>

        <div class="form-group">
            <label for="phone_consignee">Контакты грузополучателя</label>
            <input maxlength="255" id="phone_consignee" required class="form-control" name="phone_consignee" type="text" placeholder="Введите телефон представителя грузополучателя">
        </div>

        <div class="form-group">
            <label for="comment_consignee">Комментарии по грузополучателю</label>
            <textarea rows="3" id="comment_consignee" class="form-control" name="comment_consignee" placeholder="Комментарии по грузоотправителю"  maxlength="500"></textarea>
        </div>

        <div class="form-group">
            <label for="id_manager">Ответсвенный менеджер</label>
            <select id="id_manager" class="form-control" name="id_manager">
                @foreach ($managers as $id=>$man)
                    <option value="{{ $id }}">{{ $man }}</option>
                @endforeach;
            </select>
            <input type="hidden"  id="name_manager" name="name_manager">
        </div>

        <div id ="num_one_s">
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить запрос</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $.datetimepicker.setLocale('ru');
                $('#time_out').datetimepicker({
                    format:'Y-m-d H:i:s',
                });

                $("#name_manager").val($("#id_manager option:selected").html());
                $("#id_manager").change(function() {
                    $("#name_manager").val($("#id_manager option:selected").html());
                });
            });
        })(jQuery);
    </script>
@endpush