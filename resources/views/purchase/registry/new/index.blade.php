@extends('layouts.app', ['navigator' => 'Отдел закупок'])
@section('menu')
    @include('purchase.registry._vertical_nav')
@endsection
@section('content')

    @include('purchase.registry.new._nav')
    @include('purchase.registry.search', ['shipping' => $shipping])
    @if (!$search)
    <p>
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('purchase.registry.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="page" name="page" value="new">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    </p>
    @endif

    <table class="table table-bordered table-hover" style="border: 1px double black;">
        <thead>
        <tr>
            <th rowspan="2" style="border: 1px double black;">№</th>
            <th rowspan="2" style="border: 1px double black;">Дата</th>
            <th rowspan="2" style="border: 1px double black;">Менеджер</th>
            <th rowspan="2" style="border: 1px double black;">Срочность</th>
            <th rowspan="2" style="border: 1px double black;">Хозяйство</th>
            <th rowspan="2" style="border: 1px double black;">Тип доставки</th>
            <th rowspan="2" style="border: 1px double black;">Комментарий</th>
            <th rowspan="2" style="border: 1px double black;">Документы от ОП</th>
            <th colspan="12" style="border: 1px double black;">Продукты</th>
        </tr>
        <tr>
            <th style="border: 1px double black;">Продукт</th>
            <th style="border: 1px double black;">Задача</th>
            <th style="border: 1px double black;">Необходимая цена</th>
            <th style="border: 1px double black;">Примечание</th>
            <th style="border: 1px double black;">Мес. потребление</th>
            <th style="border: 1px double black;">Необх. объем</th>
            <th style="border: 1px double black;">Конкурент</th>
            <th style="border: 1px double black;">Его цена</th>
            <th style="border: 1px double black;">Вид доставки</th>
            <th style="border: 1px double black;">Условия</th>
            <th style="border: 1px double black;">Дополнительно</th>
            @if ($search && $status)
                <th style="border: 1px double black;">Статус</th>
                @php($status_purchase = [5=>'Новые запросы', 6=>'Ответ представлен', 7=>'Архив'])
            @else
                <th style="border: 1px double black;"></th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            @php($kol=count($req->registryProducts))
            @php($i=0)
            @php($style='')
            <tr>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->id }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->userCreate->name }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->urgency }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->farmsAddressB['name'] }}, {{ $req->farmsAddressB['address'] }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->shipping['name'] }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->comment_sale }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">
                    @if (!empty($req->file_sale))
                        @php($array = explode("&", $req->file_sale))
                        @foreach ($array as $file)
                            @if (strlen($file) > 0)
                                <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                            @endif
                        @endforeach
                    @endif
                </td>
                @if ($kol > 0)
                    @foreach($req->registryProducts as $product)
                        @if($i==$kol-1)
                            @php($style='border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;')
                        @endif
                        <td style="{{$style}}">{{ $product['product']['name'] }}</td>
                        <td style="{{$style}}">{{ $product['type_request'] }}. {{ $product['type_discount'] }}</td>
                        <td style="{{$style}}">{{ $product['price_sale'] }}</td>
                        <td style="{{$style}}">{{ $product['product_comment'] }}</td>
                        <td style="{{$style}}">{{ $product['product_monthly_need'] }}</td>
                        <td style="{{$style}}">{{ $product['product_value'] }} {{!empty($product['unit']['name']) ? $product['unit']['name']: ""}}</td>
                        <td style="{{$style}}">{{ $product['competitor']['name'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_price'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_delivery'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_payment'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_comment'] }}</td>
                        @if ($search && $status)
                            <td style="{{$style}}">{{ $status_purchase[$req->id_status_sale] }}</td>
                        @else
                            <td style="{{$style}}">
                                @if(!isset($product['answer']))
                                    <p><a href="{{ route('purchase.registry.new.answer', ['id'=>$product['id']]) }}" class="btn btn-success mr-3">Дать ответ</a></p>
                                @else
                                    <p><a href="{{ route('purchase.registry.new.answer', ['id'=>$product['id']]) }}" class="btn btn-secondary mr-3">Изменить ответ</a></p>
                                @endif
                            </td>
                        @endif
                        </tr>
                        @php($i++)
                    @endforeach
                @else
                    @if ($search && $status)
                        @for($i=0; $i<11; $i++)
                            <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">-</td>
                        @endfor
                        <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">{{ $status_purchase[$req->id_status_sale] }}</td>
                    @else
                        @for($i=0; $i<12; $i++)
                            <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">-</td>
                        @endfor
                    @endif
                    </tr>
                @endif
        @endforeach
        </tbody>
    </table>

    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->links() }}
    @endif
@endsection