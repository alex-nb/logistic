@extends('layouts.app', ['navigator' => 'Отдел закупок'])
@section('menu')
    @include('purchase.registry._vertical_nav')
@endsection
@section('content')

    <form method="POST" id="form_modal" action="{{ route('purchase.registry.new.answer') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <input type="hidden"  id="id_product" name="id_product" value="{{$id_product}}">
        <input type="hidden"  id="id_request" name="id_request" value="{{$id_request}}">

        <div class="alert alert-primary form-group">
            <h5><b>№:</b> {{$request_info->id}}</h5>
            <h5><b>Дата:</b> {{$request_info->created_at}}</h5>
            <h5><b>Менеджер:</b> {{$request_info->userCreate->name}}</h5>
            <h5><b>Хозяйство:</b> {{$request_info->farmsAddressB['name']}}, {{ $request_info->farmsAddressB['address'] }}</h5>
            <h5><b>Тип доставки:</b> {{$request_info->shipping['name']}}</h5>
            <h5><b>Комментарий от продаж:</b> {{$request_info->comment_sale}}</h5>
        </div>

        <div class="form-group">
            <label for="file_purchase">Необходимые документы</label>
            <input id="file_purchase" class="form-control" name="file_purchase[]" type="file" multiple>
        </div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th style="font-size: 12px;">Продукт</th>
                    <th style="font-size: 12px;">Задача</th>
                    <th style="font-size: 12px;">Необх. цена</th>
                    <th style="font-size: 12px;">Примечание</th>
                    <th style="font-size: 12px;">Мес. потр.</th>
                    <th style="font-size: 12px;">Необх. объем</th>
                    <th style="font-size: 12px;">Конкурент</th>
                    <th style="font-size: 12px;">Его цена</th>
                    <th style="font-size: 12px;">Вид доставки</th>
                    <th style="font-size: 12px;">Условия</th>
                    <th style="font-size: 12px;">Доп.</th>
                    <th style="font-size: 12px; min-width: 200px;">Ответ</th>
                    <th style="font-size: 12px; min-width: 200px;">Стоимость доставки</th>
                    <th style="font-size: 12px; min-width: 200px;">Место забора</th>
                    <th style="font-size: 12px; min-width: 200px;">Планируемая валовая прибыль</th>
                    <th style="font-size: 12px; min-width: 150px;">Срок действия цены (дней)</th>
                    <th style="font-size: 12px; min-width: 200px;">Комментарий</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td style="font-size: 12px;">{{$product->product['name']}}</td>
                        <td style="font-size: 12px;">{{ $product->type_request}}. {{ $product->type_discount}}</td>
                        <td style="font-size: 12px;">{{ $product['price_sale'] }}</td>
                        <td style="font-size: 12px;">{{ $product->product_comment }}</td>
                        <td style="font-size: 12px;">{{ $product->product_monthly_need }}</td>
                        <td style="font-size: 12px;">{{ $product->product_value}} {{!empty($product['unit']['name']) ? $product['unit']['name']: ""}}</td>
                        <td style="font-size: 12px;">{{ $product->competitor['name'] }}</td>
                        <td style="font-size: 12px;">{{ $product->competitor_price}}</td>
                        <td style="font-size: 12px;">{{ $product->competitor_delivery }}</td>
                        <td style="font-size: 12px;">{{ $product->competitor_payment }}</td>
                        <td style="font-size: 12px;">{{ $product->competitor_comment}}</td>
                        <td>
                            @if($product->id == $id_product)
                                <textarea rows="3" id="answer" required class="form-control" name="answer" placeholder="Ответ" maxlength="255">{{$product->answer}}</textarea>
                            @else
                                {{$product->answer}}
                            @endif
                        </td>
                        <td>
                            @if($product->id == $id_product)
                                <textarea rows="3" id="price" class="form-control" name="price" placeholder="Стоимость доставки" maxlength="255">{{$product->price_purchase}}</textarea>
                            @else
                                {{$product->price_purchase}}
                            @endif
                        </td>
                        <td>
                            @if($product->id == $id_product)
                                <textarea rows="3" id="place" class="form-control" name="place" placeholder="Место забора" maxlength="255">{{$product->place_take}}</textarea>
                            @else
                                {{$product->place_take}}
                            @endif
                        </td>
                        <td>
                            @if($product->id == $id_product)
                                <textarea rows="3" id="profit" class="form-control" name="profit" placeholder="Планируемая валовая прибыль" maxlength="255">{{$product->gross_profit}}</textarea>
                            @else
                                {{$product->gross_profit}}
                            @endif
                        </td>
                        <td>
                            @if($product->id == $id_product)
                                <input id="time" required class="form-control" type="number" name="time" placeholder="Срок" value="{{$product->time_active_price}}">
                            @else
                                {{$product->time_active_price}}
                            @endif
                        </td>
                        <td>
                            @if($product->id == $id_product)
                                <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500">{{$product->comment_purchase}}</textarea>
                            @else
                                {{$product->comment_purchase}}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <button type="submit" class="btn btn-success" id="save">Дать ответ</button>
        <a href="{{ route('purchase.registry.new.index') }}" class="btn btn-danger mr-3">Отмена</a>
    </form>
@endsection