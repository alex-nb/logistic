@extends('layouts.app', ['navigator' => 'Отдел закупок'])
@section('menu')
    @include('purchase.registry._vertical_nav')
@endsection
@section('content')

    @include('purchase.registry.archive._nav')
    @include('purchase.registry.archive.show_communicate')
    @include('purchase.registry.search', ['shipping' => $shipping])

    @if (!$search)
    <p>
    <div class="row">
        <div class="col-md-1">
            <form method="POST" action="{{ route('purchase.registry.archive.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="2">
                <button class="btn btn-secondary">Все запросы</button>
            </form>
        </div>
        <div class="col-md-1">
            <form method="POST" action="{{ route('purchase.registry.archive.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="1">
                <button class="btn btn-secondary">Только мои</button>
            </form>
        </div>
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('purchase.registry.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="my" name="my" value="{{$sort}}">
                <input type="hidden"  id="page" name="page" value="archive">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    </div>
    </p>
    @endif

    <table class="table table-bordered table-hover" style="border: 1px double black;">
        <thead>
        <tr>
            <th rowspan="2" style="border: 1px double black;">№</th>
            <th rowspan="2" style="border: 1px double black;">Дата</th>
            <th rowspan="2" style="border: 1px double black;">Менеджер ОП</th>
            <th rowspan="2" style="border: 1px double black;">Срочность</th>
            <th rowspan="2" style="border: 1px double black;">Хозяйство</th>
            <th rowspan="2" style="border: 1px double black;">Тип доставки</th>
            <th rowspan="2" style="border: 1px double black;">Комментарий от ОП</th>
            <th rowspan="2" style="border: 1px double black;">Документы от ОП</th>
            <th rowspan="2" style="border: 1px double black;">Документы от ОЗ</th>
            <th colspan="19" style="border: 1px double black;">Продукты</th>
            @if (!($search && $status))
                <th rowspan="2" style="border: 1px double black;">Результат</th>
                <th rowspan="2" style="border: 1px double black;">Комментарий по результатам</th>
                <th rowspan="2" style="border: 1px double black;">Коммуникации</th>
            @endif
        </tr>
        <tr>
            <th style="border: 1px double black;">Продукт</th>
            <th style="border: 1px double black;">Задача</th>
            <th style="border: 1px double black;">Необходимая цена</th>
            <th style="border: 1px double black;">Примечание</th>
            <th style="border: 1px double black;">Мес. потребление</th>
            <th style="border: 1px double black;">Необх. объем</th>
            <th style="border: 1px double black;">Конкурент</th>
            <th style="border: 1px double black;">Его цена</th>
            <th style="border: 1px double black;">Вид доставки</th>
            <th style="border: 1px double black;">Условия</th>
            <th style="border: 1px double black;">Дополнительно</th>
            @if ($search && $status)
                <th style="border: 1px double black;">Статус</th>
                @php($status_purchase = [5=>'Новые запросы', 6=>'Ответ представлен', 7=>'Архив'])
            @else
                <th style="border: 1px double black; background: #c1e2b3;">Ответ</th>
                <th style="border: 1px double black; background: #c1e2b3;">Стоимость доставки</th>
                <th style="border: 1px double black; background: #c1e2b3;">Место забора</th>
                <th style="border: 1px double black; background: #c1e2b3;">Планируемая валовая прибыль</th>
                <th style="border: 1px double black; background: #c1e2b3;">Срок действия цены (дн.)</th>
                <th style="border: 1px double black; background: #c1e2b3;">Комментарий от ОЗ</th>
                <th style="border: 1px double black; background: #c1e2b3;">Дата ответа</th>
                <th style="border: 1px double black; background: #c1e2b3;">Менеджер ОЗ</th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            @php($kol=count($req->registryProducts))
            @php($i=0)
            @php($style='')
            <tr>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->id }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->userCreate->name }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->urgency }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->shipping['name'] }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">{{ $req->comment_sale }}</td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">
                    @if (!empty($req->file_sale))
                        @php($array = explode("&", $req->file_sale))
                        @foreach ($array as $file)
                            @if (strlen($file) > 0)
                                <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                            @endif
                        @endforeach
                    @endif
                </td>
                <td rowspan="{{$kol==0 ? 1 : $kol}}" style="border: 1px double black;">
                    @if (!empty($req->file_purchase))
                        @php($array = explode("&", $req->file_purchase))
                        @foreach ($array as $file)
                            @if (strlen($file) > 0)
                                <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                            @endif
                        @endforeach
                    @endif
                </td>
                @if ($kol > 0)
                    @foreach($req->registryProducts as $product)
                        @if($i==$kol-1)
                            @php($style='border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;')
                        @endif
                        <td style="{{$style}}">{{ $product['product']['name'] }}</td>
                        <td style="{{$style}}">{{ $product['type_request'] }}. {{ $product['type_discount'] }}</td>
                        <td style="{{$style}}">{{ $product['price_sale'] }}</td>
                        <td style="{{$style}}">{{ $product['product_comment'] }}</td>
                        <td style="{{$style}}">{{ $product['product_monthly_need'] }}</td>
                        <td style="{{$style}}">{{ $product['product_value'] }} {{!empty($product['unit']['name']) ? $product['unit']['name']: ""}}</td>
                        <td style="{{$style}}">{{ $product['competitor']['name'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_price'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_delivery'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_payment'] }}</td>
                        <td style="{{$style}}">{{ $product['competitor_comment'] }}</td>
                            @if ($search && $status)
                                <td style="{{$style}}">{{ $status_purchase[$req->id_status_sale] }}</td>
                            @else
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['answer'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['price_purchase'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['place_take'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['gross_profit'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['time_active_price'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['comment_purchase'] }}</td>
                                <td style="{{$style}} background: #c1e2b3;">@if (!empty($product['date_answer'])){{ date("d.m.y H:i:s", strtotime($product['date_answer'])) }} @endif</td>
                                <td style="{{$style}} background: #c1e2b3;">{{ $product['userPurchase']['name'] }}</td>
                                @if($i==0)
                                    <td rowspan="{{$kol}}" style="border: 1px double black;">{{ $req->result_sale }}</td>
                                    <td rowspan="{{$kol}}" style="border: 1px double black;">{{ $req->comment_sale_result }}</td>
                                    <td rowspan="{{$kol}}" style="border: 1px double black;">
                                        @if (count($req->registryCommunication) > 0)
                                            <p>
                                                <button
                                                        type="button"
                                                        class="btn btn-info"
                                                        onclick="modal_data_show({{$req->registryCommunication}})">
                                                    Показать историю сообщений
                                                </button>
                                            </p>
                                        @endif
                                    </td>
                                @endif
                            @endif
                        </tr>
                        @php($i++)
                    @endforeach
        @else
            @if ($search && $status)
                @for($i=0; $i<11; $i++)
                    <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">-</td>
                @endfor
                <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">{{ $status_purchase[$req->id_status_sale] }}</td>
            @else
                @for($i=0; $i<19; $i++)
                    <td style="border-bottom-style: double; border-bottom-width: 1px; border-bottom-color: black;">-</td>
                @endfor
                <td rowspan="1" style="border: 1px double black;">{{ $req->result_sale }}</td>
                <td rowspan="1" style="border: 1px double black;">{{ $req->comment_sale_result }}</td>
                <td rowspan="1" style="border: 1px double black;">
                    @if (count($req->registryCommunication) > 0)
                        <p>
                            <button
                                    type="button"
                                    class="btn btn-info"
                                    onclick="modal_data_show({{$req->registryCommunication}})">
                                Показать историю сообщений
                            </button>
                        </p>
                    @endif
                </td>
            @endif
            </tr>
        @endif
        @endforeach
        </tbody>
    </table>

    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->appends(array('my' => $sort))->links() }}
    @endif
@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_data_show (communication) {
            $("div#show_message").html('');
            $.each(communication,function(index,value){
                $("div#show_message").append('<b>'+value.user.name+'</b>: '+value.message+' <i>('+value.created_at+')</i><hr>');
            });
            $('#show_communicate-purchase-modal').modal('show');
        };
    </script>
@endpush