@component('modal')
    @slot('name', 'show_communicate-purchase-modal')
    @slot('title', 'История сообщений')
    @slot('route', 'purchase.registry.archive.index')
    @slot('enctype', '')
    <div id="show_message"></div>

    @slot('buttons')
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    @endslot
@endcomponent