<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('purchase.registry.new.index') }}">Новые запросы</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('purchase.registry.answers.index') }}">Ответ представлен</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('purchase.registry.archive.index') }}">Архив</a></li>
</ul>