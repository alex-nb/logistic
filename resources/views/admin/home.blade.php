@extends('layouts.app', ['navigator' => 'Админка'])

@section('content')
    <ul class="nav nav-tabs mb-3">
        <li class="nav-item"><a class="nav-link active" href="{{ route('admin.home') }}">Админка</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.users.index') }}">Пользователи</a></li>
    </ul>


    <form method="POST" action="{{ route('admin.history') }}">
        {!! csrf_field() !!}

        <div class="form-group">
            <label for="text">Обновление</label>
            <textarea rows="15" id="text" class="form-control" name="text" maxlength="1000"></textarea>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="admin" name="who[]" disabled>Админы</label>
        </div>
        <div class="checkbox-inline">
            <label style="margin-right: 10px;"><input type="checkbox" value="sale_rp" name="who[]">Отдел продаж РП</label>
            <label style="margin-right: 10px;"><input type="checkbox" value="purchase" name="who[]">Отдел закупок РП</label>
            <label><input type="checkbox" value="accountant" name="who[]">Бухгалтерия РП</label>
        </div>
        <div class="checkbox">
            <label style="margin-right: 20px;"><input type="checkbox" value="logist" name="who[]">Отдел логистов Логикар</label>
            <label><input type="checkbox" value="sale_logist" name="who[]">Отдел продаж Логикар</label>
        </div>
        <button type="submit" class="btn btn-success" id="save">Отправить</button>
    </form>

@endsection
