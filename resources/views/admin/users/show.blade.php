@extends('layouts.app', ['navigator' => 'Админка'])

@section('content')
    @include('admin.users._nav')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary mr-1">Редактировать</a>
        @if ($user->isWait())
            <form method="POST" action="{{ route('admin.users.verify', $user) }}" class="mr-1">
                @csrf
                <button class="btn btn-danger">Подтвердить</button>
            </form>
        @endif
        <form method="POST" action="{{ route('admin.users.destroy', $user) }}" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Удалить</button>
        </form>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>ID</th><td>{{ $user->id }}</td>
        </tr>
        <tr>
            <th>Имя</th><td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>ID битрикс</th><td>{{ $user->id_bitrix }}</td>
        </tr>
        <tr>
            <th>Email</th><td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Статус</th>
            <td>
                @if ($user->isWait())
                    <span class="badge badge-secondary">Ожидает подтверждения</span>
                @endif
                @if ($user->isActive())
                    <span class="badge badge-primary">Подтвержден</span>
                @endif
            </td>
        </tr>
        <tr>
            <th>Роль</th>
            <td>
                @if ($user->isAdmin())
                    <span class="badge badge-danger">Админ</span>
                @elseif ($user->isLogist())
                    <span class="badge badge-secondary">Логист</span>
                @elseif ($user->isSaleLogist())
                    <span class="badge badge-warning">Логист ОП</span>
                @elseif ($user->isAccountant())
                    <span class="badge badge-secondary">Бухгалтер</span>
                @elseif ($user->isSale())
                    <span class="badge badge-secondary">Продажи</span>
                @elseif ($user->isPurchase())
                    <span class="badge badge-secondary">Закупки</span>
                @elseif ($user->isDirSale())
                    <span class="badge badge-secondary">Начальник ОП</span>
                @elseif ($user->isDirPurchase())
                    <span class="badge badge-success">Начальник ОЗ</span>
                @else
                    <span class="badge badge-secondary">User</span>
                @endif
            </td>
        </tr>
        <tbody>
        </tbody>
    </table>
@endsection