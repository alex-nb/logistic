@extends('layouts.app', ['navigator' => 'Админка'])

@section('content')
    @include('admin.users._nav')

    <p><a href="{{ route('admin.users.create') }}" class="btn btn-info">Добавить пользователя</a>
    <!--<a href="{{ route('admin.users.sales') }}" class="btn btn-secondary">Загрузить продажи</a>
    <a href="{{ route('admin.users.purchase') }}" class="btn btn-success">Загрузить снабжение</a>
    <a href="{{ route('admin.users.logist') }}" class="btn btn-warning">Загрузить логистику</a>
    <a href="{{ route('admin.users.accountant') }}" class="btn btn-primary">Загрузить бухгалтерию</a></p>-->

    <div class="card mb-3">
        <div class="card-header">Поиск</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">ID</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Имя</label>
                            <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">Статус</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value => $label)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="role" class="col-form-label">Роль</label>
                            <select id="role" class="form-control" name="role">
                                <option value=""></option>
                                @foreach ($roles as $value => $label)
                                    <option value="{{ $value }}"{{ $value === request('role') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>B24</th>
            <th>Имя</th>
            <th>Email</th>
            <th>Статус</th>
            <th>Роль</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->id_bitrix }}</td>
                <td>
                    @if($user->isOnline())
                        (<span style="color:green">on</span>) <a href="{{ route('admin.users.show', $user) }}">{{ $user->name }}</a>
                    @else
                        <a href="{{ route('admin.users.show', $user) }}">{{ $user->name }}</a> (<span style="color:red">off</span>)
                    @endif
                </td>
                <td>{{ $user->email }}</td>
                <td>
                    @if ($user->isWait())
                        <span class="badge badge-secondary">Waiting</span>
                    @endif
                    @if ($user->isActive())
                        <span class="badge badge-primary">Active</span>
                    @endif
                </td>
                <td>
                    @if ($user->isAdmin())
                        <span class="badge badge-danger">Админ</span>
                    @elseif ($user->isLogist())
                        <span class="badge badge-warning">Логист</span>
                    @elseif ($user->isSaleLogist())
                        <span class="badge badge-warning">Логист ОП</span>
                    @elseif ($user->isAccountant())
                        <span class="badge badge-primary">Бух-р</span>
                    @elseif ($user->isSale())
                        <span class="badge badge-secondary">Продажи</span>
                    @elseif ($user->isDirSale())
                        <span class="badge badge-secondary">Начальник ОП</span>
                    @elseif ($user->isDirPurchase())
                        <span class="badge badge-success">Начальник ОЗ</span>
                    @elseif ($user->isPurchase())
                        <span class="badge badge-success">Закупки</span>
                    @else
                        <span class="badge badge-light">User</span>
                    @endif
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    {{ $users->links() }}
@endsection