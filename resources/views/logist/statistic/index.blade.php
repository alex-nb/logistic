@extends('layouts.app', ['navigator' => 'Отдел логистики'])
@section('menu')
    @include('logist.statistic._vertical_nav')
@endsection
@section('content')
    <p>
        <div class="row">
            <div class="form-group">
                <label for="datefilter">Диапазон дат:</label>
                <input type="text"  name="datefilter"  id="datefilter" value="">
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary" id="check">Выбрать</button>
            </div>
        </div>
    </p>

    <div id="result"></div>
    <table class="table table-bordered table-hover table-striped" id="statistic" style="display: none">
        <thead>
            <tr id="mounth">
                <th>Показатель</th>
            </tr>
        </thead>
        <tbody>
            <tr id="all_in" title="Тип доставки: на склад, транзит"><td>Планировалось вывезти от поставщика</td></tr>
            <tr id="fact_in" title="Тип доставки: на склад, транзит"> <td>Фактически вывезено</td></tr>
            <tr id="ok_in" title="Тип доставки: на склад, транзит"><td>Вывезено в срок</td></tr>
            <tr id="late_in" title="Тип доставки: на склад, транзит"><td>Вывоз просрочен</td></tr>
            <tr id="no"><td></td></tr>
            <tr id="all_out" title="Тип доставки: со склада"><td>Планировалось доставок</td></tr>
            <tr id="fact_out" title="Тип доставки: со склада"><td>Фактически доставлено</td></tr>
            <tr id="ok_out" title="Тип доставки: со склада"><td>Доставлено в срок</td></tr>
            <tr id="late_out" title="Тип доставки: со склада"><td>Доставка просрочена</td></tr>
        </tbody>
    </table>


@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                $('input[name="datefilter"]').daterangepicker({
                    autoApply: true,
                    "locale": {
                        "format": "DD-MM-YYYY",
                        "daysOfWeek": [
                            "Вс",
                            "Пн",
                            "Вт",
                            "Ср",
                            "Чт",
                            "Пт",
                            "Сб"
                        ],
                        "monthNames": [
                            "Январь",
                            "Февраль",
                            "Март",
                            "Апрель",
                            "Май",
                            "Июнь",
                            "Июль",
                            "Август",
                            "Сентябрь",
                            "Октябрь",
                            "Ноябрь",
                            "Декабрь"
                        ],
                        "firstDay": 1
                    }
                });

                $('#check').on('click', function(e) {
                    e.preventDefault();
                    $.ajax({
                        type: 'POST',
                        url: '{{route('logist.statistic.index')}}',
                        data: {daterange:$('#datefilter').val(),_token:'{{ csrf_token() }}'},
                        success: function(data){
                            console.log(data);
                            if (data.length == 0) {
                                $("div#result").html("Данных за указанный срок не найдено");
                                $(".new").remove();
                                $("#statistic").css("display", "none");
                            }
                            else {
                                $("div#result").html("");
                                $(".new").remove();
                                $.each(data, function (index, value) {
                                    $("#mounth").append("<th class='new'>"+index+"</th>");
                                    if (typeof value['in'] !== "undefined" && typeof value['in']['all'] !== "undefined") {$("#all_in").append("<td class='new'>"+value['in']['all']+"</td>");}
                                    else {$("#all_in").append("<td class='new'>0</td>");}
                                    if (typeof value['in'] !== "undefined" && typeof value['in']['fact'] !== "undefined") {$("#fact_in").append("<td class='new'>"+value['in']['fact']+"</td>");}
                                    else {$("#fact_in").append("<td class='new'>0</td>");}
                                    if (typeof value['in'] !== "undefined" && typeof value['in']['ok'] !== "undefined") {$("#ok_in").append("<td class='new'>"+value['in']['ok']+"</td>");}
                                    else {$("#ok_in").append("<td class='new'>0</td>");}
                                    if (typeof value['in'] !== "undefined" && typeof value['in']['late'] !== "undefined") {$("#late_in").append("<td class='new'>"+value['in']['late']+"</td>");}
                                    else {$("#late_in").append("<td class='new'>0</td>");}
                                    if (typeof value['out'] !== "undefined" && typeof value['out']['all'] !== "undefined") {$("#all_out").append("<td class='new'>"+value['out']['all']+"</td>");}
                                    else {$("#all_out").append("<td class='new'>0</td>");}
                                    if (typeof value['out'] !== "undefined" && typeof value['out']['fact'] !== "undefined") {$("#fact_out").append("<td class='new'>"+value['out']['fact']+"</td>");}
                                    else {$("#fact_out").append("<td class='new'>0</td>");}
                                    if (typeof value['out'] !== "undefined" && typeof value['out']['ok'] !== "undefined") {$("#ok_out").append("<td class='new'>"+value['out']['ok']+"</td>");}
                                    else {$("#ok_out").append("<td class='new'>0</td>");}
                                    if (typeof value['out'] !== "undefined" && typeof value['out']['late'] !== "undefined") {$("#late_out").append("<td class='new'>"+value['out']['late']+"</td>");}
                                    else {$("#late_out").append("<td class='new'>0</td>");}
                                    $("#no").append("<td class='new'></td>");
                                });
                                $("#statistic").css("display", "block");
                            }
                        }
                    });
                });
            });
        })(jQuery);
    </script>
@endpush