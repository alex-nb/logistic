<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Print</title>
    <style type="text/css">
        table {
            width:100%;
            border: 1px solid black;
            border-collapse:collapse;
        }
        th {
            text-align: left; /* Выравнивание по левому краю */
            background: #D3D3D3; /* Цвет фона ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
        td {
            border: 1px solid black; /* Граница вокруг ячеек */
        }
    </style>

</head>
<body onload="setTimeout(function () { window.print(); }, 500); window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }">
</body>
</html>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>№</th>
        <th>Дата</th>
        <th>Менеджер</th>
        <th>Комментарий</th>
        <th>Тип перевозки</th>
        <th>Точка А</th>
        <th>Точка Б</th>
        <th>Расстояние</th>
        <th>Тоннаж(кг)</th>
        <th>Тип загрузки</th>
        <th>Товары</th>
        <th>ADR</th>
        <th>Упаковка</th>
        <th>Дата вывоза</th>
        <th>Поддоны</th>
        @if ($type == "archive" || $type == "problem_request" || $type == "request_car" || $type == "search" || $type == "delivery" ||
            $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Цена</th>
            <th>Комментарий</th>
            <th>Дата ответа</th>
            <th>Логист</th>
        @endif
        @if ($type == "problem_request")
            <th>Причина попадания</th>
        @endif
        @if ($type == "request_car" || $type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата запроса на перевозку</th>
            <th>Грузоотправитель</th>
            <th>Грузополучатель</th>
            <th>Менеджер</th>
            <th>№ заказа в 1С (поставщик)</th>
            <th>№ заказа в 1С (клиент)</th>
        @endif
        @if ($type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата начала поиска машины</th>
        @endif
        @if ($type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>№ в умной логистике</th>
            <th>Водитель</th>
            <th>Дата окончания поиска машины</th>
            <th>Дата ввода данных от зкакупки</th>
            <th>Дата прибытия к грузоотправителю</th>
            <th>Товары</th>
            <th>Дата ответа бухгалтерии</th>
            <th>Бухгалтер</th>
        @endif
        @if ($type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата прибытия машины</th>
        @endif
        @if ($type == "archive_complete")
            <th>Дата перемещения в архив</th>
        @endif
    </tr>
    </thead>
    <tbody>

    @foreach ($requests as $req)
        <tr>
            <td>{{ $req->id }}</td>
            <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
            <td>{{ $req->userCreate->name }}</td>
            <td>{{ $req->comment_create }}</td>

            <td>{{ $req->shipping->name }}</td>
            <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
            <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
            <td>{{ $req->distance }}</td>
            <td>{{ $req->tonnage }}</td>
            <td>{{ $req->typeLoad->name }}</td>
            <td>
                @foreach($req->products as $prod)
                    {{ $prod->name }} <br>
                @endforeach
            </td>
            <td>{{ $req->adr }}</td>
            <td>{{ $req->packaging->name }}</td>
            <td>
                @if (!empty($req->date_export))
                    {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                @endif
            </td>
            <td>
                @if ($req->pallet == 0)
                    Нет
                @else
                    Да
                @endif
            </td>
            @if ($type == "archive" || $type == "problem_request" || $type == "request_car" || $type == "search" || $type == "delivery" ||
            $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    {{ $req->price }}
                </td>
                <td>{{ $req->comment }}</td>
                <td>
                    @if (!empty($req->date_answer))
                        {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                    @endif
                </td>
                <td>
                    @if (isset($req->userLogist))
                        @if (!empty($req->userLogist->last_name))
                            {{$req->userLogist->last_name }},
                        @endif
                        {{$req->userLogist->name}}
                    @endif
                </td>
            @endif
            @if ($type == "problem_request")
                <td>{{ $req->comment_problem }}</td>
            @endif
            @if ($type == "request_car" || $type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['created_at']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['created_at'])) }}
                    @endif
                </td>
                <td>{{ $req->requestSaleLogistAccepted['contactShipper']['name'] }}<br>{{ $req->requestSaleLogistAccepted['contactShipper']['phones'] }}<br><i>{{ $req->requestSaleLogistAccepted['comment_shipper'] }}</i></td>
                <td>{{ $req->requestSaleLogistAccepted['contactConsignee']['name'] }}<br>{{ $req->requestSaleLogistAccepted['contactConsignee']['phones'] }}<br><i>{{ $req->requestSaleLogistAccepted['comment_consignee'] }}</i></td>
                <td>{{ $req->requestSaleLogistAccepted['name_manager'] }}</td>
                <td>{{ $req->requestSaleLogistAccepted['num_provider'] }}</td>
                <td>{{ $req->requestSaleLogistAccepted['num_customer'] }}</td>
            @endif
            @if ($type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_start_search']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_start_search'])) }}
                    @endif
                </td>
            @endif
            @if ($type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    {{ $req->requestSaleLogistAccepted['num_logistic'] }}
                </td>
                <td style="background: #c1e2b3;">{{ $req->requestSaleLogistAccepted['car']['driver']['name'] }},<br>
                    @if (!empty($req->requestSaleLogistAccepted['car']['driver']['birthday']))
                        {{ date("d.m.y", strtotime($req->requestSaleLogistAccepted['car']['driver']['birthday'])) }},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['driver']['series']))
                        Паспорт: {{$req->requestSaleLogistAccepted['car']['driver']['series']}},
                        {{$req->requestSaleLogistAccepted['car']['driver']['number']}},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['driver']['whom']))
                        выдан: {{$req->requestSaleLogistAccepted['car']['driver']['whom']}},
                        {{ date("d.m.y", strtotime($req->requestSaleLogistAccepted['car']['driver']['when'])) }},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['driver']['phone']))
                        #телефона: {{$req->requestSaleLogistAccepted['car']['driver']['phone']}},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['model']))
                        Машина: {{$req->requestSaleLogistAccepted['car']['model']}},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['colour']))
                        {{$req->requestSaleLogistAccepted['car']['colour']}},
                    @endif
                    @if (!empty($req->requestSaleLogistAccepted['car']['number']))
                        {{$req->requestSaleLogistAccepted['car']['number']}},
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_find_car']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_find_car'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_answer_sale_logist']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_answer_sale_logist'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_come_car_send']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_come_car_send'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['products']))
                        @foreach($req->requestSaleLogistAccepted['products'] as $product)
                            <b>{{ $product['name']  }} </b>, <i>{{ $product['pivot']['weight'] }} {{ $product['pivot']['unit'] }}</i> <br>
                        @endforeach
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_answer_buh']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_answer_buh'])) }}
                    @endif
                </td>
                <td>{{ $req->requestSaleLogistAccepted['accountant']['name'] }}</td>
            @endif
            @if ($type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_come_car_take']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_come_car_take'])) }}
                    @endif
                </td>
            @endif
            @if ($type == "archive_complete")
                <td>
                    @if (!empty($req->requestSaleLogistAccepted['date_archive']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestSaleLogistAccepted['date_archive'])) }}
                    @endif
                </td>
            @endif
        </tr>
    @endforeach

    </tbody>
</table>