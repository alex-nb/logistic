<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Print</title>
    <style type="text/css">
        table {
            width:100%;
            border: 1px solid black;
            border-collapse:collapse;
        }
        th {
            text-align: left; /* Выравнивание по левому краю */
            background: #D3D3D3; /* Цвет фона ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
        td {
            border: 1px solid black; /* Граница вокруг ячеек */
        }
    </style>

</head>
<body onload="setTimeout(function () { window.print(); }, 500); window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }">
<table>
    <thead>
    <tr>
        <th>№</th>
        <th>Дата</th>
        <th>Менеджер</th>
        <th>Комментарий</th>
        <th>Тип перевозки</th>
        <th>Точка А</th>
        <th>Точка Б</th>
        <th>Расстояние</th>
        <th>Тоннаж(кг)</th>
        <th>Тип загрузки</th>
        <th>Товары</th>
        <th>ADR</th>
        <th>Упаковка</th>
        <th>Дата вывоза</th>
        <th>Поддоны</th>
        @if ($type == "archive" || $type == "problem_request" || $type == "request_car" || $type == "search" || $type == "delivery" ||
            $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Цена</th>
            <th>Комментарий</th>
            <th>Дата ответа</th>
            <th>Логист</th>
        @endif
        @if ($type == "problem_request")
            <th>Причина попадания</th>
        @endif
        @if ($type == "request_car" || $type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата запроса на перевозку</th>
            <th>Грузоотправитель</th>
            <th>Грузополучатель</th>
            <th>Менеджер</th>
            <th>№ заказа в 1С (поставщик)</th>
            <th>№ заказа в 1С (клиент)</th>
        @endif
        @if ($type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата начала поиска машины</th>
        @endif
        @if ($type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>№ в умной логистике</th>
            <th>Водитель</th>
            <th>Дата окончания поиска машины</th>
            <th>Дата ввода данных от зкакупки</th>
            <th>Дата прибытия к грузоотправителю</th>
            <th>Товары</th>
            <th>Дата ответа бухгалтерии</th>
            <th>Бухгалтер</th>
        @endif
        @if ($type == "check_delivery" || $type == "payment" || $type == "archive_complete")
            <th>Дата прибытия машины</th>
        @endif
        @if ($type == "archive_complete")
            <th>Дата перемещения в архив</th>
        @endif
    </tr>
    </thead>
    <tbody>

    @foreach ($requests as $req)
        <tr>
            <td>{{ $req->id }}</td>
            <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
            <td>{{ $req->userCreate->name }}</td>
            <td>{{ $req->comment_create }}</td>

            <td>{{ $req->shipping->name }}</td>
            <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
            <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
            <td>{{ $req->distance }}</td>
            <td>{{ $req->tonnage }}</td>
            <td>{{ $req->typeLoad->name }}</td>
            <td>
                @foreach($req->products as $prod)
                    {{ $prod->name }} <br>
                @endforeach
            </td>
            <td>{{ $req->adr }}</td>
            <td>{{ $req->packaging->name }}</td>
            <td>
                @if (!empty($req->date_export))
                    {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                @endif
            </td>
            <td>
                @if ($req->pallet == 0)
                    Нет
                @else
                    Да
                @endif
            </td>
            @if ($type == "archive" || $type == "problem_request" || $type == "request_car" || $type == "search" || $type == "delivery" ||
            $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    {{ $req->price }}
                </td>
                <td>{{ $req->comment }}</td>
                <td>
                    @if (!empty($req->date_answer))
                        {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                    @endif
                </td>
                <td>
                    @if (isset($req->userLogist))
                        @if (!empty($req->userLogist->last_name))
                            {{$req->userLogist->last_name }},
                        @endif
                        {{$req->userLogist->name}}
                    @endif
                </td>
            @endif
            @if ($type == "problem_request")
                <td>{{ $req->comment_problem }}</td>
            @endif
            @if ($type == "request_car" || $type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestPurchaseAccepted['created_at']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['created_at'])) }}
                    @endif
                </td>
                <td>{{ $req->requestPurchaseAccepted['contactShipper']['name'] }}<br>{{ $req->requestPurchaseAccepted['contactShipper']['phones'] }}<br><i>{{ $req->requestPurchaseAccepted['comment_shipper'] }}</i></td>
                <td>{{ $req->requestPurchaseAccepted['contactConsignee']['name'] }}<br>{{ $req->requestPurchaseAccepted['contactConsignee']['phones'] }}<br><i>{{ $req->requestPurchaseAccepted['comment_consignee'] }}</i></td>
                <td>{{ $req->requestPurchaseAccepted['name_manager'] }}</td>
                <td>{{ $req->requestPurchaseAccepted['num_provider'] }}</td>
                <td>{{ $req->requestPurchaseAccepted['num_customer'] }}</td>
            @endif
            @if ($type == "search" || $type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_start_search']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_start_search'])) }}
                    @endif
                </td>
            @endif
            @if ($type == "delivery" || $type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    {{ $req->requestPurchaseAccepted['num_logistic'] }}
                </td>
                <td>{{ $req->requestPurchaseAccepted['car']['driver']['name'] }},<br>
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['birthday']))
                        {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['birthday'])) }},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['series']))
                        Паспорт: {{$req->requestPurchaseAccepted['car']['driver']['series']}},
                        {{$req->requestPurchaseAccepted['car']['driver']['number']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['whom']))
                        выдан: {{$req->requestPurchaseAccepted['car']['driver']['whom']}},
                        {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['when'])) }},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['phone']))
                        #телефона: {{$req->requestPurchaseAccepted['car']['driver']['phone']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['model']))
                        Машина: {{$req->requestPurchaseAccepted['car']['model']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['colour']))
                        {{$req->requestPurchaseAccepted['car']['colour']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['number']))
                        {{$req->requestPurchaseAccepted['car']['number']}},
                    @endif
                </td>

                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_find_car']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_find_car'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_answer_purchase']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_answer_purchase'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_come_car_send']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_come_car_send'])) }}
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestPurchaseAccepted['products']))
                        @foreach($req->requestPurchaseAccepted['products'] as $product)
                            <b>{{ $product['name']  }} </b>, <i>{{ $product['pivot']['weight'] }} {{ $product['pivot']['unit'] }}</i> <br>
                        @endforeach
                    @endif
                </td>
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_answer_buh']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_answer_buh'])) }}
                    @endif
                </td>
                <td>{{ $req->requestPurchaseAccepted['accountant']['name'] }}</td>
            @endif
            @if ($type == "check_delivery" || $type == "payment" || $type == "archive_complete")
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_come_car_take']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_come_car_take'])) }}
                    @endif
                </td>
            @endif
            @if ($type == "archive_complete")
                <td>
                    @if (!empty($req->requestPurchaseAccepted['date_archive']))
                        {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_archive'])) }}
                    @endif
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
