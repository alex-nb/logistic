@component('modal')
    @slot('name', 'delivery-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'logist.not_registry.purchase.check_delivery.file')
    @slot('enctype', 'enctype=multipart/form-data')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_req_ac" name="id_req_ac">
        <input type="hidden"  id="summ" name="summ">

        <div class="form-group">
            <label for="file_shipment">Подтверждающий документ</label>
            <input id="file_shipment" class="form-control" name="file_shipment[]" type="file" multiple>
        </div>

        <div class="form-group">
            <label for="file_sf">Счет фактура</label>
            <input id="file_sf" class="form-control" name="file_sf[]" type="file" multiple>
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent