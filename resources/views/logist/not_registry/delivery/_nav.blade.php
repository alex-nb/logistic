<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.new.sale') }}">Новые запросы</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.request_car.index') }}">Запросы на поиск машины</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.search.index') }}">Поиск машины</a></li>
    <li class="nav-item"><a class="nav-link active" href="{{ route('logist.not_registry.purchase.delivery.index') }}">Доставка груза</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.check_delivery.index') }}">Подтверждение доставки</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.payment.index') }}">Подтверждение оплаты</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.archive_complete.index') }}">Архив реализованных запросов</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.archive.sale') }}">Архив запросов</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.purchase.problem_request.index') }}">Проблемные запросы</a></li>
</ul>