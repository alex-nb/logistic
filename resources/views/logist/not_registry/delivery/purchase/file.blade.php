@component('modal')
    @slot('name', 'delivery-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'logist.not_registry.purchase.delivery.file')
    @slot('enctype', 'enctype=multipart/form-data')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_req_ac" name="id_req_ac">

        <div class="form-group">
            <label for="file_act">Акт</label>
            <input id="file_act" class="form-control" name="file_act[]" multiple type="file">
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent