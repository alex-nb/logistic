@extends('layouts.app', ['navigator' => 'Отдел логистики'])
@section('menu')
    @include('logist.not_registry._vertical_nav')
@endsection
@section('content')
    @include('logist.not_registry.archive._nav')

    <ul class="nav nav-tabs mb-2">
        <li class="nav-item"><a class="nav-link active" href="{{ route('logist.not_registry.archive.sale') }}">Продажи</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.archive.purchase') }}">Закупка</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.archive.sale_logist') }}">Продажи Логикар</a></li>
    </ul>

    @include('logist.not_registry.search_sale',['type_load' => $type_load])

    @if(!$search)
    <p>
    <div class="row">
        <div class="col-md-1">
            <form method="POST" action="{{ route('logist.not_registry.archive.sale') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="2">
                <button class="btn btn-primary">Все запросы</button>
            </form>
        </div>
        <div class="col-md-1">
            <form method="POST" action="{{ route('logist.not_registry.archive.sale') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="1">
                <button class="btn btn-primary">Только мои</button>
            </form>
        </div>
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('logist.not_registry.sale.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="my" name="my" value="{{$sort}}">
                <input type="hidden"  id="page" name="page" value="archive">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    </div>
    </p>
    @endif


    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_logist = [1 =>'Новые запросы', 9=>'Архив запросов'])
            @else
                <th style="background: #c1e2b3;">Цена</th>
                <th style="background: #c1e2b3;">Комментарий</th>
                <th style="background: #c1e2b3;">Дата ответа</th>
                <th style="background: #c1e2b3;">Логист</th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                @if ($search && $status)
                    <td>{{ $status_logist[$req->id_status_logist] }}</td>
                @else
                    <td style="background: #c1e2b3;">{{ $req->price }}</td>
                    <td style="background: #c1e2b3;">{{ $req->comment }}</td>
                    <td style="background: #c1e2b3;">
                        @if (!empty($req->date_answer))
                            {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                        @endif
                    </td>
                    <td style="background: #c1e2b3;">
                        @if (isset($req->userLogist))
                            @if (!empty($req->userLogist->last_name))
                                {{$req->userLogist->last_name }},
                            @endif
                            {{$req->userLogist->name}}
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->appends(array('my' => $sort))->links() }}
    @endif

@endsection