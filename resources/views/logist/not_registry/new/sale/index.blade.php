@extends('layouts.app', ['navigator' => 'Отдел логистики'])
@section('menu')
    @include('logist.not_registry._vertical_nav')
@endsection
@section('content')
    @include('logist.not_registry.new._nav')
    @include('logist.not_registry.new.sale.answer')

    <ul class="nav nav-tabs mb-2">
        <li class="nav-item"><a class="nav-link active" href="{{ route('logist.not_registry.new.sale') }}">Продажи</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.new.purchase') }}">Закупка</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.new.sale_logist') }}">Продажи Логикар</a></li>
    </ul>

    @include('logist.not_registry.search_sale',['type_load' => $type_load])
    @if (!$search)
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('logist.not_registry.sale.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="page" name="page" value="new">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    @endif
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_logist = [1 =>'Новые запросы', 9=>'Архив запросов'])
            @else
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $key=>$req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                @if ($search && $status)
                    <td>{{ $status_logist[$req->id_status_logist] }}</td>
                @else
                    <td>
                        <p>
                            <button
                                    type="button"
                                    class="btn btn-success"
                                    onclick="modal_data({{$req->id}}, {{$req->distance}})">
                                Дать ответ
                            </button>
                        </p>
                    </td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->links() }}
    @endif
@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_data (id, distance) {
            var type = 'sale';
            $("#id_request").val(id);
            $("#type").val(type);
            $("#distance").val(distance);
            $('#logist-answer-modal').modal('show');
        };
    </script>
@endpush