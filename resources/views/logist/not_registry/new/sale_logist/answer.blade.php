@component('modal')
    @slot('name', 'logist-answer-modal')
    @slot('title', 'Ответ для ОП')
    @slot('route', 'logist.not_registry.sale_logist.new.answer')
    @slot('enctype', '')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">

        <div class="form-group">
            <label for="price">Цена</label>
            <input id="price" type="number" class="form-control" name="price" placeholder="Введите цену" required autofocus>
        </div>

        <div class="form-group">
            <label for="distance">Километраж</label>
            <input id="distance" required class="form-control" name="distance" type="number">
        </div>

        <div class="form-group">
            <label for="comment">Комментарии</label>
            <textarea rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500"></textarea>
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Дать цену</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent