@extends('layouts.app', ['navigator' => 'Отдел логистики'])
@section('menu')
    @include('logist.not_registry._vertical_nav')
@endsection
@section('content')
    @include('logist.not_registry.new._nav')
    @include('logist.not_registry.new.sale_logist.answer')
    @include('logist.not_registry.history')
    <ul class="nav nav-tabs mb-2">
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.new.sale') }}">Продажи</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.new.purchase') }}">Закупка</a></li>
        <li class="nav-item"><a class="nav-link active" href="{{ route('logist.not_registry.new.sale_logist') }}">Продажи Логикар</a></li>
    </ul>
    @include('logist.not_registry.search_sale_logist',['shipping' => $shipping, 'package' => $package])

    @if (!$search)
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('logist.not_registry.sale_logist.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="page" name="page" value="new">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    @endif

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Тип перевозки</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            <th>Товары</th>
            <th>ADR</th>
            <th>Упаковка</th>
            <th>Дата вывоза</th>
            <th>Поддоны</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_logist = [8=>'Новые запросы', 2=>'Запросы на поиск машины', 3=>'Поиск машины', 5=>'Доставка груза', 12=>'Подтверждение доставки', 6=>'Подтверждение оплаты',
                4=>'Архив реализованных запросов', 7=>'Архив запросов', 11=>'Проблемные запросы'])
            @else
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->shipping->name }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                <td>
                    @foreach($req->products as $prod)
                        {{ $prod->name }} <br>
                    @endforeach
                </td>
                <td>{{ $req->adr }}</td>
                <td>{{ $req->packaging->name }}</td>
                <td>
                       @if (!empty($req->date_export))
                        {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                    @endif
                 </td>
                <td>
                    @if ($req->pallet == 0)
                        Нет
                    @else
                        Да
                    @endif
                </td>
                @if ($search && $status)
                    <td>{{ $status_logist[$req->id_status_logist] }}</td>
                @else
                    <td>
                        <p>
                            <button
                                    type="button"
                                    class="btn btn-success"
                                    onclick="modal_data({{$req->id}}, {{$req->distance}})">
                                Дать ответ
                            </button>
                        </p>
                        @if(!$req->requestHistory->isEmpty() && $req->requestHistory->count() > 1)
                            <p>
                                <button
                                        type="button"
                                        class="btn btn-primary"
                                        onclick="modal_history({{$req->requestHistory}})">
                                    История цен
                                </button>
                            </p>
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->links() }}
    @endif
@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_data (id, distance) {
            $("#distance").val(distance);
            $("#id_request").val(id);
            $('#logist-answer-modal').modal('show');
        };
        function modal_history (history) {
            history.pop();
            var html =
                '<table class="table table-bordered table-striped">'+
                '<thead>'+
                '<tr>'+
                '<th>Дата вывоза</th>'+
                '<th>Цена</th>'+
                '<th>Комментарий</th>'+
                '<th>Логист</th>'+
                '<th>Дата ответа</th>'+
                '</tr>'+
                '</thead>'+
                '<tbody>';
            $.each(history,function(index,value){
                html += '<tr>';
                if (value.date_export !== null) {
                    html += '<td>'+value.date_export+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.price !== null) {
                    html += '<td>'+value.price+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.comment_logist !== null) {
                    html += '<td>'+value.comment_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.logist !== null) {
                    html += '<td>'+value.logist.name+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.date_answer_logist !== null) {
                    html += '<td>'+value.date_answer_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                html += '</tr>';
            });
            html += '</tbody></table>';
            $("div#show_history").html(html);
            $('#history-modal').modal('show');
        };
    </script>
@endpush