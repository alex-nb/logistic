@extends('layouts.app', ['navigator' => 'Отдел логистики'])
@section('menu')
    @include('logist.not_registry._vertical_nav')
@endsection
@section('content')
    @include('logist.not_registry.archive_complete._nav')
    @include('logist.not_registry.history')

    <ul class="nav nav-tabs mb-2">
        <li class="nav-item"><a class="nav-link active" href="{{ route('logist.not_registry.purchase.archive_complete.index') }}">Закупка</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('logist.not_registry.sale_logist.archive_complete.index') }}">Продажи Логикар</a></li>
    </ul>
    @include('logist.not_registry.search_purchase',['shipping' => $shipping, 'package' => $package])
    @if(!$search)
    <p>
    <div class="row">
        <div class="col-md-1">
            <form method="POST" action="{{ route('logist.not_registry.purchase.archive_complete.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="2">
                <button class="btn btn-primary">Все запросы</button>
            </form>
        </div>
        <div class="col-md-1">
            <form method="POST" action="{{ route('logist.not_registry.purchase.archive_complete.index') }}">
                @csrf
                <input type="hidden"  id="my" name="my" value="1">
                <button class="btn btn-primary">Только мои</button>
            </form>
        </div>
        <div class="col-md-1">
            <form id="print" method="POST" action="{{ route('logist.not_registry.purchase.print') }}" target="_blank">
                @csrf
                <input type="hidden"  id="my" name="my" value="{{$sort}}">
                <input type="hidden"  id="page" name="page" value="archive_complete">
                <h2><span style="cursor: pointer;" onclick="$('#print').submit();" data-toggle='tooltip' title='Вывести на печать'>&#128424;</span></h2>
            </form>
        </div>
    </div>
    </p>
    @endif
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Тип перевозки</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Расстояние</th>
            <th>Тоннаж(кг)</th>
            <th>Тип загрузки</th>
            <th>Товары</th>
            <th>ADR</th>
            <th>Упаковка</th>
            <th>Дата вывоза</th>
            <th>Поддоны</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_logist = [8=>'Новые запросы', 2=>'Запросы на поиск машины', 3=>'Поиск машины', 5=>'Доставка груза', 12=>'Подтверждение доставки', 6=>'Подтверждение оплаты',
                4=>'Архив реализованных запросов', 7=>'Архив запросов', 11=>'Проблемные запросы'])
            @else
                <th style="background: #c1e2b3;">Цена</th>
                <th style="background: #c1e2b3;">Комментарий</th>
                <th style="background: #c1e2b3;">Дата ответа</th>
                <th style="background: #c1e2b3;">Логист</th>
                <th>Дата запроса на перевозку</th>
                <th>Грузоотправитель</th>
                <th>Грузополучатель</th>
                <th>Менеджер</th>
                <th>№ заказа в 1С (поставщик)</th>
                <th>№ заказа в 1С (клиент)</th>
                <th style="background: #c1e2b3;">Дата начала поиска машины</th>
                <th style="background: #c1e2b3;">№ в умной логистике</th>
                <th style="background: #c1e2b3;">Водитель</th>
                <th style="background: #c1e2b3;">Дата окончания поиска машины</th>
                <th style="background: #c1e2b3;">Счет</th>
                <th>Дата ввода данных от зкакупки</th>
                <th>Доверенность</th>
                <th>Дата прибытия к грузоотправителю</th>
                <th>Товары</th>
                <th>Отгрузочные документы</th>
                <th style="background: #87CEEB;">ТОРГ-12 (без подп)</th>
                <th style="background: #87CEEB;">ТОРГ-12 (подп)</th>
                <th style="background: #87CEEB;">Счет-фактура</th>
                <th style="background: #87CEEB;">ТНН</th>
                <th style="background: #87CEEB;">Дата ответа бухгалтерии</th>
                <th style="background: #87CEEB;">Бухгалтер</th>
                <th>Дата прибытия машины</th>
                <th style="background: #c1e2b3;">Подтверждающий документ</th>
                <th style="background: #c1e2b3;">Счет-фактура</th>
                <th style="background: #c1e2b3;">Акт</th>
                <th>Дата перемещения в архив</th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
               <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->shipping->name }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->distance }}</td>
                <td>{{ $req->tonnage }}</td>
                <td>{{ $req->typeLoad->name }}</td>
                <td>
                    @foreach($req->products as $prod)
                        {{ $prod->name }} <br>
                    @endforeach
                </td>
                <td>{{ $req->adr }}</td>
                <td>{{ $req->packaging->name }}</td>
                <td>
                       @if (!empty($req->date_export))
                        {{ date("d.m.y H:i:s", strtotime($req->date_export)) }}
                    @endif
                 </td>
                <td>
                    @if ($req->pallet == 0)
                        Нет
                    @else
                        Да
                    @endif
                </td>
                @if ($search && $status)
                    <td>{{ $status_logist[$req->id_status_logist] }}</td>
                @else
                    <td style="background: #c1e2b3;">
                        {{ $req->price }}
                        @if(!$req->requestHistory->isEmpty() && $req->requestHistory->count() > 1)
                            <p><h5><span style="cursor:pointer;" title="История цен" onclick="modal_history({{$req->requestHistory}})">&#128193;</span></h5></p>
                        @endif
                    </td>
                    <td style="background: #c1e2b3;">{{ $req->comment }}</td>
                    <td style="background: #c1e2b3;">
                            @if (!empty($req->date_answer))
                            {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                        @endif
                     </td>
                    <td style="background: #c1e2b3;">
                        @if (isset($req->userLogist))
                            @if (!empty($req->userLogist->last_name))
                                {{$req->userLogist->last_name }},
                            @endif
                            {{$req->userLogist->name}}
                        @endif
                    </td>
                    <td>
                            @if (!empty($req->requestPurchaseAccepted['created_at']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['created_at'])) }}
                        @endif
                     </td>
                    <td>{{ $req->requestPurchaseAccepted['contactShipper']['name'] }}<br>{{ $req->requestPurchaseAccepted['contactShipper']['phones'] }}<br><i>{{ $req->requestPurchaseAccepted['comment_shipper'] }}</i></td>
                    <td>{{ $req->requestPurchaseAccepted['contactConsignee']['name'] }}<br>{{ $req->requestPurchaseAccepted['contactConsignee']['phones'] }}<br><i>{{ $req->requestPurchaseAccepted['comment_consignee'] }}</i></td>
                    <td>{{ $req->requestPurchaseAccepted['name_manager'] }}</td>
                    <td>{{ $req->requestPurchaseAccepted['num_provider'] }}</td>
                    <td>{{ $req->requestPurchaseAccepted['num_customer'] }}</td>
                    <td style="background: #c1e2b3;">
                            @if (!empty($req->requestPurchaseAccepted['date_start_search']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_start_search'])) }}
                        @endif
                     </td>
                    <td style="background: #c1e2b3;">
                        {{ $req->requestPurchaseAccepted['num_logistic'] }}
                    </td>
                    <td style="background: #c1e2b3;">{{ $req->requestPurchaseAccepted['car']['driver']['name'] }},<br>
                      @if (!empty($req->requestPurchaseAccepted['car']['driver']['birthday']))
                             {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['birthday'])) }},
                         @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['driver']['series']))
                            Паспорт: {{$req->requestPurchaseAccepted['car']['driver']['series']}},
                         {{$req->requestPurchaseAccepted['car']['driver']['number']}},
                        @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['driver']['whom']))
                            выдан: {{$req->requestPurchaseAccepted['car']['driver']['whom']}},
                         {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['when'])) }},
                        @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['driver']['phone']))
                             #телефона: {{$req->requestPurchaseAccepted['car']['driver']['phone']}},
                         @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['model']))
                             Машина: {{$req->requestPurchaseAccepted['car']['model']}},
                          @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['colour']))
                             {{$req->requestPurchaseAccepted['car']['colour']}},
                          @endif
                         @if (!empty($req->requestPurchaseAccepted['car']['number']))
                             {{$req->requestPurchaseAccepted['car']['number']}},
                          @endif
                     </td>

                    <td style="background: #c1e2b3;">
                            @if (!empty($req->requestPurchaseAccepted['date_find_car']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_find_car'])) }}
                        @endif
                     </td>
                    <td style="background: #c1e2b3;">
                        @if (!empty($req->requestPurchaseAccepted['file_bill']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_bill']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td>
                           @if (!empty($req->requestPurchaseAccepted['date_answer_purchase']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_answer_purchase'])) }}
                        @endif
                     </td>
                    <td>
                        @if (!empty($req->requestPurchaseAccepted['file_proxy']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_proxy']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td>
                           @if (!empty($req->requestPurchaseAccepted['date_come_car_send']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_come_car_send'])) }}
                        @endif
                     </td>
                    <td>
                        @if (!empty($req->requestPurchaseAccepted['products']))
                            @foreach($req->requestPurchaseAccepted['products'] as $product)
                                <b>{{ $product['name']  }} </b>, <i>{{ $product['pivot']['weight'] }} {{ $product['pivot']['unit'] }}</i> <br>
                            @endforeach
                        @endif
                        @if (!empty($req->requestPurchaseAccepted['file_product']))
                                @php($array = explode("&", $req->requestPurchaseAccepted['file_product']))

                                @foreach ($array as $file)
                                    @if (strlen($file) > 0)
                                        <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                    @endif
                                @endforeach
                        @endif
                    </td>
                    <td>
                        @if (!empty($req->requestPurchaseAccepted['file_shipping']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_shipping']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #87CEEB;">
                        @if (!empty($req->requestPurchaseAccepted['file_torg']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_torg']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #87CEEB;">
                        @if (!empty($req->requestPurchaseAccepted['file_torg_sign']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_torg_sign']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #87CEEB;">
                        @if (!empty($req->requestPurchaseAccepted['file_invoice']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_invoice']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #87CEEB;">
                        @if (!empty($req->requestPurchaseAccepted['file_tnn']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_tnn']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #87CEEB;">
                       @if (!empty($req->requestPurchaseAccepted['date_answer_buh']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_answer_buh'])) }}
                       @endif
                     </td>
                    <td style="background: #87CEEB;">{{ $req->requestPurchaseAccepted['accountant']['name'] }}</td>
                    <td>
                           @if (!empty($req->requestPurchaseAccepted['date_come_car_take']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_come_car_take'])) }}
                        @endif
                     </td>
                    <td style="background: #c1e2b3;">
                        @if (!empty($req->requestPurchaseAccepted['file_shipment']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_shipment']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>

                    <td style="background: #c1e2b3;">
                        @if (!empty($req->requestPurchaseAccepted['file_sf']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_sf']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td style="background: #c1e2b3;">
                        @if (!empty($req->requestPurchaseAccepted['file_act']))
                            @php($array = explode("&", $req->requestPurchaseAccepted['file_act']))

                            @foreach ($array as $file)
                                @if (strlen($file) > 0)
                                    <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td>
                           @if (!empty($req->requestPurchaseAccepted['date_archive']))
                            {{ date("d.m.y H:i:s", strtotime($req->requestPurchaseAccepted['date_archive'])) }}
                        @endif
                     </td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->appends(array('my' => $sort))->links() }}
    @endif

@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_history (history) {
            history.pop();
            var html =
                '<table class="table table-bordered table-striped">'+
                '<thead>'+
                '<tr>'+
                '<th>Дата вывоза</th>'+
                '<th>Цена</th>'+
                '<th>Комментарий</th>'+
                '<th>Логист</th>'+
                '<th>Дата ответа</th>'+
                '</tr>'+
                '</thead>'+
                '<tbody>';
            $.each(history,function(index,value){
                html += '<tr>';
                if (value.date_export !== null) {
                    html += '<td>'+value.date_export+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.price !== null) {
                    html += '<td>'+value.price+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.comment_logist !== null) {
                    html += '<td>'+value.comment_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.logist !== null) {
                    html += '<td>'+value.logist.name+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                if (value.date_answer_logist !== null) {
                    html += '<td>'+value.date_answer_logist+'</td>';
                }
                else {
                    html += '<td></td>';
                }
                html += '</tr>';
            });
            html += '</tbody></table>';
            $("div#show_history").html(html);
            $('#history-modal').modal('show');
        };
    </script>
@endpush