@component('modal')
    @slot('name', 'history-modal')
    @slot('title', 'История цен')
    @slot('route', 'home')
    @slot('enctype', '')

        {!! csrf_field() !!}

    <div id="show_history"></div>


    @slot('buttons')
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    @endslot
@endcomponent