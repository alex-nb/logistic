@component('modal')
    @slot('name', 'search-driver-modal')
    @slot('title', 'Данные о водителе')
    @slot('route', 'logist.not_registry.sale_logist.search.driver')
    @slot('enctype', 'enctype=multipart/form-data')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_req_ac" name="id_req_ac">

        <div class="form-group">
            <label for="num_logistic">№ заявки в умной логистике</label>
            <input id="num_logistic" maxlength="50" class="form-control" name="num_logistic" type="text" placeholder="Введите № заявки в умной логистике">
        </div>

        <div class="form-group">
            <label for="name_driver">ФИО водителя</label>
            <input maxlength="255" id="name_driver" required class="form-control" name="name_driver" type="text" placeholder="Введите ФИО водителя">
        </div>

        <div class="form-group">
            <label for="birthday">Дата рождения</label>
            <input id="birthday" required class="form-control" name="birthday" type="date" placeholder="Выберите дату рождения водителя">
        </div>

        <div class="form-group">
            <label for="series">Серия паспорта</label>
            <input id="series" maxlength="100" required class="form-control" name="series" type="text" placeholder="Введите серию паспорта">
        </div>

        <div class="form-group">
            <label for="number">Номер паспорта</label>
            <input id="number" maxlength="100" required class="form-control" name="number" type="text" placeholder="Введите номер паспорта">
        </div>

        <div class="form-group">
            <label for="whom">Кем выдан паспорт</label>
            <input maxlength="100" id="whom" required class="form-control" name="whom" type="text" placeholder="Кем выдан паспорт">
        </div>

        <div class="form-group">
            <label for="when">Когда выдан паспорт</label>
            <input id="when" required class="form-control" name="when" type="date" placeholder="Когда выдан паспорт">
        </div>

        <div class="form-group">
            <label for="model">Марка/модель автомобиля</label>
            <input maxlength="50" id="model" required class="form-control" name="model" type="text" placeholder="Автомобиль">
        </div>

        <div class="form-group">
            <label for="phone">Номер телефона</label>
            <input maxlength="255" id="phone" required class="form-control" name="phone" type="text" placeholder="Номер телефона">
        </div>

        <div class="form-group">
            <label for="gosnomer">Госномер автомобиля</label>
            <input maxlength="100" id="gosnomer" required class="form-control" name="gosnomer" type="text" placeholder="Госномер">
        </div>

        <div class="form-group">
            <label for="file_bill">Счет</label>
            <input id="file_bill" class="form-control" name="file_bill[]" required type="file" multiple>
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Ввести данные</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent