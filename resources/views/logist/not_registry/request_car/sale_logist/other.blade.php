@component('modal')
    @slot('name', 'request_car-other-modal')
    @slot('title', 'Примечания')
    @slot('route', 'logist.not_registry.sale_logist.request_car.other')
    @slot('enctype', '')

        {!! csrf_field() !!}

        <input type="hidden"  id="id_request" name="id_request">
        <input type="hidden"  id="id_req_ac" name="id_req_ac">
        <input type="hidden"  id="type" name="type">

        <div class="form-group" style="display: none" id="price_div">
            <label for="price">Новая цена</label>
            <input maxlength="20" id="price" class="form-control" name="price" type="text" placeholder="Введите новую цену">
        </div>

        <div class="form-group">
            <label for="comment">Комментарии</label>
            <textarea required rows="3" id="comment" class="form-control" name="comment" placeholder="Комментарии"  maxlength="500"></textarea>
        </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Отправить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent