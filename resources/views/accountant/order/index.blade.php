@extends('layouts.app', ['navigator' => 'Бухгалтерия'])

@section('content')
    @include('accountant.order._nav')
    @include('accountant.order.num')
    @include('accountant.search')

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Комментарий</th>
            <th>Тип перевозки</th>
            <th>Точка А</th>
            <th>Точка Б</th>
            <th>Тоннаж(кг)</th>
            <th>Товары</th>
            <th style="background: #c1e2b3;">Цена</th>
            <th style="background: #c1e2b3;">Комментарий</th>
            <th style="background: #c1e2b3;">Дата ответа</th>
            <th style="background: #c1e2b3;">Логист</th>
            <th>Менеджер</th>
            <th>№ заказа в 1С (поставщик)</th>
            <th>№ заказа в 1С (клиент)</th>
            <th style="background: #c1e2b3;">Водитель</th>
            <th style="background: #c1e2b3;">Счет</th>
            @if ($search && $status)
                <th>Статус</th>
                @php($status_acc = [3=>'Создать поступление в УТ', 1=>'Создать отгрузочные док. в УТ', 2=>'Архив'])
            @else
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach ($requests as $req)
            <tr>
                <td>{{ $req->id }}</td>
                <td>{{ date("d.m.y H:i:s", strtotime($req->created_at)) }}</td>
                <td>{{ $req->userCreate->name }}</td>
                <td>{{ $req->comment_create }}</td>

                <td>{{ $req->shipping->name }}</td>
                <td>{{ $req->farmsAddressA->name }}, {{ $req->farmsAddressA->address }}</td>
                <td>{{ $req->farmsAddressB->name }}, {{ $req->farmsAddressB->address }}</td>
                <td>{{ $req->tonnage }}</td>

                <td>
                    @foreach($req->products as $prod)
                        {{ $prod->name }} <br>
                    @endforeach
                </td>
                <td style="background: #c1e2b3;">{{ $req->price }}</td>
                <td style="background: #c1e2b3;">{{ $req->comment }}</td>
                <td style="background: #c1e2b3;">
                    @if (!empty($req->date_answer))
                        {{ date("d.m.y H:i:s", strtotime($req->date_answer)) }}
                    @endif
                </td>
                <td style="background: #c1e2b3;">
                    @if (isset($req->userLogist))
                        @if (!empty($req->userLogist->last_name))
                            {{$req->userLogist->last_name }},
                        @endif
                        {{$req->userLogist->name}}
                    @endif
                </td>
                <td>{{ $req->requestPurchaseAccepted['name_manager'] }}</td>
                <td>{{ $req->requestPurchaseAccepted['num_provider'] }}</td>
                <td>{{ $req->requestPurchaseAccepted['num_customer'] }}</td>
                <td style="background: #c1e2b3;">{{ $req->requestPurchaseAccepted['car']['driver']['name'] }},<br>
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['birthday']))
                        {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['birthday'])) }},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['series']))
                        Паспорт: {{$req->requestPurchaseAccepted['car']['driver']['series']}},
                        {{$req->requestPurchaseAccepted['car']['driver']['number']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['whom']))
                        выдан: {{$req->requestPurchaseAccepted['car']['driver']['whom']}},
                        {{ date("d.m.y", strtotime($req->requestPurchaseAccepted['car']['driver']['when'])) }},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['driver']['phone']))
                        #телефона: {{$req->requestPurchaseAccepted['car']['driver']['phone']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['model']))
                        Машина: {{$req->requestPurchaseAccepted['car']['model']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['colour']))
                        {{$req->requestPurchaseAccepted['car']['colour']}},
                    @endif
                    @if (!empty($req->requestPurchaseAccepted['car']['number']))
                        {{$req->requestPurchaseAccepted['car']['number']}},
                    @endif
                </td>
                <td style="background: #c1e2b3;">
                    @if (!empty($req->requestPurchaseAccepted['file_bill']))
                        @php($array = explode("&", $req->requestPurchaseAccepted['file_bill']))
                        @foreach ($array as $file)
                            @if (strlen($file) > 0)
                                <a href="download/{{$file}}" target="_blank"><h3>&#128193;</h3></a>
                            @endif
                        @endforeach
                    @endif
                </td>
                @if ($search && $status)
                    <td>{{ isset($status_acc[$req->requestPurchaseAccepted['id_status_accountant']]) ? $status_acc[$req->requestPurchaseAccepted['id_status_accountant']] : 'Создать заказ в УТ' }}</td>
                @else
                    <td>
                        <p>
                            <button
                                    type="button"
                                    class="btn btn-success"
                                    onclick="modal_data({{$req->requestPurchaseAccepted['id']}})">
                                Ввести номер
                            </button>
                        </p>
                    </td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>

    @if ($requests instanceof \Illuminate\Pagination\AbstractPaginator)
        {{ $requests->links() }}
    @endif

@endsection

@push('scripts')
    <script>
        jQuery.noConflict();
        function modal_data (id_request_accepted) {
            $("#id_request_accepted").val(id_request_accepted);
            $('#accountant-file-modal').modal('show');
        };
    </script>
@endpush