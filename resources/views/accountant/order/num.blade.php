@component('modal')
    @slot('name', 'accountant-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'accountant.order.num')
    @slot('enctype', '')

    {!! csrf_field() !!}

    <input type="hidden"  id="id_request_accepted" name="id_request_accepted">

    <div class="form-group">
        <label for="num">№ заказа в 1С</label>
        <input id="num" required class="form-control" name="num" type="text" placeholder="Введите № заказа в 1с для логистов" maxlength="50">
    </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent