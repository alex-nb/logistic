<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link active" href="{{ route('accountant.order.index') }}">Создать заказ в УТ</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('accountant.arrival.index') }}">Создать поступление в УТ</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('accountant.loading.index') }}">Создать отгрузочные док. в УТ</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('accountant.archive.index') }}">Архив</a></li>
</ul>