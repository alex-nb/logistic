<div class="card mb-2" id="accordion">
    <!-- 1 панель -->

        <!-- Заголовок 1 панели -->
        <div class="card-header">
            <h4 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" data-parent="#accordion">
                    Поиск
                </button>
            </h4>
        </div>
        <div id="collapseOne" class="card-body collapse">
            <!-- Содержимое 1 панели -->

                <form action="?" method="GET">
                    <div class="row">
                        <div class="col-sm-1 form-group">
                            <label for="id" class="col-form-label">№</label>
                            <input id="id" type="number" class="form-control" name="id" value="{{ request('id') }}">
                        </div>

                        <div class="col-sm-1 form-group">
                            <label for="datecreate" class="col-form-label">Дата создания</label>
                            <input id="datecreate" class="form-control daterange" name="datecreate"
                                   @if(!empty(request('datecreate')))
                                   value="{{ date('d-m-Y', strtotime(request('datecreate'))) }}"
                                    @endif
                            >
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="num_post" class="col-form-label">№ заказа в 1С (поставщик)</label>
                            <input maxlength="50" id="num_post" class="form-control" name="num_post" value="{{ request('num_post') }}">
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="num_cli" class="col-form-label">№ заказа в 1С (клиент)</label>
                            <input maxlength="50" id="num_cli" class="form-control" name="num_cli" value="{{ request('num_cli') }}">
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="creator">Кто создал</label>
                            <input id="creator" name="creator" type="text"  autocomplete="off" class="typeahead form-control" maxlength="255" value="{{ request('creator') }}">
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="logist">Ответсвенный логист</label>
                            <input id="logist" name="logist" type="text"  autocomplete="off" class="typeahead form-control" maxlength="255" value="{{ request('logist') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="col-form-label">По совпадению</label>
                                <input maxlength="255" id="name" class="form-control" name="name" value="{{ request('name') }}">
                            </div>
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="field">В поле</label>
                            <select id="field" class="form-control" name="field">
                                <option value="pointA" {{ request('field') == "pointA" ? ' selected' : '' }}>Точка А</option>
                                <option value="pointB" {{ request('field') == "pointB" ? ' selected' : '' }}>Точка Б</option>
                                <option value="comment" {{ request('field') == "comment" ? ' selected' : '' }}>Комментарий (оз)</option>
                                <option value="products" {{ request('field') == "products" ? ' selected' : '' }}>Товары</option>
                                <option value="tonnage" {{ request('field') == "tonnage" ? ' selected' : '' }}>Тоннаж</option>
                                <option value="sender" {{ request('field') == "sender" ? ' selected' : '' }}>Грузоотправитель</option>
                                <option value="recipient" {{ request('field') == "recipient" ? ' selected' : '' }}>Грузополучатель</option>
                                <option value="driver" {{ request('field') == "driver" ? ' selected' : '' }}>Водитель</option>
                            </select>
                        </div>

                        <div class="col-sm-2 form-group">
                            <label for="status">Статус</label>
                            <select id="status" class="form-control" name="status">
                                <option value="this" {{ request('status') == "this" ? ' selected' : '' }}>По текущему</option>
                                <option value="all" {{ request('status') == "all" ? ' selected' : '' }}>По всем</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label">&nbsp;</label><br />
                                <button type="submit" class="btn btn-primary">Найти</button>
                            </div>
                        </div>

                    </div>
                </form>

        </div>

</div>

@push('scripts')
    <script>
        jQuery.noConflict();
        (function( $ ) {
            $(function() {
                var path = "{{ route('purchase.not_registry.autocomplete') }}";
                $('input.typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 3,
                    source:  function (query, process) {
                        return $.get(path, { query: query }, function (data) {
                            return process(data);
                        });
                    }
                });
                $('.daterange').daterangepicker({
                    singleDatePicker: true,
                    autoUpdateInput: false,
                    "locale": {
                        "cancelLabel" : "Отмена",
                        "applyLabel" : "Выбрать",
                        "daysOfWeek": [
                            "Вс",
                            "Пн",
                            "Вт",
                            "Ср",
                            "Чт",
                            "Пт",
                            "Сб"
                        ],
                        "monthNames": [
                            "Январь",
                            "Февраль",
                            "Март",
                            "Апрель",
                            "Май",
                            "Июнь",
                            "Июль",
                            "Август",
                            "Сентябрь",
                            "Октябрь",
                            "Ноябрь",
                            "Декабрь"
                        ],
                        "firstDay": 1
                    }
                });
                $('.daterange').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY'));
                });
                $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
            });
        })(jQuery);
    </script>
@endpush
