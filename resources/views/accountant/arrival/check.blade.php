@component('modal')
    @slot('name', 'accountant-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'accountant.arrival.check')
    @slot('enctype', 'enctype=multipart/form-data')

    {!! csrf_field() !!}

    <input type="hidden"  id="id_request_accepted" name="id_request_accepted">

    <div id="no_num">
    </div>

    <div class="text-center">
        <label for="ready">Поступление создано</label>
        <input id="ready" name="ready" type="checkbox" required>
    </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent