@component('modal')
    @slot('name', 'accountant-file-modal')
    @slot('title', 'Загрузка файлов')
    @slot('route', 'accountant.loading.file')
    @slot('enctype', 'enctype=multipart/form-data')

    {!! csrf_field() !!}

    <input type="hidden"  id="id_request" name="id_request">
    <input type="hidden"  id="id_request_accepted" name="id_request_accepted">
    <input type="hidden"  id="shipping" name="shipping">
    <input type="hidden"  id="id_manager" name="id_manager">

    <div class="form-group">
        <label for="torg_with">ТОРГ-12 с подписями</label>
        <input id="torg_with" class="form-control" name="torg_with[]" type="file" multiple>
    </div>

    <div class="form-group">
        <label for="torg_not">ТОРГ-12 без подписей</label>
        <input id="torg_not" class="form-control" name="torg_not[]" type="file" multiple>
    </div>

    <div class="form-group">
        <label for="invoice">Счет фактура</label>
        <input id="invoice" class="form-control" name="invoice[]" type="file" multiple>
    </div>

    <div class="form-group">
        <label for="tnn">ТНН</label>
        <input id="tnn" class="form-control" name="tnn[]" type="file" multiple>
    </div>

    @slot('buttons')
        <button type="submit" class="btn btn-success" id="save">Загрузить</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    @endslot
@endcomponent