<?php

namespace app\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $date_purchase = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
        $date_now = date("Y-m-d h:i:s");
        DB::table('request_purchase')->where([["id_status_purchase", 2],["updated_at", "<=", $date_purchase]])->update(["id_status_purchase" => 9, "date_archive" => $date_now]);
        $date_sale = date("Y-m-d", mktime(0, 0, 0, date('m') - 3, date('d'), date('Y')));
        DB::table('request_sale')->where([["id_status_sale", 2],["updated_at", "<=", $date_sale]])->update(["id_status_sale" => 3]);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
