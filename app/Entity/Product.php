<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class Product extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestPurchaseProducts()
    {
        return $this->hasMany(RequestPurchaseProducts::class, 'id_product');
    }

    public function registry()
    {
        return $this->hasMany(RegistryProduct::class, 'id_product');
    }

    public function requestPurchaseAcceptedProducts()
    {
        return $this->hasMany(RequestPurchaseAcceptedProducts::class, 'id_product');
    }

    public function requestSaleLogistProducts()
    {
        return $this->hasMany(RequestSaleLogistProducts::class, 'id_product');
    }

    public function requestSaleLogistAcceptedProducts()
    {
        return $this->hasMany(RequestSaleLogistAcceptedProducts::class, 'id_product');
    }
}
