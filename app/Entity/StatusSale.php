<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class StatusSale extends Model
{
    protected $table = 'status_sale';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];


    public function requestSale()
    {
        return $this->hasMany(RequestSale::class, 'id_status_sale');
    }

    public function registry()
    {
        return $this->hasMany(RegistryRequest::class, 'id_status_sale');
    }
}
