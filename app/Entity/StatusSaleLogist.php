<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class StatusSaleLogist extends Model
{
    protected $table = 'status_sale_logist';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_status_sale_logist');
    }

    public function requestSaleLogistAccepted()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_status_sale_logist');
    }
}
