<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $birthday
 * @property int $series
 * @property int $number
 * @property string $whom
 * @property string $when
 * @property string $phone
 */

class Driver extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'birthday', 'series', 'number', 'whom', 'when', 'name', 'phone'
    ];

    public function car()
    {
        return $this->belongsTo(Car::class, 'id_driver');
    }
}
