<?php

namespace app\Entity;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Cache;

/**
 * @property int $id
 * @property int $id_bitrix
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $verify_token
 * @property string $status
 * @property string $role
 */

class User extends Authenticatable
{
    use Notifiable;

    public const STATUS_WAIT = 'wait';
    public const STATUS_ACTIVE = 'active';

    public const ROLE_USER = 'user';
    public const ROLE_LOGIST = 'logist';
    public const ROLE_SALE_LOGIST = 'sale_logist';
    public const ROLE_SALE = 'sale';
    public const ROLE_PURCHASE = 'purchase';
    public const ROLE_ACCOUNTANT = 'accountant';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_DIRSALE = 'dirsale';
    public const ROLE_DIRPURCHASE = 'dirpurchase';

    protected $fillable = [
        'name', 'last_name', 'email', 'password', 'verify_token', 'status', 'role', 'id_bitrix'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function rolesList(): array
    {
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_LOGIST => 'Логист',
            self::ROLE_SALE_LOGIST => 'Логист ОП',
            self::ROLE_SALE => 'Менеджер продаж',
            self::ROLE_PURCHASE => 'Менеджер закупок',
            self::ROLE_ACCOUNTANT => 'Бухгалтер',
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_DIRSALE => 'Начальник ОП',
            self::ROLE_DIRPURCHASE => 'Начальник ОЗ',
        ];
    }


    public static function register(string $name, string $email, string $password): self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'verify_token' => Str::uuid(),
            'role' => self::ROLE_USER,
            'status' => self::STATUS_WAIT,
        ]);
    }

    public static function new($name, $email, $password, $id_bitrix, $role = NULL, $status = NULL): self
    {
        if (empty($role)) {
            $role = self::ROLE_USER;
        }
        if (empty($status)) {
            $status = self::STATUS_WAIT;
        }
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'role' => $role,
            'status' => $status,
            'id_bitrix' => $id_bitrix,
        ]);
    }

    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function verify(): void
    {
        if (!$this->isWait()) {
            throw new \DomainException('User is already verified.');
        }

        $this->update([
            'status' => self::STATUS_ACTIVE,
            'verify_token' => null,
        ]);
    }

    public function changeRole($role): void
    {
        if (!array_key_exists($role, self::rolesList())) {
            throw new \InvalidArgumentException('Undefined role "' . $role . '"');
        }
        if ($this->role === $role) {
            throw new \DomainException('Role is already assigned.');
        }
        $this->update(['role' => $role]);
    }

    public function changeIdBitrix($id_bitrix): void
    {
        if ($this->id_bitrix === $id_bitrix) {
            throw new \DomainException('ID is already assigned.');
        }
        $this->update(['id_bitrix' => $id_bitrix]);
    }

    public function isLogist(): bool
    {
        return $this->role === self::ROLE_LOGIST;
    }

    public function isSaleLogist(): bool
    {
        return $this->role === self::ROLE_SALE_LOGIST;
    }

    public function isSale(): bool
    {
        return $this->role === self::ROLE_SALE;
    }

    public function isDirSale(): bool
    {
        return $this->role === self::ROLE_DIRSALE;
    }

    public function isPurchase(): bool
    {
        return $this->role === self::ROLE_PURCHASE;
    }

    public function isDirPurchase(): bool
    {
        return $this->role === self::ROLE_DIRPURCHASE;
    }

    public function isAccountant(): bool
    {
        return $this->role === self::ROLE_ACCOUNTANT;
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function requestSaleCreate()
    {
        return $this->hasMany(RequestSale::class, 'id_create');
    }

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSale::class, 'id_logist');
    }

    public function requestPurchaseCreate()
    {
        return $this->hasMany(RequestPurchase::class, 'id_create');
    }

    public function requestPurchaseLogist()
    {
        return $this->hasMany(RequestPurchase::class, 'id_logist');
    }

    public function requestPurchaseAcceptedAccountant()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_accountant');
    }

    public function registryCommunication()
    {
        return $this->hasMany(RegistryCommunication::class, 'id_user');
    }

    public function registryRequest()
    {
        return $this->hasMany(RegistryRequest::class, 'id_create');
    }

    public function registryRequestPurchase()
    {
        return $this->hasMany(RegistryProduct::class, 'id_purchase');
    }

    public function registryReadMessage()
    {
        return $this->hasMany(RegistryReadMessage::class, 'id_user');
    }

    public function requestPurchaseHistoryLogist()
    {
        return $this->hasMany(RequestPurchaseAcceptedHistoryPrice::class, 'id_logist');
    }

    public function requestSaleLogistHistoryLogist()
    {
        return $this->hasMany(RequestSaleLogistAcceptedHistoryPrice::class, 'id_logist');
    }

    public function requestSaleLogistCreate()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_create');
    }

    public function requestSaleLogistLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_logist');
    }

    public function requestSaleLogistAcceptedAccountant()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_accountant');
    }

    public function historyUpdate()
    {
        return $this->hasMany(HistoryUpdate::class, 'id_create');
    }
}
