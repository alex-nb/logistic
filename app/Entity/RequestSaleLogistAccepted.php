<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id
 * @property int $id_accountant
 * @property string $file_shipping
 * @property int $id_shipper
 * @property string $comment_shipper
 * @property int $id_consignee
 * @property string $comment_consignee
 * @property int $bitrix_id_manager
 * @property string $name_manager
 * @property int $num_one_s
 * @property int $num_customer
 * @property int $num_provider
 * @property int $id_car
 * @property int $id_status_accountant
 * @property string $date_found_car
 * @property string $file_proxy
 * @property string $file_vet
 * @property string $date_answer_sale_logist
 * @property string $file_product
 * @property string $date_come_car_send
 * @property string $file_torg
 * @property string $file_torg_sign
 * @property string $file_invoice
 * @property string $file_tnn
 * @property string $date_answer_buh
 * @property string $file_shipment
 * @property string $file_bill
 * @property string $file_act
 * @property string $date_come_car_take
 * @property string $created_at
 * @property string $updated_at
 * @property string $num_logistic
 */

class RequestSaleLogistAccepted extends Model
{
    protected $table = 'request_sale_logist_accepted';

    protected $fillable = [
        'id_shipper', 'id_accountant','comment_shipper', 'id_consignee', 'comment_consignee', 'bitrix_id_manager', 'name_manager',
        'num_one_s', 'num_customer', 'num_provider', 'date_start_search','id_car', 'date_found_car', 'file_proxy', 'file_vet',
        'date_answer_sale_logist', 'file_product', 'date_come_car_send', 'file_torg', 'file_torg_sign', 'file_invoice', 'file_tnn',
        'date_answer_buh', 'file_shipment', 'file_bill', 'file_act', 'date_come_car_take', 'id_status_accountant', 'file_shipping',
        'num_logistic'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'request_sale_logist_accepted_products', 'id_request', 'id_product')->withPivot('unit', 'weight');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'id_car');
    }

    public function contactConsignee()
    {
        return $this->belongsTo(Contact::class, 'id_consignee');
    }

    public function contactShipper()
    {
        return $this->belongsTo(Contact::class, 'id_shipper');
    }

    public function requestSaleLogist()
    {
        return $this->hasOne(RequestSaleLogist::class, 'id_request_accept');
    }

    /*public function requestPurchaseStatistic()
    {
        return $this->hasOne(RequestPurchase::class, 'id_request_accept')->select(['date_come_car_take']);
    }*/

    public function accountant()
    {
        return $this->belongsTo(User::class, 'id_accountant');
    }
}
