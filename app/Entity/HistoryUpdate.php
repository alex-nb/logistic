<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_create
 * @property string $text
 */

class HistoryUpdate extends Model
{
    protected $table = 'history_update';

    protected $fillable = [
        'id_create', 'text'
    ];

    public function userCreate()
    {
        return $this->belongsTo(User::class, 'id_create');
    }
}
