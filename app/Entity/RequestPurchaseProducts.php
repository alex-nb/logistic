<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_product
 */

class RequestPurchaseProducts extends Model
{
    protected $table = 'request_purchase_products';
    public $timestamps = false;

    protected $fillable = [
        'id_request', 'id_product'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function requestPurchase()
    {
        return $this->belongsTo(RequestPurchase::class, 'id_request');
    }
}
