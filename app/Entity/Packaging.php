<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class Packaging extends Model
{
    protected $table = 'packaging';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestPurchase()
    {
        return $this->hasMany(RequestPurchase::class, 'id_package');
    }

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_package');
    }
}
