<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property string $date_export
 * @property string $price
 * @property string $comment_logist
 * @property string $date_answer_logist
 * @property int $id_logist
 */

class RequestPurchaseAcceptedHistoryPrice extends Model
{
    protected $table = 'request_purchase_accepted_history_price';
    public $timestamps = false;

    protected $fillable = [
        'id_request', 'date_export', 'price', 'comment_logist', 'date_answer_logist', 'id_logist'
    ];

    public function logist()
    {
        return $this->belongsTo(User::class, 'id_logist');
    }

    public function requestPurchase()
    {
        return $this->belongsTo(RequestPurchase::class, 'id_request');
    }
}
