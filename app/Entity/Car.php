<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_driver
 * @property string $model
 * @property string $colour
 * @property string $number
 */

class Car extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id_driver', 'model', 'colour', 'number'
    ];

    public function driver()
    {
        return $this->belongsTo(Driver::class, 'id_driver');
    }

    public function requestPurchaseAccepted()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_car');
    }

    public function requestSaleLogistAccepted()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_car');
    }
}
