<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class Unit extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function registry()
    {
        return $this->hasMany(RegistryProduct::class, 'id_unit');
    }
}
