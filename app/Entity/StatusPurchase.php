<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class StatusPurchase extends Model
{
    protected $table = 'status_purchase';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestPurchase()
    {
        return $this->hasMany(RequestPurchase::class, 'id_status_purchase');
    }

    public function requestPurchaseAccepted()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_status_purchase');
    }
}
