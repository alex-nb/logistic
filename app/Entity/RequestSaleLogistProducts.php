<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_product
 */

class RequestSaleLogistProducts extends Model
{
    protected $table = 'request_sale_logist_products';
    public $timestamps = false;

    protected $fillable = [
        'id_request', 'id_product'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function requestSaleLogist()
    {
        return $this->belongsTo(RequestSaleLogist::class, 'id_request');
    }
}
