<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class TypeLoad extends Model
{
    protected $table = 'type_load';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestSale()
    {
        return $this->hasMany(RequestSale::class, 'id_load');
    }

    public function requestPurchase()
    {
        return $this->hasMany(RequestPurchase::class, 'id_load');
    }

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_load');
    }
}
