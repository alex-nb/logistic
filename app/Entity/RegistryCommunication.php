<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_user
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
 */

class RegistryCommunication extends Model
{
    protected $table = 'registry_communications';

    protected $fillable = [
        'id_request', 'id_user', 'message'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function requestPurchase()
    {
        return $this->belongsTo(RegistryRequest::class, 'id_request');
    }
}
