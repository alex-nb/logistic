<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_product
 * @property int $id_purchase
 * @property string $product_comment
 * @property string $product_monthly_need
 * @property string $product_value
 * @property int $id_competitor
 * @property string $competitor_price
 * @property string $competitor_delivery
 * @property string $competitor_payment
 * @property string $competitor_comment
 * @property string $answer
 * @property string $price_purchase
 * @property string $place_take
 * @property string $gross_profit
 * @property int $time_active_price
 * @property string $result_sale
 * @property string $type_request
 * @property string $price_sale
 * @property string $type_discount
 * @property string $comment_purchase
 * @property string $date_answer
 * @property int $id_unit
 */

class RegistryProduct extends Model
{
    protected $table = 'registry_products';
    public $timestamps = false;


    protected $fillable = [
        'id_request', 'id_product', 'product_comment', 'product_monthly_need', 'product_value', 'id_competitor', 'competitor_price', 'competitor_delivery',
        'competitor_payment', 'competitor_comment', 'answer', 'price_purchase', 'place_take', 'gross_profit', 'time_active_price', 'result_sale',
        'type_request', 'price_sale', 'comment_purchase', 'type_discount', 'date_answer', 'id_purchase', 'id_unit'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function requestPurchase()
    {
        return $this->belongsTo(RegistryRequest::class, 'id_request');
    }

    public function competitor()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_competitor');
    }

    public function userPurchase()
    {
        return $this->belongsTo(User::class, 'id_purchase');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'id_unit');
    }
}
