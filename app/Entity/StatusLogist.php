<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class StatusLogist extends Model
{
    protected $table = 'status_logist';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestSale()
    {
        return $this->hasMany(RequestSale::class, 'id_status_logist');
    }

    public function requestPurchase()
    {
        return $this->hasMany(RequestPurchase::class, 'id_status_logist');
    }

    public function requestPurchaseAccepted()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_status_logist');
    }

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_status_logist');
    }

    public function requestSaleLogistAccepted()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_status_logist');
    }
}
