<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;
//use app\Entity\FarmsAddress;

/**
 * @property int $id
 * @property string $comment_create
 * @property int $id_A
 * @property int $id_B
 * @property int $distance
 * @property int $tonnage
 * @property int $id_load
 * @property int $id_create
 * @property string $date_answer
 * @property int $price
 * @property int $id_logist
 * @property string $comment
 * @property int $id_status_sale
 * @property int $id_status_logist
 * @property string $created_at
 * @property string $updated_at
 */


class RequestSale extends Model
{
    protected $table = 'request_sale';

    protected $fillable = [
        'id_A', 'id_B', 'distance', 'tonnage', 'id_load', 'id_create', 'date_answer', 'price', 'id_logist', 'comment',
        'id_status_sale', 'id_status_logist', 'comment_create'
    ];

    public const NEW = 1;
    public const GET_ANSWER = 2;
    public const ARCHIVE_ANSWER = 3;
    public const ARCHIVE_COMPLETE = 4;

    public const LNEW = 1;
    public const LARCHIVE = 9;
    public const LOUT = 10;

    public function farmsAddressA()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_A');
        //return $this->belongsTo(Farm::class, 'id_A')->withPivot('farmsAddress');
    }

    public function farmsAddressB()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_B');
    }

    public function statusLogist()
    {
        return $this->belongsTo(StatusLogist::class, 'id_status_logist');
    }

    public function statusSale()
    {
        return $this->belongsTo(StatusSale::class, 'id_status_sale');
    }

    public function userCreate()
    {
        return $this->belongsTo(User::class, 'id_create');
    }

    public function userLogist()
    {
        return $this->belongsTo(User::class, 'id_logist');
    }

    public function typeLoad()
    {
        return $this->belongsTo(TypeLoad::class, 'id_load');
    }
}
