<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $address
 */

class FarmsAddress extends Model
{
    protected $table = 'farms_address';
    public $timestamps = false;

    protected $fillable = [
        'name', 'address'
    ];

    public function requestSaleA()
    {
        return $this->hasMany(RequestSale::class, 'id_A');
    }

    public function requestSaleB()
    {
        return $this->hasMany(RequestSale::class, 'id_B');
    }

    public function registrySaleB()
    {
        return $this->hasMany(RegistryRequest::class, 'id_B');
    }

    public function registryCompetitor()
    {
        return $this->hasMany(RegistryProduct::class, 'id_competitor');
    }

    public function requestPurchaseA()
    {
        return $this->hasMany(RequestPurchase::class, 'id_A');
    }

    public function requestPurchaseB()
    {
        return $this->hasMany(RequestPurchase::class, 'id_B');
    }

    public function requestSaleLogistA()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_A');
    }

    public function requestSaleLogistB()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_B');
    }

}
