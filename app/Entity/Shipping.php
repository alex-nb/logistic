<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */

class Shipping extends Model
{
    protected $table = 'shipping';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function requestPurchase()
    {
        return $this->hasMany(RequestPurchase::class, 'id_shipping');
    }

    public function requestSaleLogist()
    {
        return $this->hasMany(RequestSaleLogist::class, 'id_shipping');
    }

    public function registry()
    {
        return $this->hasMany(RegistryRequest::class, 'id_shipping');
    }
}
