<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_product
 * @property string $unit
 * @property int $weight
 */

class RequestSaleLogistAcceptedProducts extends Model
{
    protected $table = 'request_sale_logist_accepted_products';
    public $timestamps = false;

    protected $fillable = [
        'id_request', 'id_product', 'unit', 'weight'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function requestSaleLogistAccepted()
    {
        return $this->belongsTo(RequestSaleLogistAccepted::class, 'id_request');
    }
}
