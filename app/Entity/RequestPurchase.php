<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $comment_create
 * @property int $id_A
 * @property int $id_B
 * @property int $distance
 * @property int $tonnage
 * @property int $id_load
 * @property int $adr
 * @property int $id_package
 * @property bool $pallet
 * @property int $id_shipping
 * @property string $date_export
 * @property int $id_create
 * @property string $date_answer
 * @property int $price
 * @property int $id_logist
 * @property string $comment
 * @property string $comment_problem
 * @property int $id_status_purchase
 * @property int $id_status_logist
 * @property string $date_archive
 * @property string $created_at
 * @property string $updated_at
 */

class RequestPurchase extends Model
{
    protected $table = 'request_purchase';

    protected $fillable = [
        'id_A', 'id_B', 'distance', 'tonnage', 'id_load', 'adr', 'id_package', 'pallet', 'id_shipping', 'date_export', 'id_create',
        'date_answer', 'price', 'id_logist', 'comment', 'id_status_purchase', 'id_status_logist', 'date_archive', 'id_request_accept',
        'comment_create', 'comment_problem'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public const NEW = 1;
    public const GET_ANSWER = 2;
    public const TAKE = 3;
    public const SEARCH = 4;
    public const WAY_SENDER = 5;
    public const WAY_RECIPIENT = 6;
    public const IN_RECIPIENT = 7;
    public const ARCHIVE_REALIZE = 8;
    public const ARCHIVE_UNREALIZE = 9;
    public const FIND = 10;
    public const PROBLEM = 11;

    public const LNEW = 8;
    public const LREQUEST_CAR = 2;
    public const LSEARCH_CAR = 3;
    public const LDELIVERY = 5;
    public const LPAYMENT = 6;
    public const LARCHIVE = 7;
    public const LARCHIVE_COMPLETE = 4;
    public const LPROBLEM = 11;
    public const LCHECK_DELIVERY = 12;

    public const ALOADING = 1;
    public const AARCHIVE = 2;
    public const AARRIVAL = 3;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'request_purchase_products', 'id_request', 'id_product');
    }

    public function farmsAddressA()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_A');
    }

    public function farmsAddressB()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_B');
    }

    public function statusLogist()
    {
        return $this->belongsTo(StatusLogist::class, 'id_status_logist');
    }

    public function statusPurchase()
    {
        return $this->belongsTo(StatusPurchase::class, 'id_status_purchase');
    }

    public function userCreate()
    {
        return $this->belongsTo(User::class, 'id_create');
    }

    public function userLogist()
    {
        return $this->belongsTo(User::class, 'id_logist');
    }

    public function shipping()
    {
        return $this->belongsTo(Shipping::class, 'id_shipping');
    }

    public function packaging()
    {
        return $this->belongsTo(Packaging::class, 'id_package');
    }

    public function typeLoad()
    {
        return $this->belongsTo(TypeLoad::class, 'id_load');
    }

    public function requestPurchaseAccepted()
    {
        return $this->belongsTo(RequestPurchaseAccepted::class, 'id_request_accept');
    }

    public function productsAccepted()
    {
        return $this->belongsToMany(Product::class, 'request_purchase_accepted_products', 'id_request', 'id_product');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'id_car');
    }

    public function contactConsignee()
    {
        return $this->belongsTo(Contact::class, 'id_consignee');
    }

    public function contactShipper()
    {
        return $this->belongsTo(Contact::class, 'id_shipper');
    }

    public function requestHistory()
    {
        return $this->hasMany(RequestPurchaseAcceptedHistoryPrice::class, 'id_request');
    }
}
