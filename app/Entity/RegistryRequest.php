<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_create
 * @property string $urgency
 * @property int $id_B
 * @property int $id_shipping
 * @property int $id_status_sale
 * @property string $comment_sale
 * @property string $comment_sale_result
 * @property string $created_at
 * @property string $updated_at
 * @property string $result_sale
 * @property string $file_sale
 * @property string $file_purchase
 */

class RegistryRequest extends Model
{
    protected $table = 'registry_request';

    protected $fillable = [
        'id_create', 'id_B', 'urgency', 'id_shipping', 'id_status_sale', 'comment_sale',
        'comment_sale_result', 'result_sale', 'file_sale', 'file_purchase'
    ];

    public const NEW = 5;
    public const ANSWER = 6;
    public const ARCHIVE = 7;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'registry_products', 'id_request', 'id_product');
    }

    public function farmsAddressB()
    {
        return $this->belongsTo(FarmsAddress::class, 'id_B');
    }

    public function shipping()
    {
        return $this->belongsTo(Shipping::class, 'id_shipping');
    }

    public function statusSale()
    {
        return $this->belongsTo(StatusLogist::class, 'id_status_sale');
    }

    public function userCreate()
    {
        return $this->belongsTo(User::class, 'id_create');
    }

    public function registryProducts()
    {
        return $this->hasMany(RegistryProduct::class, 'id_request');
    }

    public function registryCommunication()
    {
        return $this->hasMany(RegistryCommunication::class, 'id_request');
    }

    public function registryReadMessage()
    {
        return $this->hasMany(RegistryReadMessage::class, 'id_request');
    }
}
