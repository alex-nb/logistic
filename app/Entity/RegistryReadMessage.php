<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_request
 * @property int $id_user
 * @property boolean $read
 */

class RegistryReadMessage extends Model
{
    protected $table = 'registry_read_message';
    public $timestamps = false;

    protected $fillable = [
        'id_request', 'id_user', 'read'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function registryReadMessage()
    {
        return $this->belongsTo(RegistryRequest::class, 'id_request');
    }
}
