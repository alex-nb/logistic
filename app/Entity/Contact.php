<?php

namespace app\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $phones
 */

class Contact extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'phones'
    ];

    public function requestPurchaseAcceptedShipper()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_shipper');
    }

    public function requestPurchaseAcceptedConsignee()
    {
        return $this->hasMany(RequestPurchaseAccepted::class, 'id_consignee');
    }

    public function requestSaleLogistAcceptedShipper()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_shipper');
    }

    public function requestSaleLogistAcceptedConsignee()
    {
        return $this->hasMany(RequestSaleLogistAccepted::class, 'id_consignee');
    }
}
