<?php

namespace app\UseCases\Bitrix;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class BitrixService
{
    private $keys;

    public function __construct()
    {
        $this->keys = ["rp" => ["hook_user_id" => "34", "secret_key" => "9spfqdf25l0pujnj", "domain" => "https://regpartner.bitrix24.ru/rest"],
            "pk" => ["hook_user_id" => "79", "secret_key" => "25szy9hzhh0r986i", "domain" => "https://ipyakovlevpp.bitrix24.ru/rest"],
            "lg" => ["hook_user_id" => "22", "secret_key" => "3sfvj5e6px2b6vv0", "domain" => "https://logicar.bitrix24.ru/rest"]
        ];
    }

    public function getManagerSale() {
        $fields = ["UF_DEPARTMENT" => ['216', '222', '280', '300'], "ACTIVE" => true];
        $result = $this->doRequest('user.get', $fields, "rp");
        $field = ["ID" => 24, "ACTIVE" => true];
        $novokovskii = $this->doRequest('user.get', $field, "rp");
        //array_push($result, $novokovskii);
        return array_merge($result, $novokovskii);
    }
    public function getManagerPurchase() {
        $fields = ["UF_DEPARTMENT" => ['36'], "ACTIVE" => true];
        $result = $this->doRequest('user.get', $fields, "rp");

        return $result;
    }

    public function getAccountant() {
        $fields = ["UF_DEPARTMENT" => ['92'], "ACTIVE" => true];
        $result = $this->doRequest('user.get', $fields, "rp");

        return $result;
    }

    public function getLogists() {
        $fields = ["UF_DEPARTMENT" => ['9'], "ACTIVE" => true];
        $result = $this->doRequest('user.get', $fields, "lg");

        return $result;
    }

    public function getLogistsSale() {
        $fields = ["UF_DEPARTMENT" => ['7'], "ACTIVE" => true];
        $result = $this->doRequest('user.get', $fields, "lg");

        return $result;
    }



    /*
     * 'CODE' => 'bot_logist'
     * "CLIENT_ID" => 'bot_logist_web_hook'
     * "BOT_ID" => 280
     */

    /*
/*
    'CODE' => 'logistic_bot',
    'EVENT_HANDLER' => 'https://tech.pinkomp.ru/platezhi/core/bot.php',
    'CLIENT_ID' => 'logistic_bot_web_hook',
    'BOT_ID' => 26
*/

    public function sendMessageBuh($num, $type_mess) : void
    {
        $message = "";
        switch ($type_mess) {
            case "order":
                $message = 'Пожалуйста, необходимо создать заказ в УТ для Заказа №'.$num.'. № созданного заказа необходимо ввести в заявке. [url=http://192.168.4.62:8000/accountant/order]Перейти в НеРеестр.[/url]';
                break;
            case "loading":
                $message = 'Пожалуйста, перейдите в 1С для формирования отгрузочных документов для Заказа №'.$num.'. Файлы заказа требуется приложить к заявке. [url=http://192.168.4.62:8000/accountant/loading]Перейти в НеРеестр.[/url]';
                break;
            case "arrival":
                $message = 'Пожалуйста, необходимо создать поступление в УТ для Заказа №'.$num.'. Отметьте выполнение в нереестре. [url=http://192.168.4.62:8000/accountant/arrival]Перейти в НеРеестр.[/url]';
                break;
        }

        $fields = [
            "CLIENT_ID" => 'bot_logist_web_hook',
            "DIALOG_ID" => 74,
            "MESSAGE" => $message
        ];

        $this->doRequest('imbot.message.add', $fields, "rp");
    }

    public function sendMessageLogist($num, $type_mess, $dop_mess = '', $id_logist = NULL) : void
    {
        $message = "";
        switch ($type_mess) {
            case 'update':
                $message = 'Новое обновление в реестрах: '.$dop_mess;
                break;
            case "loading":
                $message = 'Создан новый запрос №'.$num.'. '.$dop_mess.' [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case "to_search":
                $message = 'Запрос №'.$num.' направлен на поиск машины. '.$dop_mess.' [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case "cancel":
                $message = 'Запрос №'.$num.' отменен. '.$dop_mess.' [url=http://192.168.4.622:8000]Перейти в НеРеестр.[/url]';
                break;
        }

        if (empty($id_logist)) {
            $fields = ["UF_DEPARTMENT" => ['9'], "ACTIVE" => true];
            $all_logist = $this->doRequest('user.get', $fields, "lg");

            foreach ($all_logist as $logist) {
                $fields = [
                    "CLIENT_ID" => 'logistic_bot_web_hook',
                    "DIALOG_ID" => $logist['ID'],
                    "MESSAGE" => $message
                ];
                $this->doRequest('imbot.message.add', $fields, "lg");
            }
        }
        else {
            $fields = [
                "CLIENT_ID" => 'logistic_bot_web_hook',
                "DIALOG_ID" => $id_logist,
                "MESSAGE" => $message
            ];
            $this->doRequest('imbot.message.add', $fields, "lg");
        }
    }

    public function sendMessageManager($num, $id_man, $type_mess, $dop_message = '') : void
    {
        $CLIENT_ID = "bot_logist_web_hook";
        switch ($type_mess) {
            case 'update':
                $message = 'Новое обновление в реестрах: '.$dop_message;
                break;
            case 'answer':
                $message = 'На ваш запрос №'.$num.' получен ответ. Пожалуйста, перейдите в реестр для получения более детальной информации. [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'search':
                $message = 'Начата обработка Вашего запроса на отправку №'.$num.'. [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'start':
                $message = 'Создан запрос на поиск машины №'.$num.', по которому вы ответственный менеджер. '.$dop_message." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'search_sale':
                $message = 'Начат поиск машины на запрос, по которому вы ответственный менеджер. №'.$num." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'find_car':
                $message = 'Машина на перевозку №'.$num.', найдена. Пожалуйста, перейдите к оформлению документов. Вам требуется приложить ветеринарку и доверенность! [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'find_car_sale':
                $message = 'Машина на перевозку найдена по запросу, по которому вы ответственный менеджер.  №'.$num." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'way_sender':
                $message = 'Машина в пути к отправителю по запросу №'.$num.', по которому вы ответственный менеджер. '.$dop_message." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'way_recepient':
                $message = 'Машина в пути к получателю по запросу №'.$num.', по которому вы ответственный менеджер. '.$dop_message." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'get_payment':
                $message = 'Получена оплату по запросу №'.$num.'. [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'get_payment_sale':
                $message = 'Получена оплату по запросу, по которому вы ответственный менеджер. №'.$num." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'load_accountant':
                $message = 'Бухгалтерия загрузила документы по запросу №'.$num.'. [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'load_accountant_sale':
                $message = 'Бухгалтерия загрузила документы по запросу, по которому вы ответственный менеджер. №'.$num." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'in_recipient':
                $message = 'Машина по запросу №'.$num.' прибыла к грузополучателю. [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]';
                break;
            case 'in_recipient_sale':
                $message = 'Машина прибыла к грузополучателю по запросу, по которому вы ответственный менеджер. №'.$num." [url=http://192.168.4.62:8000]Перейти в НеРеестр.[/url]";
                break;
            case 'answer_registry':
                $CLIENT_ID = "bot_reestr_web_hook";
                $message = 'Получен ответ на ваш запрос №'.$num.' от '.Auth::user()->name.'. Вы можете увидеть и запросить новую информации внутри Вашего запроса. [url=http://192.168.4.62:8000/sale/registry/answers]Перейти в Реестр.[/url]';
                break;
            case 'get_comment':
                $CLIENT_ID = "bot_reestr_web_hook";
                $message = 'Новый комментарий от '.Auth::user()->name.' на Ваш запрос №'.$num.'. Вы можете запросить новую информацию внутри запроса. [url=http://192.168.4.62:8000]Перейти в Реестр.[/url]';
                break;
            case 'close_request':
                $CLIENT_ID = "bot_reestr_web_hook";
                $message = 'Запрос №'.$num.' от '.Auth::user()->name.' завершен. Вы можете просмотреть всю информацию внутри запроса. [url=http://192.168.4.62:8000/purchase/registry/archive]Перейти в Реестр.[/url]';
                break;

        }

        $fields = [
            "CLIENT_ID" => $CLIENT_ID,
            "DIALOG_ID" => $id_man,
            "MESSAGE" => $message
        ];

        $this->doRequest('imbot.message.add', $fields, "rp");
    }

    public function taskManager($id_men, $num, $message, $name ='') : void
    {
        $date = date("c", mktime(0, 0, 0, date('m'), date('d')+3, date('Y')));

        $fields = ["TASKDATA" =>
            [
            "TITLE" => 'Требуется отправить документы на подпись клиенту '.$name.' по Заказу №'.$num,
            "DESCRIPTION" => 'Вашему клиенту '.$name.' осуществляется доставка заказа №'.$num.". Вложенные документы необходимо отправить клиенту по электронной почте с запросом вернуть Вам обратно сканы подписанных документов после выгрузки товара.
                              \n Возвращенные клиентом сканы прикрепите к данной задаче в комментарии. И ТОЛЬКО после этого закройте задачу. \n Документы: \n".$message,
            "DEADLINE" => $date,
            "CREATED_BY" => 24,
            "AUDITORS" => [74],
            "RESPONSIBLE_ID" => $id_men
            ]];

        $this->doRequest('task.item.add', $fields, "rp");
    }

    public function taskSecretar($id_men, $num, $message) : void
    {
        $date = date("c", mktime(0, 0, 0, date('m'), date('d')+8, date('Y')));

        $fields = ["TASKDATA" =>
            [
                "TITLE" => 'Требуется отправить оригиналы документов на подпись почтой РФ по Заказу №'.$num,
                "DESCRIPTION" => 'Требуется отправить оригиналы документов на подпись почтой РФ по Заказу №'.$num.".\n Документы: \n".$message,
                "DEADLINE" => $date,
                "CREATED_BY" => $id_men,
                "RESPONSIBLE_ID" => 32
            ]];

        $this->doRequest('task.item.add', $fields, "rp");
    }

    public function taskTokareva($summ, $message, $num="") : void
    {
        $fields = ["TASKDATA" =>
            [
                "TITLE" => 'Требуется проверить и оплатить заявку по перевозке на сумму '.$summ.'р.'.$num,
                "DESCRIPTION" => 'Требуется проверить и оплатить заявку по перевозке на сумму '.$summ."р.\n Документы: \n".$message,
                "CREATED_BY" => 16,
                "RESPONSIBLE_ID" => 16
            ]];

        $this->doRequest('task.item.add', $fields, "rp");
    }

    public function taskDeveloper($message) : void
    {
        $fields = ["TASKDATA" =>
            [
                "TITLE" => 'Запрос по НЕ реестру от '.Auth::user()->name,
                "DESCRIPTION" => $message,
                "CREATED_BY" => 113,
                "RESPONSIBLE_ID" => 79,
                "AUDITORS" => [1],
                "GROUP_ID" => 85
            ]];

        $this->doRequest('task.item.add', $fields, "pk");
    }

    public function sendMessagePurchasesRegistry($num, $urgency)
    {
        $message = "Добавлен новый запрос №".$num." от ".Auth::user()->name.". Дайте ответ ".$urgency.". [url=http://192.168.4.62:8000/purchase/registry/new]Перейти в Реестр.[/url]";
        $managers = $this->getManagerPurchase();

        foreach ($managers as $man) {
            $fields = [
                "CLIENT_ID" => 'bot_reestr_web_hook',
                "DIALOG_ID" => $man['ID'],
                "MESSAGE" => $message
            ];
            $this->doRequest('imbot.message.add', $fields, "rp");
        }
    }

    private function doRequest($method, $fields, $bitrix) {
        $queryUrl = $this->keys[$bitrix]['domain']."/".$this->keys[$bitrix]['hook_user_id']."/".$this->keys[$bitrix]['secret_key']."/".$method;
        $queryData = http_build_query($fields);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);
        if (array_key_exists('error', $result)) {
            Log::error('Ошибка работы сервиса Битрикс: '.$result['error_description']);
        }
        else {
            return $result['result'];
        }
    }

}
