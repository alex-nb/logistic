<?php

namespace app\Http\Controllers\Purchase;

use app\Entity\RegistryProduct;
use app\Entity\RegistryReadMessage;
use app\Entity\Shipping;
use app\UseCases\Bitrix\BitrixService;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RegistryRequest;
use Illuminate\Support\Facades\Auth;
use app\Entity\RegistryCommunication;

class RegistryPurchaseController extends Controller
{
    public function index(Request $request, $print = false)
    {
        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->paginate(20);
            }
        }

        return view('purchase.registry.new.index', compact('requests', 'search', 'status', 'shipping'));
    }

    public function answers(Request $request, $print = false)
    {
        $sort = 2;

        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->whereHas('registryProducts', function($query) {
                    $query->where('id_purchase', Auth::user()->id);});
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->paginate(20);
            }

        }

        return view('purchase.registry.answers.index', compact('requests', 'sort', 'search', 'status', 'shipping'));
    }

    public function archive(Request $request, $print = false)
    {
        $sort = 2;

        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->whereHas('registryProducts', function($query) {
                    $query->where('id_purchase', Auth::user()->id);});
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->paginate(20);
            }
        }

        return view('purchase.registry.archive.index', compact('requests', 'sort', 'search', 'status', 'shipping'));
    }

    public function giveAnswer(Request $request, BitrixService $bitrix,$id=NULL)
    {
        if ($request->isMethod('post')) {
            $id_product = $request->get('id_product');
            $id_request = $request->get('id_request');
            $comment = $request->get('comment');
            $answer = $request->get('answer');
            $price = $request->get('price');
            $place = $request->get('place');
            $profit = $request->get('profit');
            $time = $request->get('time');
            $now = date('Y-m-d H:i:s');


            if($request->hasFile('file_purchase')) {
                $old_puth = RegistryRequest::where('id', $id_request)->select('file_purchase')->first();
                !empty($old_puth) ? $file_purchase = $old_puth->file_purchase : $file_purchase = "";
                foreach ($request->file('file_purchase') as $file) {
                    $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                    $file_purchase = $file_purchase."&".$file->storeAs('registry_purchase', $name_file);
                }
                RegistryRequest::where('id', $id_request)->update(['file_purchase' => $file_purchase]);
            }

            RegistryProduct::where('id', $id_product)->update(['answer' => $answer, 'price_purchase' => $price,
                'place_take' => $place, 'gross_profit' => $profit, 'time_active_price' => $time, 'comment_purchase' => $comment,
                'date_answer' => $now, 'id_purchase'=> Auth::user()->id]);

            $products = RegistryProduct::where('id_request', $id_request)->get();
            $flag = true;
            foreach ($products as $product) {
                if (!isset($product->answer)) {
                    $flag = false;
                    break;
                }
            }

            if ($flag) {
                RegistryRequest::where('id', $id_request)->update(['id_status_sale' => RegistryRequest::ANSWER,]);
                $manager = RegistryRequest::where('id', $id_request)->with('userCreate')->first();
                $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'answer_registry');
            }

            return redirect('purchase/registry/new');
        }
        else {
            $product = RegistryProduct::where('id', $id)->first();
            $request_info = RegistryRequest::with(['farmsAddressB', 'shipping', 'userCreate'])->where('id', $product->id_request)->first();
            $products = RegistryProduct::with(['product', 'competitor'])->where('id_request', $product->id_request)->get();
            return view('purchase.registry.new.answer', ['id_product'=>$id, 'products' => $products, 'request_info' => $request_info, 'id_request' => $product->id_request]);
        }
    }

    public function communicate(Request $request, BitrixService $bitrix) {
        $comment = $request->get('comment');
        $id_req = $request->get('id_request_comm');
        RegistryCommunication::create(['id_request' => $id_req, 'id_user' => Auth::user()->id, 'message'=>$comment]);
        $manager = RegistryRequest::where('id', $id_req)->with('userCreate')->first();
        $read_message = RegistryReadMessage::where([['id_request', $id_req],['id_user', $manager->userCreate['id']]])->first();
        if (empty($read_message)) {
            RegistryReadMessage::create(['id_request' => $id_req, 'id_user' => $manager->userCreate['id']]);
        }
        else {
            RegistryReadMessage::where('id', $read_message->id)->update(['read' => false]);
        }
        $bitrix->sendMessageManager($id_req, $manager->userCreate['id_bitrix'], 'get_comment');
        return  redirect('purchase/registry/answers');
    }

    public function check(Request $request) {
        $id_req = $request->get('id_request');
        RegistryReadMessage::where([['id_user', Auth::user()->id], ['id_request', $id_req]])->update(['read' => true]);
    }

    public function print(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, true);
            return view('purchase.registry.print', compact('requests', 'type'));
        }
        if ($type == "answers") {
            $requests = $this->answers($request, true);
            return view('purchase.registry.print', compact('requests', 'type'));
        }
        if ($type == "archive") {
            $requests = $this->archive($request, true);
            return view('purchase.registry.print', compact('requests', 'type'));
        }
    }

}
