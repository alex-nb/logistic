<?php

namespace app\Http\Controllers\Purchase;

use app\Entity\RequestPurchase;
use app\Entity\RequestPurchaseAccepted;
use app\Entity\Contact;
use app\Entity\RequestPurchaseAcceptedHistoryPrice;
use app\Entity\RequestPurchaseAcceptedProducts;
use app\Entity\User;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\TypeLoad;
use app\Entity\Shipping;
use app\Entity\Packaging;
use app\Entity\Product;
use app\Entity\FarmsAddress;
use app\Entity\RequestPurchaseProducts;
use Illuminate\Support\Facades\Auth;
use app\UseCases\Bitrix\BitrixService;

class NotRegistryPurchaseController extends Controller
{
    public function index(Request $request, BitrixService $bitrix, $print = false)
    {
        $type_load = TypeLoad::all();
        $sort = 2;

        $managers = $bitrix->getManagerSale();
        $mass_man = [];
        foreach ($managers as $man) {
            $mass_man[$man['ID']] = $man['LAST_NAME']." ".$man['NAME']." ".$man['SECOND_NAME'];
        }

        if (!empty($request->get('cancel'))) {
            $id = $request->get('cancel');
            RequestPurchase::where('id', $id)->update(['id_status_purchase' => RequestPurchase::PROBLEM, 'id_status_logist' => RequestPurchase::LPROBLEM, 'comment_problem' => 'Запрос отменен на этапе создания.']);
        }

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
            case "pointA":
                $query->whereHas('farmsAddressA', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            case "pointB":
                $query->whereHas('farmsAddressB', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            case "comment":
                $query->where('comment_create', 'like', '%' . $value . '%');
                $search = true;
                break;
            case "products":
                $query->whereHas('products', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            case "tonnage":
                $query->where('tonnage', $value);
                $search = true;
                break;
            case "sender":
                $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            case "recipient":
                $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            case "driver":
                $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                    $query->where('name', 'like', '%' . $value . '%');});
                $search = true;
                break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->paginate(20);
            }
        }

        return view('purchase.not_registry.new.index', compact('requests', 'type_load', 'shipping', 'package', 'mass_man', 'sort', 'search', 'status'));
    }

    public function answer(Request $request, BitrixService $bitrix, $print = false)
    {
        $managers = $bitrix->getManagerSale();
        $mass_man = [];
        $sort = 2;

        foreach ($managers as $man) {
            $mass_man[$man['ID']] = $man['LAST_NAME']." ".$man['NAME']." ".$man['SECOND_NAME'];
        }

        if (!empty($request->get('cancel'))) {
            $id = $request->get('cancel');
            RequestPurchase::where('id', $id)->update(['id_status_purchase' => RequestPurchase::PROBLEM, 'id_status_logist' => RequestPurchase::LPROBLEM, 'comment_problem' => 'Запрос отменен на этапе получения цены.']);
        }
        if (!empty($request->get('agree'))) {
            $id = $request->get('agree');
            $request_pur = RequestPurchase::where('id', $id)->first();
            RequestPurchaseAcceptedHistoryPrice::create(['id_request' => $id, 'date_export' => $request_pur->date_export,
                'price' => $request_pur->price, 'comment_logist' => $request_pur->comment,
                'date_answer_logist' => $request_pur->date_answer, 'id_logist' => $request_pur->id_logist]);
            RequestPurchase::where('id', $id)->update(['id_status_purchase' => RequestPurchase::TAKE, 'id_status_logist' => RequestPurchase::LREQUEST_CAR]);
        }

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.answer.index', compact('requests', 'mass_man', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function take(Request $request, BitrixService $bitrix, $print = false)
    {
        $sort = 2;
        $query = RequestPurchase::orderByDesc('created_at');
        if (empty($request->get('my')) || $request->get('my') == 1) {
            $query->where('id_create', Auth::user()->id);
            $sort = 1;
        }
        if (!empty($request->get('cancel'))) {
            $id = $request->get('cancel');
            $dop_message = 'Запрос отменен на этапе запроса на поиск машины';
            $req = RequestPurchase::where('id', $id)->with(['userLogist'])->first();
            $bitrix->sendMessageLogist($id, 'cancel', $dop_message, $req->userLogist['id_bitrix']);
            RequestPurchase::where('id', $id)->update(['id_status_purchase' => RequestPurchase::PROBLEM, 'id_status_logist' => RequestPurchase::LPROBLEM, 'comment_problem' => 'Запрос отменен на этапе запроса на поиск машины.']);
        }

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::TAKE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::TAKE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::TAKE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.take.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function search(Request $request, BitrixService $bitrix, $print = false)
    {
        $sort = 2;

        if (!empty($request->get('cancel'))) {
            $id = $request->get('cancel');
            $dop_message = 'Запрос отменен на этапе поиска машины';
            $req = RequestPurchase::where('id', $id)->with(['userLogist'])->first();
            $bitrix->sendMessageLogist($id, 'cancel', $dop_message, $req->userLogist['id_bitrix']);
            RequestPurchase::where('id', $id)->update(['id_status_purchase' => RequestPurchase::PROBLEM, 'id_status_logist' => RequestPurchase::LPROBLEM, 'comment_problem' => 'Запрос отменен на этапе поиска машины.']);
        }

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::SEARCH)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::SEARCH)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::SEARCH)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.search.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function find(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::FIND)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::FIND)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::FIND)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.find.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function waySender(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_SENDER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_SENDER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_SENDER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.way_sender.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function wayRecipient(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::WAY_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('purchase.not_registry.way_recipient.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function inRecipient(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::IN_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::IN_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::IN_RECIPIENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.in_recipient.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archiveRealized(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_REALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_REALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_REALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.archive_realized.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archiveUnrealized(Request $request, $print = false)
    {
        $sort = 2;

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_UNREALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_UNREALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::ARCHIVE_UNREALIZE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.archive_unrealized.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function problemRequest(Request $request, $print = false)
    {

        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (empty($request->get('my')) || $request->get('my') == 1) {
                $query->where('id_create', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('purchase.not_registry.problem_request.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function create(Request $request, BitrixService $bitrix)
    {
        $point_A = $request->get('point_A');
        $point_B = $request->get('point_B');
        $farm_A = $request->get('farm_A');
        $farm_B = $request->get('farm_B');
        $tonnage = $request->get('tonnage');
        $type_load = $request->get('type_load');
        $package = $request->get('package');
        $shipping = $request->get('shipping');
        $time_out = $request->get('time_out');
        $adr= $request->get('adr');
        $product = $request->get('product');
        $pallet = $request->get('pallet');
        $comment = $request->get('comment');

        $exist_firm_A = FarmsAddress::where('address', $point_A)->where('name', $farm_A)->first();
        $exist_firm_B = FarmsAddress::where('address', $point_B)->where('name', $farm_B)->first();
        if (empty($exist_firm_A)) {
            $exist_firm_A = FarmsAddress::create(['name' => $farm_A,'address' => $point_A]);
        }
        if (empty($exist_firm_B)) {
            $exist_firm_B = FarmsAddress::create(['name' => $farm_B, 'address' => $point_B]);
        }

        $search = $request->get('search');
        if (!empty($search)) {
            $price = $request->get('price');
            $distance = $request->get('distance');
            $comment_price = "Цена и километраж даны ОЗ";

            $fio_shipper = $request->get('fio_shipper');
            $phone_shipper = $request->get('phone_shipper');
            $comment_shipper = $request->get('comment_shipper');
            $fio_consignee = $request->get('fio_consignee');
            $phone_consignee = $request->get('phone_consignee');
            $comment_consignee = $request->get('comment_consignee');
            $id_manager = $request->get('id_manager');
            $name_manager = $request->get('name_manager');
            $num_provider = $request->get('num_provider');
            $num_customer = $request->get('num_customer');


            $exist_shipper = Contact::where('name', $fio_shipper)->where('phones', 'like', '%'.$phone_shipper.'%')->first();
            $exist_consignee = Contact::where('name', $fio_consignee)->where('phones', 'like', '%'.$phone_consignee.'%')->first();
            if (empty($exist_shipper)) {
                $exist_shipper = Contact::create(['name' => $fio_shipper,'phones' => $phone_shipper]);
            }
            if (empty($exist_consignee)) {
                $exist_consignee = Contact::create(['name' => $fio_consignee, 'phones' => $phone_consignee]);
            }
            $request_accepted = RequestPurchaseAccepted::create(['id_shipper' => $exist_shipper->id, 'id_consignee' => $exist_consignee->id,
                'comment_shipper' => $comment_shipper, 'comment_consignee' => $comment_consignee, 'bitrix_id_manager' => $id_manager,
                'id_create' => Auth::user()->id, 'id_status_purchase' => RequestPurchase::NEW, 'id_status_logist' => RequestPurchase::LNEW,
                'name_manager' => $name_manager]);

            if (!empty($num_provider)) {
                $request_accepted->update(['num_provider' => $num_provider]);
            }
            if (!empty($num_customer)) {
                $request_accepted->update(['num_customer' => $num_customer]);
            }

            $request = RequestPurchase::create(['id_A' => $exist_firm_A->id, 'id_B' => $exist_firm_B->id,
                'tonnage' => $tonnage, 'id_load' => $type_load,
                'id_create' => Auth::user()->id, 'id_status_purchase' => RequestPurchase::TAKE, 'id_status_logist' => RequestPurchase::LREQUEST_CAR,
                'adr' => $adr, 'id_package' => $package, 'id_shipping' => $shipping, 'pallet' => $pallet,
                'date_export' => $time_out, 'comment_create' => $comment, 'price' => $price, 'distance' => $distance, 'comment' => $comment_price,
                'id_request_accept' => $request_accepted->id]);

            RequestPurchaseAcceptedHistoryPrice::create(['id_request' => $request->id, 'date_export' => $time_out,
                'price' => $price, 'comment_logist' => $comment_price]);

            $dop_message = '[B]Грузоотправитель:[/B] '.$farm_A.', '.$point_A.
                '. [B]Грузополучатель:[/B] '.$farm_B.', '.$point_B.'. Запрос сразу направлен на поиск машины.';
            $bitrix->sendMessageLogist($request->id, 'loading', $dop_message);
        }
        else {
            $request = RequestPurchase::create(['id_A' => $exist_firm_A->id, 'id_B' => $exist_firm_B->id,
                'tonnage' => $tonnage, 'id_load' => $type_load,
                'id_create' => Auth::user()->id, 'id_status_purchase' => RequestPurchase::NEW, 'id_status_logist' => RequestPurchase::LNEW,
                'adr' => $adr, 'id_package' => $package, 'id_shipping' => $shipping, 'pallet' => $pallet,
                'date_export' => $time_out, 'comment_create' => $comment]);
            /*$dop_message = '[B]Грузоотправитель:[/B] '.$farm_A.', '.$point_A.
                '. [B]Грузополучатель:[/B] '.$farm_B.', '.$point_B;
            $bitrix->sendMessageLogist($request->id, 'loading', $dop_message);*/
        }

        foreach ($product as $prod) {
            $exist_product = Product::where('name', $prod)->first();
            if (empty($exist_product)) {
                $exist_product = Product::create(['name' => $prod]);
            }
            RequestPurchaseProducts::create(['id_request' => $request->id, 'id_product' => $exist_product->id]);
        }
/*
        $manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'load_accountant');
*/
        return redirect('purchase/not_registry/new');
    }

    public function requestFind(Request $request, BitrixService $bitrix)
    {
        $time_out = $request->get('time_out');
        $fio_shipper = $request->get('fio_shipper');
        $phone_shipper = $request->get('phone_shipper');
        $comment_shipper = $request->get('comment_shipper');
        $fio_consignee = $request->get('fio_consignee');
        $phone_consignee = $request->get('phone_consignee');
        $comment_consignee = $request->get('comment_consignee');
        $id_manager = $request->get('id_manager');
        $name_manager = $request->get('name_manager');
        $num_provider = $request->get('num_provider');
        $num_customer = $request->get('num_customer');
        $id_request = $request->get('id_request');

        $exist_shipper = Contact::where('name', $fio_shipper)->where('phones', 'like', '%'.$phone_shipper.'%')->first();
        $exist_consignee = Contact::where('name', $fio_consignee)->where('phones', 'like', '%'.$phone_consignee.'%')->first();
        if (empty($exist_shipper)) {
            $exist_shipper = Contact::create(['name' => $fio_shipper,'phones' => $phone_shipper]);
        }
        if (empty($exist_consignee)) {
            $exist_consignee = Contact::create(['name' => $fio_consignee, 'phones' => $phone_consignee]);
        }
        $request = RequestPurchaseAccepted::create(['id_shipper' => $exist_shipper->id, 'id_consignee' => $exist_consignee->id,
            'comment_shipper' => $comment_shipper, 'comment_consignee' => $comment_consignee, 'bitrix_id_manager' => $id_manager,
            'id_create' => Auth::user()->id, 'id_status_purchase' => RequestPurchase::NEW, 'id_status_logist' => RequestPurchase::LNEW,
            'name_manager' => $name_manager]);

        if (!empty($num_provider)) {
            $request->update(['num_provider' => $num_provider]);
        }
        if (!empty($num_customer)) {
            $request->update(['num_customer' => $num_customer]);
        }

        $request_purchase = RequestPurchase::where('id', $id_request)->with(['farmsAddressA', 'farmsAddressB', 'userLogist'])->first();

        RequestPurchaseAcceptedHistoryPrice::create(['id_request' => $request_purchase->id, 'date_export' => $request_purchase->date_export,
            'price' => $request_purchase->price, 'comment_logist' => $request_purchase->comment,
            'date_answer_logist' => $request_purchase->date_answer, 'id_logist' => $request_purchase->id_logist]);


        RequestPurchase::where('id', $id_request)->update(['id_request_accept' => $request->id, 'id_status_purchase' => RequestPurchase::TAKE,
            'id_status_logist' => RequestPurchase::LREQUEST_CAR, 'date_export' => $time_out]);


        $dop_message = '[B]Грузоотправитель:[/B] '.$request_purchase->farmsAddressA->name.', '.$request_purchase->farmsAddressA->address.
        '. [B]Грузополучатель:[/B] '.$request_purchase->farmsAddressB->name.', '.$request_purchase->farmsAddressB->address;


        //$bitrix->sendMessageManager($id_request, $id_manager, 'start', $dop_message);
        $bitrix->sendMessageLogist($id_request, 'to_search', $dop_message, $request_purchase->userLogist['id_bitrix']);

        return redirect('purchase/not_registry/answer');
    }

    public function other(Request $request) {
        $other_date = $request->get('other_date');
        $id_req = $request->get('id_req');
        $request_purchase = RequestPurchase::where('id', $id_req)->first();
        RequestPurchaseAcceptedHistoryPrice::create(['id_request' => $id_req, 'date_export' => $request_purchase->date_export,
            'price' => $request_purchase->price, 'comment_logist' => $request_purchase->comment,
            'date_answer_logist' => $request_purchase->date_answer, 'id_logist' => $request_purchase->id_logist]);
        RequestPurchase::where('id', $id_req)->update(['date_export' => $other_date, 'price' => NULL, 'comment' => NULL,
            'date_answer' => NULL, 'id_logist' => NULL, 'id_status_purchase' => RequestPurchase::NEW,
            'id_status_logist' => RequestPurchase::LNEW]);
        return redirect('purchase/not_registry/answer');
    }

    public function loadFiles(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_request_accepted');
        $now = date('Y-m-d H:i:s');
        $shipping = $request->get('shipping');

        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['date_answer_purchase' => $now]);

        if($request->hasFile('file_proxy')) {
            $file_proxy = "";
            foreach ($request->file('file_proxy') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_proxy = $file_proxy."&".$file->storeAs('proxy', $name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_torg_sign' => $file_proxy]);
        }

        if($request->hasFile('file_vet')) {
            $file_vet = "";
            foreach ($request->file('file_vet') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_vet = $file_vet."&".$file->storeAs('vet', $name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_vet' => $file_vet]);
        }

        if ($shipping == 3) {
            $bitrix->sendMessageBuh($id_request, 'loading');
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['id_status_accountant' => RequestPurchase::ALOADING]);
        }

        RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::WAY_SENDER, 'id_status_logist' => RequestPurchase::LDELIVERY]);

        //$manager = RequestPurchaseAccepted::where('id', $id_request_accepted)->first();
        //$bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'way_sender');

        return redirect('purchase/not_registry/find');
    }

    public function putProducts(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_request_accepted');
        $product = $request->get('product');
        $unit = $request->get('unit');
        $weight = $request->get('weight');
        $num = $request->get('num');
        $shipping = $request->get('shipping');
        $now = date('Y-m-d H:i:s');

        if($request->hasFile('file_shipping')) {
            $file_shipping = "";
            foreach ($request->file('file_shipping') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_shipping = $file_shipping."&".$file->storeAs('shipping', $name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_shipping' => $file_shipping]);
        }

        if(isset($product[0]))
        {
            foreach ($product as $id=>$prod) {
                $exist_product = Product::where('name', $prod)->first();
                if (empty($exist_product)) {
                    $exist_product = Product::create(['name' => $prod]);
                }
                RequestPurchaseAcceptedProducts::create(['id_request' => $id_request_accepted, 'id_product' => $exist_product->id,
                    'unit' => $unit[$id], 'weight' => $weight[$id]]);
            }
        }

        if ($shipping == 1) {
            $bitrix->sendMessageBuh($num, 'loading');
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['id_status_accountant' => RequestPurchase::ALOADING]);
        }


        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['date_come_car_send' => $now]);
        RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::WAY_RECIPIENT]);

        /*$manager = RequestPurchaseAccepted::where('id', $id_request_accepted)->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'way_recepient');*/

        return redirect('purchase/not_registry/way_sender');
    }

    public function autocomplete(Request $request)
    {
        $data = User::select("name")->where("name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }

    public function print(Request $request) {
        $bitrix = new BitrixService();
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, $bitrix, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "answer") {
            $requests = $this->answer($request, $bitrix, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "take") {
            $requests = $this->take($request, $bitrix, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "search") {
            $requests = $this->search($request, $bitrix, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "find") {
            $requests = $this->find($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "way_sender") {
            $requests = $this->waySender($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "way_recipient") {
            $requests = $this->wayRecipient($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "in_recipient") {
            $requests = $this->inRecipient($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "archive_realized") {
            $requests = $this->archiveRealized($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "archive_unrealized") {
            $requests = $this->archiveUnrealized($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "problem_request") {
            $requests = $this->problemRequest($request, true);
            return view('purchase.not_registry.print', compact('requests', 'type'));
        }
    }
}
