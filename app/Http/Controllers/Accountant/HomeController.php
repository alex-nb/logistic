<?php

namespace app\Http\Controllers\Accountant;

use function foo\func;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RequestPurchase;
use app\Entity\RequestPurchaseAccepted;
use app\UseCases\Bitrix\BitrixService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use app\Mail\StoreMail;

class HomeController extends Controller
{
    public function order(Request $request)
    {
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_post'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_cli'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->whereNull('num_one_s_logist');})
                    ->whereIn('id_status_logist', [4, 5, 6, 10, 12])
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                    'requestPurchaseAccepted.products', 'userCreate',
                    'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->get();
            }
            else {
                $status = true;
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->whereIn('id_status_accountant', [RequestPurchase::AARRIVAL, RequestPurchase::ALOADING, RequestPurchase::AARCHIVE])
                    ->orWhere(function ($query) {
                        $query->whereNull('num_one_s_logist');
                    });
                })
                    ->whereIn('id_status_logist', [4, 5, 6, 10, 12])
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->get();
            }
        }
        else {
            $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                $query->whereNull('num_one_s_logist');})
                ->whereIn('id_status_logist', [4, 5, 6, 10, 12])->with(['requestPurchaseAccepted.car.driver', 'packaging', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                'requestPurchaseAccepted.products', 'userCreate',
                'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->paginate(20);
        }

        return view('accountant.order.index', compact('requests', 'search', 'status'));
    }

    public function arrival(Request $request)
    {
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_post'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_cli'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->where('id_status_accountant', RequestPurchase::AARRIVAL);})
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->get();
            }
            else {
                $status = true;
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->whereIn('id_status_accountant', [RequestPurchase::AARRIVAL, RequestPurchase::ALOADING, RequestPurchase::AARCHIVE])
                        ->orWhere(function ($query) {
                            $query->whereNull('num_one_s_logist');
                        });
                })
                    ->whereIn('id_status_logist', [4, 5, 6, 10, 12])
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->get();
            }
        }
        else {
            $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                $query->where('id_status_accountant', RequestPurchase::AARRIVAL);})->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                'requestPurchaseAccepted.products', 'userCreate',
                'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->paginate(20);
        }

        return view('accountant.arrival.index', compact('requests', 'search', 'status'));
    }

    public function loading(Request $request)
    {
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_post'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_cli'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->where('id_status_accountant', RequestPurchase::ALOADING);})
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'requestPurchaseAccepted.accountant'])->get();
            }
            else {
                $status = true;
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->whereIn('id_status_accountant', [RequestPurchase::AARRIVAL, RequestPurchase::ALOADING, RequestPurchase::AARCHIVE])
                        ->orWhere(function ($query) {
                            $query->whereNull('num_one_s_logist');
                        });
                })
                    ->whereIn('id_status_logist', [4, 5, 6, 10, 12])
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->get();
            }
        }
        else {
            $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                $query->where('id_status_accountant', RequestPurchase::ALOADING);})->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                'requestPurchaseAccepted.products', 'userCreate',
                'farmsAddressA', 'farmsAddressB', 'requestPurchaseAccepted.accountant'])->paginate(20);
        }

        return view('accountant.loading.index', compact('requests', 'search', 'status'));
    }

    public function archive(Request $request)
    {
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_post'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('num_cli'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->where('id_status_accountant', RequestPurchase::AARCHIVE);})
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.contactConsignee', 'requestPurchaseAccepted.contactShipper',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'requestPurchaseAccepted.accountant', 'farmsAddressA', 'farmsAddressB'])->get();
            }
            else {
                $status = true;
                $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                    $query->whereIn('id_status_accountant', [RequestPurchase::AARRIVAL, RequestPurchase::ALOADING, RequestPurchase::AARCHIVE])
                        ->orWhere(function ($query) {
                            $query->whereNull('num_one_s_logist');
                        });
                })
                    ->whereIn('id_status_logist', [4, 5, 6, 10, 12])
                    ->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                        'requestPurchaseAccepted.contactConsignee', 'requestPurchaseAccepted.contactShipper',
                        'requestPurchaseAccepted.products', 'userCreate',
                        'requestPurchaseAccepted.accountant', 'farmsAddressA', 'farmsAddressB'])->get();
            }
        }
        else {
            $requests = $query->whereHas('requestPurchaseAccepted', function($query) {
                $query->where('id_status_accountant', RequestPurchase::AARCHIVE);})->with(['requestPurchaseAccepted.car.driver', 'packaging', 'typeLoad', 'products', 'shipping', 'userLogist', 'requestPurchaseAccepted',
                'requestPurchaseAccepted.contactConsignee', 'requestPurchaseAccepted.contactShipper',
                'requestPurchaseAccepted.products', 'userCreate',
                'requestPurchaseAccepted.accountant', 'farmsAddressA', 'farmsAddressB'])->paginate(20);
        }

        return view('accountant.archive.index', compact('requests', 'search', 'status'));
    }

    public function orderNumber(Request $request)
    {
        $id_request_accepted = $request->get('id_request_accepted');
        $num = $request->get('num');

        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['num_one_s_logist' => $num]);

        return redirect('accountant/order');
    }

    public function checkArrival(Request $request)
    {
        $id_request_accepted = $request->get('id_request_accepted');
        $num = $request->get('num');
        if(!empty($num)) {RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['num_one_s_logist' => $num]);}
        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['id_status_accountant' => RequestPurchase::AARCHIVE]);

        return redirect('accountant/arrival');
    }


    public function loadFiles(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_request_accepted');
        $shipping = $request->get('shipping');
        $id_manager = $request->get('id_manager');

        $message = '';

        if($request->hasFile('torg_with')) {
            $message = $message."\n ТОРГ-12 с подписью:";
            $torg_with = "";
            foreach ($request->file('torg_with') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $torg_with = $torg_with."&".$file->storeAs('torg', $name_file);
                $message = $message."\n http://192.168.4.62:8000/files/download/torg/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_torg_sign' => $torg_with]);
        }
        if($request->hasFile('torg_not')) {
            $message = $message."\n ТОРГ-12 без подписи:";
            $torg_not = "";
            foreach ($request->file('torg_not') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $torg_not = $torg_not."&".$file->storeAs('torg', $name_file);
                $message = $message."\n http://192.168.4.62:8000/files/download/torg/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_torg' => $torg_not]);
        }
        if($request->hasFile('invoice')) {
            $message = $message."\n Счет-фактура:";
            $invoice = "";
            foreach ($request->file('invoice') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $invoice = $invoice."&".$file->storeAs('invoice', $name_file);
                $message = $message."\n http://192.168.4.62:8000/files/download/invoice/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_invoice' => $invoice]);
        }
        if($request->hasFile('tnn')) {
            $message = $message."\n Счет-фактура:";
            $tnn = "";
            foreach ($request->file('tnn') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $tnn = $tnn."&".$file->storeAs('tnn', $name_file);
                $message = $message."\n http://192.168.4.62:8000/files/download/tnn/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_invoice' => $tnn]);
        }

        $now = date('Y-m-d H:i:s');
        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['date_answer_buh' => $now, 'id_status_accountant' => RequestPurchase::AARCHIVE,
            'id_accountant' => Auth::user()->id]);


        if($shipping == 3) {
            Mail::to('logist002@regionpartner.ru')->send(new StoreMail($id_request, $message));
            RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::WAY_RECIPIENT]);
        }
        else {
            $bitrix->taskSecretar($id_manager, $id_request, $message);
        }
        if($shipping == 1) {
            $req = RequestPurchase::where('id_request_accept', $id_request_accepted)->with(['farmsAddressB'])->first();
            $name = $req->farmsAddressB->name;
            $bitrix->taskManager($id_manager, $id_request, $message, $name);
        }

        /*$manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'load_accountant');
        $manager = RequestPurchaseAccepted::where('id', $id_request_accepted)->select('bitrix_id_manager')->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'load_accountant_sale');*/

        return redirect('accountant/loading');
    }
}
