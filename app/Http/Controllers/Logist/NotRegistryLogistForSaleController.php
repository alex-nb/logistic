<?php

namespace app\Http\Controllers\Logist;


use app\Entity\Car;
use app\Entity\Driver;
use app\Entity\Packaging;
use app\Entity\RequestSaleLogistAccepted;
use app\Entity\Shipping;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RequestSaleLogist;
use Illuminate\Support\Facades\Auth;
use app\UseCases\Bitrix\BitrixService;

class NotRegistryLogistForSaleController extends Controller
{
    public function index(Request $request, $print = false)
    {
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging',
                    'shipping', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging',
                    'shipping', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.new.sale_logist.index', compact('requests', 'search', 'status', 'shipping', 'package'));
    }

    public function requestCar(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('logist.not_registry.request_car.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function search(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.search.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function delivery(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.delivery.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function checkDelivery(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.check_delivery.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function payment(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('logist.not_registry.payment.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archive(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.archive.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archiveComplete(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSaleLogist::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestSaleLogistAccepted', 'requestSaleLogistAccepted.contactConsignee',
                    'requestSaleLogistAccepted.contactShipper', 'requestSaleLogistAccepted.car.driver', 'requestSaleLogistAccepted.products', 'userCreate',
                    'requestSaleLogistAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.archive_complete.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function problemRequest(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestSaleLogist::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestSaleLogistAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestSaleLogistAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestSaleLogistAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale_logist', RequestSaleLogist::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_sale_logist', RequestSaleLogist::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale_logist', RequestSaleLogist::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.problem_request.sale_logist.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function giveAnswer(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $price = $request->get('price');
        $comment = $request->get('comment');
        $distance = $request->get('distance');
        $now = date('Y-m-d H:i:s');


        //$manager = RequestSaleLogist::where('id', $id_request)->with('userCreate')->first();
        RequestSaleLogist::where('id', $id_request)->update(['price' => $price, 'comment' => $comment, 'id_status_logist' => RequestSaleLogist::LARCHIVE,
            'id_logist'=> Auth::user()->id, 'date_answer' => $now, 'id_status_sale_logist' => RequestSaleLogist::GET_ANSWER,
            'distance' => $distance]);
            //$bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'answer');

        return redirect('logist/not_registry/new/sale_logist');
    }

    public function startSearchCar(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->id_request;
        $id_req_accept = $request->id_req_accept;

        $now = date('Y-m-d H:i:s');

        //$manager = RequestSaleLogist::where('id', $id_request)->with('userCreate')->first();
        //$bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'search_sale');

        $request_info = RequestSaleLogist::where('id', $id_request)->first();

        if (empty($request_info->id_logist)) {
            $request_info->update(['id_logist' => Auth::user()->id]);
        }

        $request_info->update(['id_status_logist' => RequestSaleLogist::LSEARCH_CAR, 'id_status_sale_logist' => RequestSaleLogist::SEARCH]);
        RequestSaleLogistAccepted::where('id', $id_req_accept)->update(['date_start_search' => $now]);
        return redirect('logist/not_registry/sale_logist/request_car');
    }

    public function other(Request $request)
    {
        $id_request = $request->get('id_request');
        $type = $request->get('type');
        $comment = $request->get('comment');
        if ($type == "cancel") {
            RequestSaleLogist::where('id', $id_request)->update(['id_status_sale_logist' => RequestSaleLogist::PROBLEM, 'id_status_logist' => RequestSaleLogist::LPROBLEM,
                'comment_problem' => $comment]);
        }
        elseif($type == "other_price") {
            $now = date('Y-m-d H:i:s');
            $price = $request->get('price');
            RequestSaleLogist::where('id', $id_request)->update(['id_status_sale_logist' => RequestSaleLogist::GET_ANSWER, 'id_status_logist' => RequestSaleLogist::LARCHIVE,
                'price' => $price, 'comment' => $comment, 'id_logist'=> Auth::user()->id, 'date_answer' => $now]);
        }
        return redirect('logist/not_registry/sale_logist/request_car');
    }

    public function findCar(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_req_ac = $request->get('id_req_ac');
        $now = date('Y-m-d H:i:s');

        $name_driver = $request->get('name_driver');
        $birthday = $request->get('birthday');
        $series = $request->get('series');
        $number = $request->get('number');
        $whom = $request->get('whom');
        $when = $request->get('when');
        $model = $request->get('model');
        $phone = $request->get('phone');
        $gosnomer = $request->get('gosnomer');
        $num_logistic =  $request->get('num_logistic');

        //$manager = RequestSaleLogist::where('id', $id_request)->with('userCreate')->first();
        //$bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'find_car');

        //$manager = RequestSaleLogistAccepted::where('id', $id_req_ac)->first();
        //$bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'find_car_sale');

        //создать водителя
        $exist_driver = Driver::where('name', $name_driver)->where('birthday', $birthday)
            ->where('series', $series)->where('number', $number)->where('whom', $whom)
            ->where('when', $when)->where('phone', $phone)->first();
        if(empty($exist_driver)) {
            $exist_driver = Driver::create(['name'=> $name_driver, 'birthday' => $birthday, 'series' => $series, 'number' => $number, 'whom' => $whom, 'when' => $when, 'phone' => $phone]);
        }
        //создать машину
        $exist_car = Car::where('id_driver', $exist_driver->id)->where('model', $model)->where('number', $gosnomer)->first();
        if(empty($exist_car)) {
            $exist_car = Car::create(['id_driver'=> $exist_driver->id, 'model' => $model, 'number' => $gosnomer]);
        }

        //привязать к заявке, сменить статус
        RequestSaleLogist::where('id', $id_request)->update(['id_status_logist' => RequestSaleLogist::LDELIVERY, 'id_status_sale_logist' => RequestSaleLogist::FIND]);
        RequestSaleLogistAccepted::where('id', $id_req_ac)->update(['date_find_car' => $now, 'id_car' => $exist_car->id, 'num_logistic' => $num_logistic]);

        if($request->hasFile('file_bill')) {
            $file_bill = "";
            foreach ($request->file('file_bill') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_bill = $file_bill."&".$file->storeAs('end', $name_file);
            }
            RequestSaleLogistAccepted::where('id', $id_req_ac)->update(['file_bill' => $file_bill]);
        }

        //$bitrix->sendMessageBuh($id_request, 'order');

        return redirect('logist/not_registry/sale_logist/search');
    }

    public function loadFiles(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_req_ac');
        $now = date('Y-m-d H:i:s');

        RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['date_come_car_take' => $now]);

        if($request->hasFile('file_act')) {
            $file_act = "";
            foreach ($request->file('file_act') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_act = $file_act."&".$file->storeAs('end', $name_file);
            }
            RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['file_act' => $file_act]);
        }

        //$bitrix->sendMessageBuh($id_request, 'arrival');
        RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['id_status_accountant' => RequestSaleLogist::AARRIVAL]);
        RequestSaleLogist::where('id', $id_request)->update(['id_status_sale_logist' => RequestSaleLogist::IN_RECIPIENT, 'id_status_logist' => RequestSaleLogist::LCHECK_DELIVERY]);

        //$manager = RequestSaleLogistAccepted::where('id', $id_request_accepted)->first();
        //$bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'in_recipient_sale');

        return redirect('logist/not_registry/sale_logist/delivery');
    }

    public function checkDeliveryFiles(Request $request)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_req_ac');
        $now = date('Y-m-d H:i:s');

        RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['date_come_car_take' => $now]);

        if($request->hasFile('file_shipment')) {
            $file_shipment = "";
            foreach ($request->file('file_shipment') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_shipment = $file_shipment."&".$file->storeAs('end', $name_file);
            }
            RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['file_shipment' => $file_shipment]);
        }

        if($request->hasFile('file_sf')) {
            $file_sf = "";
            foreach ($request->file('file_sf') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_sf = $file_sf."&".$file->storeAs('end', $name_file);
            }
            RequestSaleLogistAccepted::where('id', $id_request_accepted)->update(['file_sf' => $file_sf]);
        }

        RequestSaleLogist::where('id', $id_request)->update(['id_status_logist' => RequestSaleLogist::LPAYMENT]);


        return redirect('logist/not_registry/sale_logist/check_delivery');
    }

    public function getPayment(Request $request)
    {
        $id_request = $request->id_request;
        $id_req_accept = $request->id_req_accept;

        $now = date('Y-m-d H:i:s');

        RequestSaleLogist::where('id', $id_request)->update(['id_status_logist' => RequestSaleLogist::LARCHIVE_COMPLETE, 'id_status_sale_logist' => RequestSaleLogist::ARCHIVE_REALIZE]);
        RequestSaleLogistAccepted::where('id', $id_req_accept)->update(['date_archive' => $now]);

        return redirect('logist/not_registry/sale_logist/payment');
    }

    public function print(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "request_car") {
            $requests = $this->requestCar($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "search") {
            $requests = $this->search($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "delivery") {
            $requests = $this->delivery($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "check_delivery") {
            $requests = $this->checkDelivery($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "payment") {
            $requests = $this->payment($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "archive_complete") {
            $requests = $this->archiveComplete($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "archive") {
            $requests = $this->archive($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
        if ($type == "problem_request") {
            $requests = $this->problemRequest($request, true);
            return view('logist.not_registry.print_sale_logist', compact('requests', 'type'));
        }
    }
}
