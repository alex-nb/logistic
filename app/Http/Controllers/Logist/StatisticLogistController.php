<?php

namespace app\Http\Controllers\Logist;


use app\Entity\RequestPurchase;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StatisticLogistController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $datefilter = $request->get('daterange');
            $from = substr($datefilter, 0, 10);
            $to = substr($datefilter, 13, 10);

            $requests = RequestPurchase::select('id_request_accept','date_export', 'id_shipping', 'id_status_logist')->whereIn('id_status_logist', [RequestPurchase::LPAYMENT, RequestPurchase::LARCHIVE_COMPLETE,
                RequestPurchase::LDELIVERY, RequestPurchase::LCHECK_DELIVERY])
                ->whereBetween('date_export', [date('Y-m-d',strtotime($from)), date('Y-m-d', strtotime($to))])
                ->with(['requestPurchaseAccepted'])->get();
            $counts = [];
            //return $requests;
            //$counts['in'] = [];

            foreach ($requests as $req) {
                $type = 'in';
                if ($req->id_shipping == 3) {
                    $type = 'out';
                }
                $date_m =[1=>'Январь', 2=>'Февраль', 3=>'Март', 4=>'Апрель', 5=>'Май', 6=>'Июнь', 7=>'Июль', 8=>'Август', 9=>'Сентябрь', 10=>'Октябрь', 11=>'Ноябрь', 12=>'Декабрь'];
                $date = $date_m[date('n', strtotime($req->date_export))]." ".date('Y', strtotime($req->date_export));
                //$date = date('Y-m', strtotime($req->date_export));

                if (!empty($counts[$date][$type]['all'])) {
                    $counts[$date][$type]['all'] += 1;
                }
                else {
                    $counts[$date][$type]['all'] = 1;
                }
                if ($req->id_status_logist == RequestPurchase::LPAYMENT || $req->id_status_logist == RequestPurchase::LARCHIVE_COMPLETE) {
                    if (!empty($counts[$date][$type]['fact'])) {
                        $counts[$date][$type]['fact'] += 1;
                    }
                    else {
                        $counts[$date][$type]['fact'] = 1;
                    }
                    if (strtotime($req->requestPurchaseAccepted->date_come_car_take) > strtotime($req->date_export)) {
                        if (!empty($counts[$date][$type]['late'])) {
                            $counts[$date][$type]['late'] += 1;
                        }
                        else {
                            $counts[$date][$type]['late'] = 1;
                        }
                    }
                    else {
                        if (!empty($counts[$date][$type]['ok'])) {
                            $counts[$date][$type]['ok'] += 1;
                        }
                        else {
                            $counts[$date][$type]['ok'] = 1;
                        }
                    }
                }
            }
            return $counts;
        }
        else {
            return view('logist.statistic.index');
        }
    }
}
