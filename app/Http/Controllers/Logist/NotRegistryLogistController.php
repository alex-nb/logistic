<?php

namespace app\Http\Controllers\Logist;


use app\Entity\Car;
use app\Entity\Driver;
use app\Entity\Packaging;
use app\Entity\RequestPurchaseAccepted;
use app\Entity\Shipping;
use app\Entity\TypeLoad;
use app\Http\Requests\Logist\GivePriceRequest;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RequestPurchase;
use app\Entity\RequestSale;
use Illuminate\Support\Facades\Auth;
use app\UseCases\Bitrix\BitrixService;

class NotRegistryLogistController extends Controller
{
    public function index(Request $request, $print = false)
    {
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }

        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSale::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSale::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSale::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->paginate(20);
            }
        }
        return view('logist.not_registry.new.sale.index', compact('requests', 'type_load', 'search', 'status'));
    }

    public function purchase(Request $request, $print = false)
    {
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LNEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.new.purchase.index', compact('requests', 'search', 'status', 'shipping', 'package'));
    }

    public function requestCar(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LREQUEST_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }

        }

        return view('logist.not_registry.request_car.purchase.index', compact('requests', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function search(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LSEARCH_CAR)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.search.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function delivery(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if ($print) {
                if (!empty($request->get('my')) && $request->get('my') == 1) {
                    $query->where('id_logist', Auth::user()->id);
                    $sort = 1;
                }
                $requests = $query->where('id_status_logist', RequestPurchase::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LDELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.delivery.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function checkDelivery(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LCHECK_DELIVERY)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.check_delivery.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function payment(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LPAYMENT)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.payment.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archive(Request $request, $print = false)
    {
        $sort = 2;
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }


        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestSale::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestSale::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestSale::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->paginate(20);
            }
        }


        return view('logist.not_registry.archive.sale.index', compact('requests', 'sort', 'type_load', 'search', 'status'));
    }

    public function archivePurchase(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.archive.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function archiveComplete(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_logist', RequestPurchase::LARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                    'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                    'requestPurchaseAccepted.accountant', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.archive_complete.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function problemRequest(Request $request, $print = false)
    {
        $sort = 2;
        $shipping = Shipping::all();
        $package = Packaging::all();
        $query = RequestPurchase::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('dateexport'))) {
            $query->whereBetween('date_export', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('package'))) {
            $query->where('id_package', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "pointA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "pointB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "products":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "tonnage":
                    $query->where('tonnage', $value);
                    $search = true;
                    break;
                case "sender":
                    $query->whereHas('requestPurchaseAccepted.contactShipper', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "recipient":
                    $query->whereHas('requestPurchaseAccepted.contactConsignee', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "driver":
                    $query->whereHas('requestPurchaseAccepted.car.driver', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products', 'packaging', 'shipping', 'userCreate'])->get();
            }
        }
        else {
            if (!empty($request->get('my')) && $request->get('my') == 1) {
                $query->where('id_logist', Auth::user()->id);
                $sort = 1;
            }
            if ($print) {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_purchase', RequestPurchase::PROBLEM)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                    'packaging', 'shipping', 'userLogist', 'userCreate', 'requestHistory', 'requestHistory.logist'])->paginate(20);
            }
        }

        return view('logist.not_registry.problem_request.purchase.index', compact('requests', 'sort', 'sort', 'search', 'status', 'shipping', 'package'));
    }

    public function giveAnswer(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $price = $request->get('price');
        $comment = $request->get('comment');
        $type = $request->get('type');
        $distance = $request->get('distance');
        $now = date('Y-m-d H:i:s');

        if ($type == 'sale') {
            $manager = RequestSale::where('id', $id_request)->with('userCreate')->first();
            RequestSale::where('id', $id_request)->update(['price' => $price, 'comment' => $comment, 'id_status_logist' => RequestSale::LARCHIVE,
                'id_logist'=> Auth::user()->id, 'date_answer' => $now, 'id_status_sale' => RequestSale::GET_ANSWER, 'distance' => $distance]);
            $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'answer');
        }
        else {
            $manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
            RequestPurchase::where('id', $id_request)->update(['price' => $price, 'comment' => $comment, 'id_status_logist' => RequestPurchase::LARCHIVE,
                'id_logist'=> Auth::user()->id, 'date_answer' => $now, 'id_status_purchase' => RequestPurchase::GET_ANSWER,
                'distance' => $distance]);
            $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'answer');
        }

        return redirect('logist/not_registry/new/'.$type);
    }

    public function startSearchCar(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->id_request;
        $id_req_accept = $request->id_req_accept;

        $now = date('Y-m-d H:i:s');

        /*$manager = RequestPurchaseAccepted::where('id', $id_req_accept)->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'search');*/
        $manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'search_sale');

        $request_info = RequestPurchase::where('id', $id_request)->first();

        if (empty($request_info->id_logist)) {
            $request_info->update(['id_logist' => Auth::user()->id]);
        }

        $request_info->update(['id_status_logist' => RequestPurchase::LSEARCH_CAR, 'id_status_purchase' => RequestPurchase::SEARCH]);
        RequestPurchaseAccepted::where('id', $id_req_accept)->update(['date_start_search' => $now]);
        return redirect('logist/not_registry/purchase/request_car');
    }

    public function other(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        //$id_req_accept = $request->get('id_req_ac');
        $type = $request->get('type');
        $comment = $request->get('comment');
        //$old_value = RequestPurchaseAccepted::where('id', $id_req_accept)->first();
        if ($type == "cancel") {
            RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::PROBLEM, 'id_status_logist' => RequestPurchase::LPROBLEM,
                'comment_problem' => $comment]);
            //$manager = RequestPurchaseAccepted::where('id', $id_req_accept)->first();
            //$bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'search');
        }
        elseif($type == "other_price") {
            $now = date('Y-m-d H:i:s');
            $price = $request->get('price');
            RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::GET_ANSWER, 'id_status_logist' => RequestPurchase::LARCHIVE,
                'price' => $price, 'comment' => $comment, 'id_logist'=> Auth::user()->id, 'date_answer' => $now]);
        }
        return redirect('logist/not_registry/purchase/request_car');
    }

    public function findCar(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_req_ac = $request->get('id_req_ac');
        $now = date('Y-m-d H:i:s');

        $name_driver = $request->get('name_driver');
        $birthday = $request->get('birthday');
        $series = $request->get('series');
        $number = $request->get('number');
        $whom = $request->get('whom');
        $when = $request->get('when');
        $model = $request->get('model');
        $phone = $request->get('phone');
        $gosnomer = $request->get('gosnomer');
        $num_logistic =  $request->get('num_logistic');

        $manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'find_car');

        $manager = RequestPurchaseAccepted::where('id', $id_req_ac)->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'find_car_sale');

        //создать водителя
        $exist_driver = Driver::where('name', $name_driver)->where('birthday', $birthday)
            ->where('series', $series)->where('number', $number)->where('whom', $whom)
            ->where('when', $when)->where('phone', $phone)->first();
        if(empty($exist_driver)) {
            $exist_driver = Driver::create(['name'=> $name_driver, 'birthday' => $birthday, 'series' => $series, 'number' => $number, 'whom' => $whom, 'when' => $when, 'phone' => $phone]);
        }
        //создать машину
        $exist_car = Car::where('id_driver', $exist_driver->id)->where('model', $model)->where('number', $gosnomer)->first();
        if(empty($exist_car)) {
            $exist_car = Car::create(['id_driver'=> $exist_driver->id, 'model' => $model, 'number' => $gosnomer]);
        }

        //привязать к заявке, сменить статус
        RequestPurchase::where('id', $id_request)->update(['id_status_logist' => RequestPurchase::LDELIVERY, 'id_status_purchase' => RequestPurchase::FIND]);
        RequestPurchaseAccepted::where('id', $id_req_ac)->update(['date_find_car' => $now, 'id_car' => $exist_car->id, 'num_logistic' => $num_logistic]);

        if($request->hasFile('file_bill')) {
            $file_bill = "";
            foreach ($request->file('file_bill') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_bill = $file_bill."&".$file->storeAs('end', $name_file);
            }
            RequestPurchaseAccepted::where('id', $id_req_ac)->update(['file_bill' => $file_bill]);
        }

        $bitrix->sendMessageBuh($id_request, 'order');

        return redirect('logist/not_registry/purchase/search');
    }

    public function loadFiles(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_req_ac');
        $now = date('Y-m-d H:i:s');

        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['date_come_car_take' => $now]);

        if($request->hasFile('file_act')) {
            $file_act = "";
            foreach ($request->file('file_act') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_act = $file_act."&".$file->storeAs('end', $name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_act' => $file_act]);
        }

        $bitrix->sendMessageBuh($id_request, 'arrival');
        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['id_status_accountant' => RequestPurchase::AARRIVAL]);
        RequestPurchase::where('id', $id_request)->update(['id_status_purchase' => RequestPurchase::IN_RECIPIENT, 'id_status_logist' => RequestPurchase::LCHECK_DELIVERY]);

        $manager = RequestPurchaseAccepted::where('id', $id_request_accepted)->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'in_recipient_sale');
        /*$manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'in_recipient');*/

        return redirect('logist/not_registry/purchase/delivery');
    }

    public function checkDeliveryFiles(Request $request) //, BitrixService $bitrix
    {
        $id_request = $request->get('id_request');
        $id_request_accepted = $request->get('id_req_ac');
        //$summ = $request->get('summ');
        $now = date('Y-m-d H:i:s');
        //$message = '';

        RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['date_come_car_take' => $now]);

        if($request->hasFile('file_shipment')) {
            $file_shipment = "";
            //$message = $message."\n Подтверждающий документ:";
            foreach ($request->file('file_shipment') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_shipment = $file_shipment."&".$file->storeAs('end', $name_file);
                //$message = $message."\n http://192.168.4.62:8000/files/download/end/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_shipment' => $file_shipment]);
        }

        if($request->hasFile('file_sf')) {
            $file_sf = "";
            //$message = $message."\n Счет-фактура:";
            foreach ($request->file('file_sf') as $file) {
                $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                $file_sf = $file_sf."&".$file->storeAs('end', $name_file);
                //$message = $message."\n http://192.168.4.62:8000/files/download/end/".urlencode($name_file);
            }
            RequestPurchaseAccepted::where('id', $id_request_accepted)->update(['file_sf' => $file_sf]);
        }

        RequestPurchase::where('id', $id_request)->update(['id_status_logist' => RequestPurchase::LPAYMENT]);

        /*$orher_file = RequestPurchaseAccepted::where('id', $id_request_accepted)->first();
        if (!empty($orher_file->file_bill)) {
            $array = explode("&", $orher_file->file_bill);
            foreach ($array as $file) {
                if (strlen($file) > 0) {
                    $message = $message."\n Счет: http://192.168.4.62:8000/files/download/".urlencode($file);
                }
            }

        }
        if (!empty($orher_file->file_act)) {
            $array = explode("&", $orher_file->file_act);
            foreach ($array as $file) {
                if (strlen($file) > 0) {
                    $message = $message."\n Акт: http://192.168.4.62:8000/files/download/".urlencode($file);
                }
            }
        }*/

        //$num = $orher_file->num_one_s_logist;

        //$bitrix->taskTokareva($summ, $message, $num);

        return redirect('logist/not_registry/purchase/check_delivery');
    }

    public function getPayment(Request $request, BitrixService $bitrix)
    {
        $id_request = $request->id_request;
        $id_req_accept = $request->id_req_accept;

        $now = date('Y-m-d H:i:s');

        RequestPurchase::where('id', $id_request)->update(['id_status_logist' => RequestPurchase::LARCHIVE_COMPLETE, 'id_status_purchase' => RequestPurchase::ARCHIVE_REALIZE]);
        RequestPurchaseAccepted::where('id', $id_req_accept)->update(['date_archive' => $now]);

        /*$manager = RequestPurchase::where('id', $id_request)->with('userCreate')->first();
        $bitrix->sendMessageManager($id_request, $manager->userCreate['id_bitrix'], 'get_payment');
        $manager = RequestPurchaseAccepted::where('id', $id_req_accept)->first();
        $bitrix->sendMessageManager($id_request, $manager->bitrix_id_manager, 'get_payment_sale');*/
        return redirect('logist/not_registry/purchase/payment');
    }

    public function printSale(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, true);
            return view('logist.not_registry.print_sale', compact('requests', 'type'));
        }
        if ($type == "archive") {
            $requests = $this->archive($request, true);
            return view('logist.not_registry.print_sale', compact('requests', 'type'));
        }
    }

    public function printPurchase(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->purchase($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "request_car") {
            $requests = $this->requestCar($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "search") {
            $requests = $this->search($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "delivery") {
            $requests = $this->delivery($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "check_delivery") {
            $requests = $this->checkDelivery($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "payment") {
            $requests = $this->payment($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "archive_complete") {
            $requests = $this->archiveComplete($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "archive") {
            $requests = $this->archivePurchase($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
        if ($type == "problem_request") {
            $requests = $this->problemRequest($request, true);
            return view('logist.not_registry.print_purchase', compact('requests', 'type'));
        }
    }
}
