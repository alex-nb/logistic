<?php

namespace app\Http\Controllers\Sale;

use app\Entity\User;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RequestSale;
use app\Entity\RequestPurchase;
use app\Entity\TypeLoad;
use app\Entity\FarmsAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class NotRegistrySaleController extends Controller
{
    public function index(Request $request, $print = false)
    {
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }


        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RequestSale::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RequestSale::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RequestSale::NEW)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->paginate(20);
            }

        }

        return view('sale.not_registry.new.index', compact('requests', 'type_load', 'search', 'status'));
    }

    public function answer(Request $request, $print = false)
    {
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }


        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RequestSale::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RequestSale::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RequestSale::GET_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->paginate(20);
            }

        }

        return view('sale.not_registry.answer.index', compact('requests', 'type_load', 'search', 'status'));
    }

    public function archiveAnswer(Request $request, $print = false)
    {
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }


        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_ANSWER)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->paginate(20);
            }

        }

        return view('sale.not_registry.archive_answer.index', compact('requests', 'type_load', 'search', 'status'));
    }

    public function archivePrice(Request $request, $print = false)
    {
        $type_load = TypeLoad::all();
        $query = RequestSale::orderByDesc('created_at');
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('logist'))) {
            $query->whereHas('userLogist', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('typeLoad'))) {
            $query->where('id_load', $value);
            $search = true;
        }


        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farmA":
                    $query->whereHas('farmsAddressA', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "farmB":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_create', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_ol":
                    $query->where('comment', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "distance":
                    $query->where('distance', 'like', '%' . $value . '%');
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userCreate'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RequestSale::ARCHIVE_COMPLETE)->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'userLogist', 'userCreate'])->paginate(20);
            }

        }

        return view('sale.not_registry.archive_price.index', compact('requests', 'type_load', 'search', 'status'));
    }

    public function requests(Request $request)
    {
        $sort = 2;
        $query = RequestPurchase::orderByDesc('created_at');
        if (empty($request->get('my')) || $request->get('my') == 1) {
            $query->whereHas('requestPurchaseAccepted', function($q) {
                $q->where('bitrix_id_manager', Auth::user()->id_bitrix);});
            $sort = 1;
        }
        $requests = $query
            ->whereIn('id_status_purchase', [3,4,5,6,7,8,10])
            ->with(['typeLoad', 'farmsAddressA', 'farmsAddressB', 'products',
                'packaging', 'shipping', 'userLogist', 'requestPurchaseAccepted', 'requestPurchaseAccepted.contactConsignee',
                'requestPurchaseAccepted.contactShipper', 'requestPurchaseAccepted.car.driver', 'requestPurchaseAccepted.products', 'userCreate',
                'requestPurchaseAccepted.accountant'])
            ->paginate(20);
        return view('sale.not_registry.requests.index', compact('requests', 'sort'));
    }

    public function create(Request $request)
    {
        $point_A = $request->get('point_A');
        $point_B = $request->get('point_B');
        $farm_A = $request->get('farm_A');
        $farm_B = $request->get('farm_B');
        $distance = $request->get('distance');
        $tonnage = $request->get('tonnage');
        $type_load = $request->get('type_load');
        $comment = $request->get('comment');

        if ($distance<100 || $distance>1000 ||
        ($tonnage<=2000 && $type_load == 3) ||
        ($tonnage<=3000 && $tonnage>2000 && $type_load != 1) ||
        ($tonnage<=5000 && $tonnage>3000 && $type_load == 3) ||
        ($tonnage<=7000 && $tonnage>5000 && $type_load != 2) ||
        ($tonnage<=10000 && $tonnage>7000 && $type_load == 3)) {
            $this->createRequest($point_A, $point_B, $farm_A, $farm_B, $distance, $tonnage, $type_load, RequestSale::NEW, RequestSale::LNEW);
        }
        else {
            $exist_point_A = FarmsAddress::where('address', $point_A)->first();
            if(!empty($exist_point_A)) {
                $exist_point_B = FarmsAddress::where('address', $point_B)->first();
                if(!empty($exist_point_B)) {
                    $date = date("Y-m-d G:i:s", mktime(0, 0, 0, date('m')-3, date('d'), date('Y')));
                    $last_request = RequestSale::where('id_A', $exist_point_A->id)
                        ->where('id_B', $exist_point_B->id)
                        ->where('created_at', '>', $date)
                        ->where('id_status_sale', '<>', RequestSale::NEW)
                        ->where('distance', $distance)
                        ->where('tonnage', $tonnage)
                        ->where('id_load', $type_load)
                        ->orderByDesc('created_at')
                        ->first();
                    if(!empty($last_request)) {
                        \Session::flash('price_request', 'Подобный запрос уже расчитывался. Цена: '.$last_request->price.' р.');
                        $this->createRequest($point_A, $point_B, $farm_A, $farm_B, $distance, $tonnage, $type_load, RequestSale::ARCHIVE_COMPLETE, RequestSale::LOUT, $last_request->price, $comment);
                    }
                }
            }
            if (empty($exist_point_A) || empty($exist_point_B) || empty($last_request)) {
                $this->createRequest($point_A, $point_B, $farm_A, $farm_B, $distance, $tonnage, $type_load, RequestSale::NEW, RequestSale::LNEW, NULL, $comment);
            }
        }
        return redirect('sale/not_registry/new');
    }

    private function createRequest($point_A, $point_B, $farm_A, $farm_B, $distance, $tonnage, $type_load, $status_sale, $status_logist, $price = NULL, $comment = NULL) {
        $exist_firm_A = FarmsAddress::where('address', $point_A)->where('name', $farm_A)->first();
        $exist_firm_B = FarmsAddress::where('address', $point_B)->where('name', $farm_B)->first();
        if (empty($exist_firm_A)) {
            $exist_firm_A = FarmsAddress::create(['name' => $farm_A,'address' => $point_A]);
        }
        if (empty($exist_firm_B)) {
            $exist_firm_B = FarmsAddress::create(['name' => $farm_B, 'address' => $point_B]);
        }
        RequestSale::create(['id_A' => $exist_firm_A->id, 'id_B' => $exist_firm_B->id,
            'distance' => $distance, 'tonnage' => $tonnage, 'id_load' => $type_load,
            'id_create' => Auth::user()->id, 'id_status_sale' => $status_sale, 'id_status_logist' => $status_logist, 'price' => $price, 'comment_create' => $comment]);
    }

    public function print(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, true);
            return view('sale.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "answer") {
            $requests = $this->answer($request, true);
            return view('sale.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "archive_answer") {
            $requests = $this->archiveAnswer($request, true);
            return view('sale.not_registry.print', compact('requests', 'type'));
        }
        if ($type == "archive_price") {
            $requests = $this->archivePrice($request, true);
            return view('sale.not_registry.print', compact('requests', 'type'));
        }
    }
}
