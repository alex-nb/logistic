<?php

namespace app\Http\Controllers\Sale;

use app\Entity\RegistryCommunication;
use app\Entity\RegistryProduct;
use app\Entity\RegistryReadMessage;
use app\Entity\Unit;
use app\UseCases\Bitrix\BitrixService;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Entity\RegistryRequest;
use Illuminate\Support\Facades\Auth;
use app\Entity\Shipping;
use app\Entity\FarmsAddress;
use app\Entity\Product;

class RegistrySaleController extends Controller
{
    public function index(Request $request, $print = false)
    {
        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $units = Unit::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::NEW)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->paginate(20);
            }
        }

        return view('sale.registry.new.index', compact('requests', 'shipping', 'search', 'status', 'units'));
    }

    public function answers(Request $request, $print = false)
    {
        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::ANSWER)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryReadMessage', 'registryProducts.unit'])->paginate(20);
            }

        }

        return view('sale.registry.answers.index', compact('requests', 'search', 'status', 'shipping'));
    }

    public function archive(Request $request, $print = false)
    {
        $query = RegistryRequest::orderByDesc('created_at');
        $shipping = Shipping::all();
        $status = false;
        $search = false;
        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
            $search = true;
        }
        if (!empty($value = $request->get('datecreate'))) {
            $query->whereBetween('created_at', [date('Y-m-d H:i:s',strtotime($value.' 00:00:00')), date('Y-m-d H:i:s',strtotime($value.' 23:59:59'))]);
            $search = true;
        }
        if (!empty($value = $request->get('task'))) {
            if ($value == "ask") $value = "Запрос цены";
            if ($value == "sale") $value = "Необходимость скидки";
            $query->whereHas('registryProducts', function($query) use($value) {
                $query->where('type_request', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('shipping'))) {
            $query->where('id_shipping', $value);
            $search = true;
        }
        if (!empty($value = $request->get('creator'))) {
            $query->whereHas('userCreate', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('purchase'))) {
            $query->whereHas('registryProducts.userPurchase', function($query) use($value) {
                $query->where('name', 'like', '%' . $value . '%');});
            $search = true;
        }
        if (!empty($value = $request->get('name'))) {
            $where = $request->get('field');
            switch ($where){
                case "farm":
                    $query->whereHas('farmsAddressB', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "comment_op":
                    $query->where('comment_sale', 'like', '%' . $value . '%');
                    $search = true;
                    break;
                case "comment_oz":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('comment_purchase', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
                case "product":
                    $query->whereHas('products', function($query) use($value) {
                        $query->where('name', 'like', '%' . $value . '%');});
                    $search = true;
                    break;
            }
        }

        if ($search) {
            if ($request->get('status') == "this") {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->get();
            }
            else {
                $status = true;
                $requests = $query->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.unit'])->get();
            }
        }
        else {
            if (!Auth::user()->isAdmin() && !Auth::user()->isDirSale()) { //Тут проверка еще и на руководителя
                $query->where('id_create', Auth::user()->id);
            }
            if ($print) {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->get();
                return $requests;
            }
            else {
                $requests = $query->where('id_status_sale', RegistryRequest::ARCHIVE)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor', 'registryProducts.userPurchase',
                    'registryCommunication', 'registryCommunication.user', 'registryProducts.unit'])->paginate(20);
            }

        }

        return view('sale.registry.archive.index', compact('requests', 'search', 'status', 'shipping'));
    }

    public function create(Request $request, BitrixService $bitrix, $id=0)
    {
        if ($request->isMethod('post')) {
            $old_id = $request->get('id_request');
            $point_B = $request->get('point_B');
            $farm_B = $request->get('farm_B');
            $urgency = $request->get('urgency');

            $shipping = $request->get('shipping');
            $comment = $request->get('comment');
            $products = $request->get('product');
            $prims = $request->get('prim');
            $mounths = $request->get('mounth');
            $needs = $request->get('need');
            $concs = $request->get('conc');
            $prices = $request->get('price');
            $dostavkas = $request->get('dostavka');
            $uslovs = $request->get('uslov');
            $commcs = $request->get('commc');
            $id_products = $request->get('id_product');
            $deistvie = $request->get('deistvie');
            $vid_skidki = $request->get('vid_skidki');
            $sale_price = $request->get('sale_price');
            $need_unit = $request->get('need_unit');

            if($request->hasFile('file_sale')) {
                $files = $request->file('file_sale');
            }

            $exist_firm_B = FarmsAddress::where('address', $point_B)->where('name', $farm_B)->first();
            if (empty($exist_firm_B)) {
                $exist_firm_B = FarmsAddress::create(['name' => $farm_B, 'address' => $point_B]);
            }

            if ($old_id != 0) {
                $old_puth = RegistryRequest::where('id', $old_id)->select('file_sale')->first();
                !empty($old_puth) ? $file_sale = $old_puth->file_sale : $file_sale = "";
                if(!empty($files)) {
                    foreach ($files as $file) {
                        $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                        $file_sale = $file_sale."&".$file->storeAs('registry_sale', $name_file);
                    }
                }
                RegistryRequest::where('id', $old_id)->update([
                    'id_B' => $exist_firm_B->id,
                    'urgency' => $urgency,
                    'id_shipping' => $shipping,
                    'comment_sale' => $comment,
                    'file_sale' => $file_sale,
                ]);
            }
            else {
                $file_sale = "";
                if(!empty($files)) {
                    foreach ($files as $file) {
                        $name_file = str_replace(' ', '', random_int(0, 9999)."-".$file->getClientOriginalName());
                        $file_sale = $file_sale."&".$file->storeAs('registry_sale', $name_file);
                    }
                }
                $request_registry = RegistryRequest::create([
                    'id_B' => $exist_firm_B->id,
                    'id_create' => Auth::user()->id,
                    'urgency' => $urgency,
                    'id_shipping' => $shipping,
                    'id_status_sale' => RegistryRequest::NEW,
                    'comment_sale' => $comment,
                    'file_sale' => $file_sale,
                ]);
                //$bitrix->sendMessagePurchasesRegistry($request_registry->id, $urgency);
            }

            $id_request = $old_id!=0 ? $old_id: $request_registry->id;
            if (!empty($id_products)) {
                $old_product = RegistryProduct::where('id_request', $id_request)->select('id')->get();
                $product_for_delete = array_diff($old_product->pluck('id')->toArray(), $id_products);
                foreach ($product_for_delete as $prod_del) {
                    RegistryProduct::where('id', $prod_del)->delete();
                }
            }

            foreach ($products as $id=>$prod) {
                $exist_product = Product::where('name', $prod)->first();
                if (empty($exist_product)) {
                    $exist_product = Product::create(['name' => $prod]);
                }
                $exist_competitor = FarmsAddress::where('name', $concs[$id])->first();
                if (empty($exist_competitor) && !empty($concs[$id])) {
                    $exist_competitor = FarmsAddress::create(['name' => $concs[$id]]);
                }


                if (!empty($id_products[$id])) {
                    RegistryProduct::where('id', $id_products[$id])->update([
                        'id_product' => $exist_product->id,
                        'product_comment' => $prims[$id],
                        'product_monthly_need' => $mounths[$id],
                        'product_value' => $needs[$id],
                        'id_competitor' => (!empty($exist_competitor->id) ? $exist_competitor->id : NULL),
                        'competitor_price' => $prices[$id],
                        'competitor_delivery' => ($dostavkas[$id]=="Вид доставки" ? NULL : $dostavkas[$id]),
                        'competitor_payment' => ($uslovs[$id]=="Условия оплаты" ? NULL : $uslovs[$id]),
                        'competitor_comment' => $commcs[$id],
                        'type_request' => $deistvie[$id],
                        'type_discount' => (($vid_skidki[$id]=="Вид скидки" || $deistvie[$id]=="Запрос цены") ? NULL : $vid_skidki[$id]),
                        'price_sale' => $sale_price[$id],
                        'id_unit' => $need_unit[$id],
                    ]);
                }
                else {
                    RegistryProduct::create([
                        'id_product' => $exist_product->id,
                        'product_comment' => $prims[$id],
                        'product_monthly_need' => $mounths[$id],
                        'product_value' => $needs[$id],
                        'id_competitor' => (!empty($exist_competitor->id) ? $exist_competitor->id : NULL),
                        'competitor_price' => $prices[$id],
                        'competitor_delivery' => ($dostavkas[$id]=="Вид доставки" ? NULL : $dostavkas[$id]),
                        'competitor_payment' => ($uslovs[$id]=="Условия оплаты" ? NULL : $uslovs[$id]),
                        'competitor_comment' => $commcs[$id],
                        'id_request' => $id_request,
                        'type_request' => $deistvie[$id],
                        'type_discount' => (($vid_skidki[$id]=="Вид скидки" || $deistvie[$id]=="Запрос цены") ? NULL : $vid_skidki[$id]),
                        'price_sale' => $sale_price[$id],
                        'id_unit' => $need_unit[$id],
                    ]);
                }
            }
            return  redirect('sale/registry/new');
        }
        else {
            $shipping = Shipping::all();
            $units = Unit::all();
            if ($id == 0) {
                return view('sale.registry.new.create', compact('id','shipping', 'units'));
            }
            else {
                $request = RegistryRequest::orderByDesc('created_at')->where('id', $id)->with(['farmsAddressB', 'shipping', 'userCreate',
                    'registryProducts', 'registryProducts.product', 'registryProducts.competitor'])->first();
                return view('sale.registry.new.create', compact('id', 'shipping', 'request', 'units'));
            }
        }

    }

    public function closeRequest(Request $request, BitrixService $bitrix) {
        $id_req = $request->get('id_request_close');
        $result_sale = $request->get('result_sale');
        $comment = $request->get('comment');
        RegistryRequest::where('id', $id_req)->update(['id_status_sale' => RegistryRequest::ARCHIVE, 'result_sale' => $result_sale, 'comment_sale_result' => $comment]);
        $managers = RegistryProduct::select('id_purchase')->where('id_request', $id_req)->with('userPurchase')->distinct()->get();
        /*foreach ($managers as $manager) {
            $bitrix->sendMessageManager($id_req, $manager->userPurchase['id_bitrix'], 'close_request');
        };*/
        return  redirect('sale/registry/answers');
    }

    public function communicate(Request $request, BitrixService $bitrix) {
        $comment = $request->get('comment');
        $id_req = $request->get('id_request_comm');
        RegistryCommunication::create(['id_request' => $id_req, 'id_user' => Auth::user()->id, 'message'=>$comment]);
        $managers = RegistryProduct::select('id_purchase')->where('id_request', $id_req)->with('userPurchase')->distinct()->get();
        foreach ($managers as $manager) {
            $read_message = RegistryReadMessage::where([['id_request', $id_req],['id_user', $manager->userPurchase['id']]])->first();
            if (empty($read_message)) {
                RegistryReadMessage::create(['id_request' => $id_req, 'id_user' => $manager->userPurchase['id']]);
            }
            else {
                RegistryReadMessage::where('id', $read_message->id)->update(['read' => false]);
            }
            $bitrix->sendMessageManager($id_req, $manager->userPurchase['id_bitrix'], 'get_comment');
        };

        return  redirect('sale/registry/answers');
    }

    public function check(Request $request) {
        $id_req = $request->get('id_request');
        RegistryReadMessage::where([['id_user', Auth::user()->id], ['id_request', $id_req]])->update(['read' => true]);
    }

    public function print(Request $request) {
        $type = $request->get("page");
        if ($type == "new") {
            $requests = $this->index($request, true);
            return view('sale.registry.print', compact('requests', 'type'));
        }
        if ($type == "answers") {
            $requests = $this->answers($request, true);
            return view('sale.registry.print', compact('requests', 'type'));
        }
        if ($type == "archive") {
            $requests = $this->archive($request, true);
            return view('sale.registry.print', compact('requests', 'type'));
        }
    }
}
