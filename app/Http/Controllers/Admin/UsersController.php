<?php

namespace app\Http\Controllers\Admin;


use app\Http\Controllers\Controller;
use app\Entity\User;
use app\Http\Requests\Admin\Users\CreateRequest;
use app\Http\Requests\Admin\Users\UpdateRequest;
use app\UseCases\Auth\RegisterService;
use app\UseCases\Bitrix\BitrixService;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    private $register;

    public function __construct(RegisterService $register)
    {
        $this->register = $register;
        //$this->middleware('can:manage-users');
    }


    public function index(Request $request)
    {
        //$users = User::orderBy('id', 'desc')->paginate(20);
        //return view('admin.users.index', compact('users'));

        $query = User::orderByDesc('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if (!empty($value = $request->get('role'))) {
            $query->where('role', $value);
        }

        $users = $query->paginate(20);

        $statuses = [
            User::STATUS_WAIT => 'Ожидает подтверждения',
            User::STATUS_ACTIVE => 'Подтвержден',
        ];

        $roles = User::rolesList();

        return view('admin.users.index', compact('users', 'statuses', 'roles'));

    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateRequest $request)
    {
        $user = User::new(
            $request['name'],
            $request['email'],
            $request['password'],
            $request['id_bitrix']
        );

        return redirect()->route('admin.users.show', $user);
    }

    public function sales(BitrixService $bitrix)
    {
        $id_bitrix = User::where('id_bitrix', '<>', '')->pluck('id_bitrix')->toArray();
        $manager = $bitrix->getManagerSale();
        foreach ($manager as $man) {
            if (!in_array($man['ID'], $id_bitrix)) {
                $name = '';
                if (!empty($man['LAST_NAME'])) {
                    $name .= $man['LAST_NAME']." ";
                }
                if (!empty($man['NAME'])) {
                    $name .= $man['NAME']." ";
                }
                if (!empty($man['SECOND_NAME'])) {
                    $name .= $man['SECOND_NAME']." ";
                }
                User::new(
                    $name,
                    $man['EMAIL'],
                    $man['EMAIL'],
                    $man['ID'],
                    User::ROLE_SALE,
                    User::STATUS_ACTIVE
                );
            }
        }
        return redirect()->route('admin.users.index');
    }

    public function purchase(BitrixService $bitrix)
    {
        $id_bitrix = User::where('id_bitrix', '<>', '')->pluck('id_bitrix')->toArray();
        $manager = $bitrix->getManagerPurchase();
        foreach ($manager as $man) {
            if (!in_array($man['ID'], $id_bitrix)) {
                $name = '';
                if (!empty($man['LAST_NAME'])) {
                    $name .= $man['LAST_NAME']." ";
                }
                if (!empty($man['NAME'])) {
                    $name .= $man['NAME']." ";
                }
                if (!empty($man['SECOND_NAME'])) {
                    $name .= $man['SECOND_NAME']." ";
                }
                User::new(
                    $name,
                    $man['EMAIL'],
                    $man['EMAIL'],
                    $man['ID'],
                    User::ROLE_PURCHASE,
                    User::STATUS_ACTIVE
                );
            }
        }
        return redirect()->route('admin.users.index');
    }

    public function logist(BitrixService $bitrix)
    {
        $id_bitrix = User::where('id_bitrix', '<>', '')->pluck('id_bitrix')->toArray();
        $manager = $bitrix->getLogists();
        foreach ($manager as $man) {
            if (!in_array($man['ID'], $id_bitrix)) {
                $name = '';
                if (!empty($man['LAST_NAME'])) {
                    $name .= $man['LAST_NAME']." ";
                }
                if (!empty($man['NAME'])) {
                    $name .= $man['NAME']." ";
                }
                if (!empty($man['SECOND_NAME'])) {
                    $name .= $man['SECOND_NAME']." ";
                }
                User::new(
                    $name,
                    $man['EMAIL'],
                    $man['EMAIL'],
                    $man['ID'],
                    User::ROLE_LOGIST,
                    User::STATUS_ACTIVE
                );
            }
        }
        return redirect()->route('admin.users.index');
    }

    public function accountant(BitrixService $bitrix)
    {
        $id_bitrix = User::where('id_bitrix', '<>', '')->pluck('id_bitrix')->toArray();
        $manager = $bitrix->getAccountant();
        foreach ($manager as $man) {
            if (!in_array($man['ID'], $id_bitrix)) {
                $name = '';
                if (!empty($man['LAST_NAME'])) {
                    $name .= $man['LAST_NAME']." ";
                }
                if (!empty($man['NAME'])) {
                    $name .= $man['NAME']." ";
                }
                if (!empty($man['SECOND_NAME'])) {
                    $name .= $man['SECOND_NAME']." ";
                }
                User::new(
                    $name,
                    $man['EMAIL'],
                    $man['EMAIL'],
                    $man['ID'],
                    User::ROLE_ACCOUNTANT,
                    User::STATUS_ACTIVE
                );
            }
        }

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        $roles = User::rolesList();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(UpdateRequest $request, User $user)
    {
        $user->update($request->only(['name', 'email','id_bitrix']));
        if ($request['role'] !== $user->role) {
            $user->changeRole($request['role']);
        }
        if (!empty($request['password'])) {
            $user->password = bcrypt($request['password']);
            $user->save();
        }
        return redirect()->route('admin.users.show', $user);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }


    public function verify(User $user)
    {
        $this->register->verify($user->id);

        return redirect()->route('admin.users.show', $user);
    }

}
