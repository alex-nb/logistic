<?php

namespace app\Http\Controllers\Admin;

use app\Entity\HistoryUpdate;
use app\Entity\User;
use app\UseCases\Bitrix\BitrixService;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;


class HomeController extends Controller
{
    public function index()
    {
        return view('admin.home');
    }

    public function createRecord(Request $request, BitrixService $bitrix)
    {
        $text = $request->get("text");
        $who = $request->get("who");
        //var_dump($who);
        HistoryUpdate::create(['text' => $text, 'id_create' => \Auth::user()->id]);
        if (in_array('sale_rp', $who)) {
            $users = $bitrix->getManagerSale();
            foreach ($users as $user) {
                $bitrix->sendMessageManager(0, $user['ID'], 'update', $text);
            }
        }
        if (in_array('purchase', $who)) {
            $users = $bitrix->getManagerPurchase();
            foreach ($users as $user) {
                $bitrix->sendMessageManager(0, $user['ID'], 'update', $text);
            }
        }
        if (in_array('accountant', $who)) {
            $users = $bitrix->getAccountant();
            foreach ($users as $user) {
                $bitrix->sendMessageManager(0, $user['ID'], 'update', $text);
            }
        }
        if (in_array('logist', $who)) {
            $users = $bitrix->getLogists();
            foreach ($users as $user) {
                $bitrix->sendMessageLogist(0, 'update', $text, $user['ID']);
            }
        }
        if (in_array('sale_logist', $who)) {
            $users = $bitrix->getLogistsSale();
            foreach ($users as $user) {
                $bitrix->sendMessageLogist(0, 'update', $text, $user['ID']);
            }
        }
        return redirect('cabinet');
    }
}
