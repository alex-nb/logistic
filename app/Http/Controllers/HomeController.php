<?php

namespace app\Http\Controllers;

use app\UseCases\Bitrix\BitrixService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function notRole()
    {
        return view('no_role');
    }

    public function help(Request $request, BitrixService $bitrix)
    {
        $question = $request->get('question');
        $bitrix->taskDeveloper($question);
    }
}