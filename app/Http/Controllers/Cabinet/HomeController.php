<?php

namespace app\Http\Controllers\Cabinet;
use app\Entity\HistoryUpdate;
use app\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $history = HistoryUpdate::orderByDesc('created_at')->with(['userCreate'])->get();
        return view('cabinet.home', compact('history'));
    }
}
