<?php

namespace app\Http\Controllers\Cabinet;

use app\Http\Controllers\Controller;
//use app\Http\Requests\Auth\ProfileEditRequest;
//use app\UseCases\Profile\ProfileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //private $service;

    /*public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }*/

    public function index()
    {
        $user = Auth::user();

        return view('cabinet.profile.home', compact('user'));
    }

    public function edit()
    {
        $user = Auth::user();

        return view('cabinet.profile.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $user = Auth::user();
        $user->update($request->only('name'));
        return redirect()->route('cabinet.profile.home');
        /* try {
             $this->service->edit(Auth::id(), $request);
         } catch (\DomainException $e) {
             return redirect()->back()->with('error', $e->getMessage());
         }
         return redirect()->route('cabinet.profile.home');*/
    }
}