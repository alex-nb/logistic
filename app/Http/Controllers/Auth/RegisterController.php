<?php

namespace app\Http\Controllers\Auth;

use app\Http\Requests\Auth\RegisterRequest;
use app\Entity\User;
use app\Http\Controllers\Controller;
use app\UseCases\Auth\RegisterService;


class RegisterController extends Controller
{
    private $service;

    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $this->service->register($request);

        return redirect()->route('login')
            ->with('success', 'Check your email and click on the link to verify.');
    }

    public function verify($token)
    {
        if (!$user = User::where('verify_token', $token)->first()) {
            return redirect()->route('login')
                ->with('error', 'Sorry your link cannot be identified.');
        }

        try {
            $this->service->verify($user->id);
            return redirect()->route('login')->with('success', 'Your e-mail is verified. You can now login.');
        } catch (\DomainException $e) {
            return redirect()->route('login')->with('error', $e->getMessage());
        }
    }

    /*
       protected function validator(array $data)
       {
           return Validator::make($data, [
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users',
               'password' => 'required|string|min:6|confirmed',
           ]);
       }

       protected function create(array $data)
       {
           $user = User::create([
               'name' => $data['name'],
               'email' => $data['email'],
               'password' => Hash::make($data['password']),
               'verify_token' => Str::random(),
               'status' => User::STATUS_WAIT,
           ]);
           Mail::to($user->email)->send(new VerifyMail($user));

           return $user;
       }
   */
}
