<?php

namespace app\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use app\Entity\User;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$this->user->id,
            'role' => ['required', 'string', Rule::in(array_keys(User::rolesList()))],
            'id_bitrix' => 'required|integer',
        ];
    }
}
