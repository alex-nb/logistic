<?php

namespace app\Http\Middleware;

use Closure;

class Purchase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::user()->isPurchase() && !\Auth::user()->isAdmin() && !\Auth::user()->isDirPurchase()) {
            return redirect('/');
        }

        return $next($request);
    }
}
