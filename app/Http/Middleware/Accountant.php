<?php

namespace app\Http\Middleware;

use Closure;

class Accountant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::user()->isAccountant() && !\Auth::user()->isAdmin()) {
            return redirect('/');
        }

        return $next($request);
    }
}
