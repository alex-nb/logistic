<?php

namespace app\Http\Middleware;

use Closure;


class CheckRole
{

    public function handle($request, Closure $next)
    {
        if (\Auth::user()->isAdmin()) {
            return redirect('admin');
        }
        elseif (\Auth::user()->isLogist()) {
            return redirect('logist/not_registry/new/sale');
        }
        elseif (\Auth::user()->isSaleLogist()) {
            return redirect('sale_logist/not_registry/new');
        }
        elseif (\Auth::user()->isSale() || \Auth::user()->isDirSale()) {
            return redirect('sale/not_registry/new');
        }
        elseif (\Auth::user()->isPurchase() || \Auth::user()->isDirPurchase()) {
            return redirect('purchase/not_registry/new');
        }
        elseif (\Auth::user()->isAccountant()) {
            return redirect('accountant/order');
        }
        else {
            return redirect('home');
        }

        return $next($request);
    }
}
