<?php

namespace app\Http\Middleware;

use Closure;

class Sale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::user()->isSale() && !\Auth::user()->isAdmin() && !\Auth::user()->isDirSale()) {
            return redirect('/');
        }

        return $next($request);
    }
}
