<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\Car::class, function (Faker $faker) {
    return [
        'id_driver' => $faker->numberBetween(1,100),
        'model' => $faker->companySuffix(),
        'colour' => $faker->colorName(),
        'number' => $faker->numerify(),
    ];
});
