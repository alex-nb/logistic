<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\FarmsAddress::class, function (Faker $faker) {
    return [
        'name' => $faker->company(),
        'address' => $faker->address(),
    ];
});
