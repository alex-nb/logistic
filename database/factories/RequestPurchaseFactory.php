<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\RequestPurchase::class, function (Faker $faker) {
    return [
        'id_A' => $faker->numberBetween(1,100),
        'id_B' => $faker->numberBetween(1,100),
        'distance' => $faker->randomNumber(),
        'tonnage' => $faker->randomNumber(),
        'id_load' => $faker->numberBetween(1,3),
        'adr' => $faker->numberBetween(1,9),
        'id_package' => $faker->numberBetween(1,5),
        'pallet' => $faker->numberBetween(0,1),
        'id_shipping' => $faker->numberBetween(1,3),
        'date_export' => $faker->date(),
        'id_create' => $faker->numberBetween(1,36),
        'id_status_purchase' => 1,
        'id_status_logist' => 1,
    ];
});
