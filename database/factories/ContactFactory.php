<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'phones' => $faker->phoneNumber(),
    ];
});
