<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\Driver::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'birthday' => $faker->date(),
        'series' => $faker->numberBetween(1000, 9999),
        'number' => $faker->numberBetween(100000, 999999),
        'whom' => $faker->company(),
        'when' => $faker->date(),
    ];
});
