<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\RequestSale::class, function (Faker $faker) {
    return [
        'id_A' => $faker->numberBetween(1,100),
        'id_B' => $faker->numberBetween(1,100),
        'distance' => $faker->randomNumber(),
        'tonnage' => $faker->randomNumber(),
        'id_load' => $faker->numberBetween(1,3),
        'id_create' => $faker->numberBetween(1,36),
        'id_status_sale' => 1,
        'id_status_logist' => 1,
    ];
});
