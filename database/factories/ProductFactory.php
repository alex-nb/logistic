<?php

use Faker\Generator as Faker;

$factory->define(app\Entity\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
    ];
});
