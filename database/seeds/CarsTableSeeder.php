<?php

use Illuminate\Database\Seeder;
use app\Entity\Car;

class CarsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Car::class, 100)->create();
    }
}