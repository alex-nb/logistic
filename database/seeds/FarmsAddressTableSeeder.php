<?php

use Illuminate\Database\Seeder;
use app\Entity\FarmsAddress;

class FarmsAddressTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(FarmsAddress::class, 100)->create();
    }
}