<?php

use Illuminate\Database\Seeder;
use app\Entity\RequestPurchase;

class RequestPurchaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(RequestPurchase::class, 100)->create();
    }
}
