<?php

use Illuminate\Database\Seeder;
use app\Entity\Address;

class AddressTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Address::class, 100)->create();
    }
}