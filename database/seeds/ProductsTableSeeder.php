<?php

use Illuminate\Database\Seeder;
use app\Entity\Product;

class ProductsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Product::class, 100)->create();
    }
}