<?php

use Illuminate\Database\Seeder;
use app\Entity\Driver;

class DriversTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Driver::class, 100)->create();
    }
}