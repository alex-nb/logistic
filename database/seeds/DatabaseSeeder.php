<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        //$this->call(DriversTableSeeder::class);
        //$this->call(CarsTableSeeder::class);
        //$this->call(ContactsTableSeeder::class);
        //$this->call(FarmsAddressTableSeeder::class);
        //$this->call(ProductsTableSeeder::class);
        $this->call(RequestPurchaseTableSeeder::class);
        $this->call(RequestSaleTableSeeder::class);
    }
}
