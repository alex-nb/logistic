<?php

use Illuminate\Database\Seeder;
use app\Entity\Contact;

class ContactsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Contact::class, 100)->create();
    }
}