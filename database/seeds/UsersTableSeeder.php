<?php

use Illuminate\Database\Seeder;
use app\Entity\User;

class UsersTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(User::class, 10)->create();
    }
}