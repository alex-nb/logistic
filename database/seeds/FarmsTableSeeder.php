<?php

use Illuminate\Database\Seeder;
use app\Entity\Farm;

class FarmsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Farm::class, 100)->create();
    }
}