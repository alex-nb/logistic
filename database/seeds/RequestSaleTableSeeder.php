<?php

use Illuminate\Database\Seeder;
use app\Entity\RequestSale;

class RequestSaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(RequestSale::class, 100)->create();
    }
}
