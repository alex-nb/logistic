<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropColumn(['type_request', 'type_discount', 'price_sale']);
        });
        Schema::table('registry_products', function (Blueprint $table) {
            $table->string('type_request', 25)->nullable();
            $table->string('type_discount', 25)->nullable();
            $table->string('price_sale', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
