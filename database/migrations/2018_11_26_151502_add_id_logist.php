<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdLogist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->integer('id_logist')->unsigned()->nullable();
            $table->foreign('id_logist')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropForeign(['id_logist']);
        });
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropColumn(['id_logist']);
        });
    }
}
