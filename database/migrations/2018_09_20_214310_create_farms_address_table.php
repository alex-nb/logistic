<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmsAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farms_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('country', 100);
            $table->string('city', 100);
            $table->string('street', 100);
            $table->string('house', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farms_address');
    }
}
