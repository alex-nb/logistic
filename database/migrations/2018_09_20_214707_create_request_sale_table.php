<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_A');
            $table->integer('id_B');
            $table->integer('distance');
            $table->integer('tonnage');
            $table->integer('id_load');
            $table->integer('id_create');
            $table->timestamp('date_answer')->nullable();
            $table->integer('price')->nullable();
            $table->integer('id_logist')->nullable();
            $table->string('comment')->nullable();
            $table->integer('id_status_sale');
            $table->integer('id_status_logist');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_sale');
    }
}
