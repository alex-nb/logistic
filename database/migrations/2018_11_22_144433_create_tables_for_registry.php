<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_create');
            $table->string('urgency');
            $table->integer('id_B');
            $table->string('type_request');
            $table->integer('id_shipping');
            $table->integer('id_status_sale');
            $table->string('price_sale')->nullable();
            $table->string('comment_sale')->nullable();
            $table->string('type_discount')->nullable();
            $table->string('comment_purchase')->nullable();
            $table->timestamp('date_answer')->nullable();
            $table->string('comment_sale_result')->nullable();
            $table->timestamps();
        });

        Schema::create('registry_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_request');
            $table->integer('id_product');
            $table->string('product_comment')->nullable();
            $table->string('product_monthly_need')->nullable();
            $table->integer('product_value')->nullable();
            $table->integer('id_competitor')->nullable();
            $table->string('competitor_price')->nullable();
            $table->string('competitor_delivery')->nullable();
            $table->string('competitor_payment')->nullable();
            $table->string('competitor_comment')->nullable();
            $table->string('answer')->nullable();
            $table->string('price_purchase')->nullable();
            $table->string('place_take')->nullable();
            $table->string('gross_profit')->nullable();
            $table->integer('time_active_price')->nullable();
            $table->string('result_sale')->nullable();
        });

        DB::table('status_sale')->insert([
            ['name' => 'registry_new'],
            ['name' => 'registry_answer'],
            ['name' => 'registry_archive'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_products');
        Schema::dropIfExists('registry_request');
    }
}
