<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRequestPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->integer('id_accountant')->unsigned()->nullable()->after('id_status_accountant');
            $table->foreign('id_accountant')->references('id')->on('users');
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->integer('distance')->nullable()->change();
        });
        DB::table('packaging')->insert([['name' => 'Канистра']]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->dropForeign(['id_accountant']);
            $table->dropColumn('id_accountant');
        });
    }
}
