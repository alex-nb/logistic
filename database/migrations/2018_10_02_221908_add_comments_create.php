<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->string('comment_create')->nullable()->after('id');
        });
        Schema::table('request_sale', function (Blueprint $table) {
            $table->string('comment_create')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->dropColumn(['comment_create']);
        });
        Schema::table('request_sale', function (Blueprint $table) {
            $table->dropColumn(['comment_create']);
        });
    }
}
