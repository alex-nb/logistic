<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCheckMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_read_message', function (Blueprint $table) {
            $table->dropForeign(['id_message']);
            $table->dropColumn(['id_message']);
            $table->unsignedInteger('id_request');
            $table->foreign('id_request')->references('id')->on('registry_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_read_message', function (Blueprint $table) {
            $table->dropForeign(['registry_request']);
            $table->dropColumn(['registry_request']);
            $table->unsignedInteger('id_message');
            $table->foreign('id_message')->references('id')->on('registry_communications');
        });
    }
}
