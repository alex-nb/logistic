<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->string('comment_create', 500)->nullable()->change();
            $table->string('comment', 500)->nullable()->change();
        });
        Schema::table('request_sale', function (Blueprint $table) {
            $table->string('comment_create', 500)->nullable()->change();
            $table->string('comment', 500)->nullable()->change();
        });
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->string('comment_consignee', 500)->nullable()->change();
            $table->string('comment_shipper', 500)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
