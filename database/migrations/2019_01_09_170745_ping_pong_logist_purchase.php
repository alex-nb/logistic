<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PingPongLogistPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->timestamp('deadline')->nullable();
        });

        Schema::create('request_purchase_accepted_history_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_request');
            $table->foreign('id_request')->references('id')->on('request_purchase');
            $table->timestamp('date_export')->nullable();
            $table->string('price', 20)->nullable();
            $table->string('comment_logist', 500)->nullable();
            $table->timestamp('date_answer_logist')->nullable();
            $table->unsignedInteger('id_logist');
            $table->foreign('id_logist')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
