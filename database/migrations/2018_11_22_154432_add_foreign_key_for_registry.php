<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyForRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->integer('id_create')->unsigned()->change();
            $table->foreign('id_create')->references('id')->on('users');
            $table->integer('id_B')->unsigned()->change();
            $table->foreign('id_B')->references('id')->on('farms_address');
            $table->integer('id_shipping')->unsigned()->change();
            $table->foreign('id_shipping')->references('id')->on('shipping');
            $table->integer('id_status_sale')->unsigned()->change();
            $table->foreign('id_status_sale')->references('id')->on('status_sale');
        });
        Schema::table('registry_products', function (Blueprint $table) {
            $table->integer('id_request')->unsigned()->change();
            $table->foreign('id_request')->references('id')->on('registry_request');
            $table->integer('id_product')->unsigned()->change();
            $table->foreign('id_product')->references('id')->on('products');
            $table->integer('id_competitor')->unsigned()->change();
            $table->foreign('id_competitor')->references('id')->on('farms_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropForeign(['id_create']);
            $table->dropForeign(['id_B']);
            $table->dropForeign(['id_shipping']);
            $table->dropForeign(['id_status_sale']);
        });
        Schema::table('registry_products', function (Blueprint $table) {
            $table->dropForeign(['id_request']);
            $table->dropForeign(['id_product']);
            $table->dropForeign(['id_competitor']);
        });
    }
}
