<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblemRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('status_purchase')->insert([
            ['name' => 'problem_request'],
        ]);
        DB::table('status_logist')->insert([
            ['name' => 'problem_request'],
        ]);
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->string('comment_problem')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
