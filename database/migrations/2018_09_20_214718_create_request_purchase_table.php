<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_A');
            $table->integer('id_B');
            $table->integer('distance');
            $table->integer('tonnage');
            $table->integer('id_load');
            $table->integer('adr');
            $table->integer('id_package');
            $table->boolean('pallet');
            $table->integer('id_shipping');
            $table->dateTime('date_export');
            $table->integer('id_create');
            $table->timestamp('date_answer')->nullable();
            $table->integer('price')->nullable();
            $table->integer('id_logist')->nullable();
            $table->string('comment')->nullable();
            $table->integer('id_status_purchase');
            $table->integer('id_status_logist');
            $table->timestamp('date_archive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_purchase');
    }
}
