<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataStartSearchCar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->timestamp('date_start_search')->nullable()->after('num_one_s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->dropColumn('date_start_search');
        });
    }
}
