<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaxLengthFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->string('file_shipping', 1000)->nullable()->change();
            $table->string('file_proxy', 1000)->nullable()->change();
            $table->string('file_vet', 1000)->nullable()->change();
            $table->string('file_product', 1000)->nullable()->change();
            $table->string('file_torg', 1000)->nullable()->change();
            $table->string('file_torg_sign', 1000)->nullable()->change();
            $table->string('file_invoice', 1000)->nullable()->change();
            $table->string('file_tnn', 1000)->nullable()->change();
            $table->string('file_shipment', 1000)->nullable()->change();
            $table->string('file_bill', 1000)->nullable()->change();
            $table->string('file_act', 1000)->nullable()->change();
            $table->string('file_sf', 1000)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->string('file_shipping', 255)->nullable()->change();
            $table->string('file_proxy', 255)->nullable()->change();
            $table->string('file_vet', 255)->nullable()->change();
            $table->string('file_product', 255)->nullable()->change();
            $table->string('file_torg', 255)->nullable()->change();
            $table->string('file_torg_sign', 255)->nullable()->change();
            $table->string('file_invoice', 255)->nullable()->change();
            $table->string('file_tnn', 255)->nullable()->change();
            $table->string('file_shipment', 255)->nullable()->change();
            $table->string('file_bill', 255)->nullable()->change();
            $table->string('file_act', 255)->nullable()->change();
            $table->string('file_sf', 255)->nullable()->change();
        });
    }
}
