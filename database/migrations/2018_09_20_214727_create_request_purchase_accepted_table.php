<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPurchaseAcceptedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_purchase_accepted', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_shipper');
            $table->string('comment_shipper')->nullable();
            $table->integer('id_consignee');
            $table->string('comment_consignee')->nullable();
            $table->integer('bitrix_id_manager');
            $table->string('name_manager', 100);
            $table->integer('num_one_s');
            $table->integer('id_logist')->nullable();
            $table->integer('id_car')->nullable();
            $table->integer('id_status_purchase');
            $table->integer('id_status_logist');
            $table->timestamp('date_find_car')->nullable();
            $table->string('file_proxy')->nullable();
            $table->string('file_vet')->nullable();
            $table->timestamp('date_answer_purchase')->nullable();
            $table->string('file_product')->nullable();
            $table->timestamp('date_come_car_send')->nullable();
            $table->string('file_torg')->nullable();
            $table->string('file_torg_sign')->nullable();
            $table->string('file_invoice')->nullable();
            $table->string('file_tnn')->nullable();
            $table->timestamp('date_answer_buh')->nullable();
            $table->string('file_shipment')->nullable();
            $table->string('file_bill')->nullable();
            $table->string('file_act')->nullable();
            $table->timestamp('date_come_car_take')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_purchase_accepted');
    }
}
