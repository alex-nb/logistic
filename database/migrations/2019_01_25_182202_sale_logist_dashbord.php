<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaleLogistDashbord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_sale_logist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
        });

        DB::table('status_sale_logist')->insert([
            ['name' => 'loading'],
            ['name' => 'get_answer'],
            ['name' => 'take'],
            ['name' => 'search_car'],
            ['name' => 'way_sender'],
            ['name' => 'way_recipient'],
            ['name' => 'in_recipient'],
            ['name' => 'archive_realized'],
            ['name' => 'archive_unrealized'],
            ['name' => 'find'],
            ['name' => 'problem_request']
        ]);

        Schema::create('request_sale_logist_accepted', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_status_accountant')->nullable();
            $table->foreign('id_status_accountant')->references('id')->on('status_accountant');
            $table->unsignedInteger('id_accountant')->nullable();
            $table->foreign('id_accountant')->references('id')->on('users');
            $table->string('file_shipping', 1000)->nullable();
            $table->unsignedInteger('id_shipper');
            $table->foreign('id_shipper')->references('id')->on('contacts');
            $table->string('comment_shipper')->nullable();
            $table->unsignedInteger('id_consignee');
            $table->foreign('id_consignee')->references('id')->on('contacts');
            $table->string('comment_consignee', 500)->nullable();
            $table->integer('bitrix_id_manager')->nullable();
            $table->string('name_manager', 100)->nullable();
            $table->integer('num_one_s')->nullable();
            $table->integer('num_provider')->nullable();
            $table->integer('num_customer')->nullable();
            $table->dateTime('date_start_search')->nullable();
            $table->unsignedInteger('id_car')->nullable();
            $table->foreign('id_car')->references('id')->on('cars');
            $table->timestamp('date_find_car')->nullable();
            $table->string('file_proxy', 1000)->nullable();
            $table->string('file_vet', 1000)->nullable();
            $table->dateTime('date_answer_sale_logist')->nullable();
            $table->string('file_product', 1000)->nullable();
            $table->dateTime('date_come_car_send')->nullable();
            $table->string('file_torg', 1000)->nullable();
            $table->string('file_torg_sign', 1000)->nullable();
            $table->string('file_invoice', 1000)->nullable();
            $table->string('file_tnn', 1000)->nullable();
            $table->dateTime('date_answer_buh')->nullable();
            $table->string('file_shipment', 1000)->nullable();
            $table->string('file_bill', 1000)->nullable();
            $table->string('file_act', 1000)->nullable();
            $table->dateTime('date_come_car_take')->nullable();
            $table->dateTime('date_archive')->nullable();
            $table->string('file_sf', 1000)->nullable();
            $table->string('num_one_s_logist', 255)->nullable();
            $table->string('num_logistic', 50)->nullable();
            $table->timestamps();
        });

        Schema::create('request_sale_logist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment_create', 500)->nullable();
            $table->unsignedInteger('id_A');
            $table->foreign('id_A')->references('id')->on('farms_address');
            $table->unsignedInteger('id_B');
            $table->foreign('id_B')->references('id')->on('farms_address');
            $table->integer('distance')->nullable();
            $table->integer('tonnage');
            $table->unsignedInteger('id_load');
            $table->foreign('id_load')->references('id')->on('type_load');
            $table->integer('adr');
            $table->unsignedInteger('id_package');
            $table->foreign('id_package')->references('id')->on('packaging');
            $table->boolean('pallet');
            $table->unsignedInteger('id_shipping');
            $table->foreign('id_shipping')->references('id')->on('shipping');
            $table->dateTime('date_export');
            $table->unsignedInteger('id_create');
            $table->foreign('id_create')->references('id')->on('users');
            $table->dateTime('date_answer')->nullable();
            $table->integer('price')->nullable();
            $table->unsignedInteger('id_logist')->nullable();
            $table->foreign('id_logist')->references('id')->on('users');
            $table->string('comment', 500)->nullable();
            $table->unsignedInteger('id_status_sale_logist');
            $table->foreign('id_status_sale_logist')->references('id')->on('status_sale_logist');
            $table->unsignedInteger('id_status_logist');
            $table->foreign('id_status_logist')->references('id')->on('status_logist');
            $table->dateTime('date_archive')->nullable();
            $table->unsignedInteger('id_request_accept')->nullable();
            $table->foreign('id_request_accept')->references('id')->on('request_sale_logist_accepted');
            $table->string('comment_problem', 500)->nullable();
            $table->timestamps();
        });

        Schema::create('request_sale_logist_accepted_history_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_request');
            $table->foreign('id_request')->references('id')->on('request_sale_logist');
            $table->dateTime('date_export')->nullable();
            $table->string('price', 20)->nullable();
            $table->string('comment_logist', 500)->nullable();
            $table->dateTime('date_answer_logist')->nullable();
            $table->unsignedInteger('id_logist')->nullable();
            $table->foreign('id_logist')->references('id')->on('users');
        });

        Schema::create('request_sale_logist_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_request');
            $table->foreign('id_request')->references('id')->on('request_sale_logist');
            $table->unsignedInteger('id_product');
            $table->foreign('id_product')->references('id')->on('products');
        });

        Schema::create('request_sale_logist_accepted_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_request');
            $table->foreign('id_request')->references('id')->on('request_sale_logist_accepted');
            $table->unsignedInteger('id_product');
            $table->foreign('id_product')->references('id')->on('products');
            $table->string('unit', 50);
            $table->integer('weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
