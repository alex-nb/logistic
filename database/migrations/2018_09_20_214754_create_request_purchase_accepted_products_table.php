<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPurchaseAcceptedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_purchase_accepted_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_request');
            $table->integer('id_product');
            $table->string('unit', 50);
            $table->integer('weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_purchase_accepted_products');
    }
}
