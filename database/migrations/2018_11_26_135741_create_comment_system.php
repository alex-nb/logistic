<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_communications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_request')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->string('message', 500);
            $table->timestamps();
        });
        Schema::table('registry_communications', function (Blueprint $table) {
            $table->foreign('id_request')->references('id')->on('registry_request');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_communications', function (Blueprint $table) {
            $table->dropForeign(['id_request']);
            $table->dropForeign(['id_user']);
        });
        Schema::dropIfExists('registry_communications');
    }
}
