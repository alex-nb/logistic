<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompleteData extends Migration
{
    public function up()
    {
        DB::table('type_load')->insert([
            ['name' => 'Задняя'],
            ['name' => 'Верхняя'],
            ['name' => 'Боковая']
        ]);
        DB::table('status_sale')->insert([
            ['name' => 'loading'],
            ['name' => 'get_answer'],
            ['name' => 'archive_answers'],
            ['name' => 'archive_complete']
        ]);
        DB::table('status_purchase')->insert([
            ['name' => 'loading'],
            ['name' => 'get_answer'],
            ['name' => 'take'],
            ['name' => 'search_car'],
            ['name' => 'way_sender'],
            ['name' => 'way_recipient'],
            ['name' => 'in_recipient'],
            ['name' => 'archive_realized'],
            ['name' => 'archive_unrealized'],
            ['name' => 'find']
        ]);
        DB::table('status_logist')->insert([
            ['name' => 'new_sale'],
            ['name' => 'request_car'],
            ['name' => 'search_car'],
            ['name' => 'archive_complete'],
            ['name' => 'delivery'],
            ['name' => 'payment'],
            ['name' => 'archive_purchase'],
            ['name' => 'new_purchase'],
            ['name' => 'archive_sale'],
            ['name' => 'out'],
        ]);
        DB::table('shipping')->insert([
            ['name' => 'Транзит'],
            ['name' => 'Доставка на склад'],
            ['name' => 'Отгрузка со склада']
        ]);
        DB::table('packaging')->insert([
            ['name' => 'Навалом'],
            ['name' => 'Мешки'],
            ['name' => 'Биг-беги'],
            ['name' => 'Коробки'],
            ['name' => 'Обрешетка']
        ]);
    }

    public function down()
    {
        DB::table('request_purchase')->delete();
        DB::table('request_purchase_accepted')->delete();
        DB::table('request_sale')->delete();
        DB::table('status_purchase')->delete();
        DB::table('status_logist')->delete();
        DB::table('type_load')->delete();
        DB::table('status_sale')->delete();
        DB::table('shipping')->delete();
        DB::table('packaging')->delete();
    }
}
