<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_sale', function (Blueprint $table) {
            $table->integer('id_A')->unsigned()->change();
            $table->foreign('id_A')->references('id')->on('farms_address');
            $table->integer('id_B')->unsigned()->change();
            $table->foreign('id_B')->references('id')->on('farms_address');
            $table->integer('id_load')->unsigned()->change();
            $table->foreign('id_load')->references('id')->on('type_load');
            $table->integer('id_create')->unsigned()->change();
            $table->foreign('id_create')->references('id')->on('users');
            $table->integer('id_logist')->unsigned()->change();
            $table->foreign('id_logist')->references('id')->on('users');
            $table->integer('id_status_sale')->unsigned()->change();
            $table->foreign('id_status_sale')->references('id')->on('status_sale');
            $table->integer('id_status_logist')->unsigned()->change();
            $table->foreign('id_status_logist')->references('id')->on('status_logist');
        });
        Schema::table('request_purchase_products', function (Blueprint $table) {
            $table->integer('id_request')->unsigned()->change();
            $table->foreign('id_request')->references('id')->on('request_purchase');
            $table->integer('id_product')->unsigned()->change();
            $table->foreign('id_product')->references('id')->on('products');
        });
        Schema::table('request_purchase_accepted_products', function (Blueprint $table) {
            $table->integer('id_request')->unsigned()->change();
            $table->foreign('id_request')->references('id')->on('request_purchase_accepted');
            $table->integer('id_product')->unsigned()->change();
            $table->foreign('id_product')->references('id')->on('products');
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('id_driver')->unsigned()->change();
            $table->foreign('id_driver')->references('id')->on('drivers');
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->integer('id_A')->unsigned()->change();
            $table->foreign('id_A')->references('id')->on('farms_address');
            $table->integer('id_B')->unsigned()->change();
            $table->foreign('id_B')->references('id')->on('farms_address');
            $table->integer('id_load')->unsigned()->change();
            $table->foreign('id_load')->references('id')->on('type_load');
            $table->integer('id_package')->unsigned()->change();
            $table->foreign('id_package')->references('id')->on('packaging');
            $table->integer('id_shipping')->unsigned()->change();
            $table->foreign('id_shipping')->references('id')->on('shipping');
            $table->integer('id_create')->unsigned()->change();
            $table->foreign('id_create')->references('id')->on('users');
            $table->integer('id_logist')->unsigned()->change();
            $table->foreign('id_logist')->references('id')->on('users');
            $table->integer('id_status_purchase')->unsigned()->change();
            $table->foreign('id_status_purchase')->references('id')->on('status_purchase');
            $table->integer('id_status_logist')->unsigned()->change();
        });
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->integer('id_shipper')->unsigned()->change();
            $table->foreign('id_shipper')->references('id')->on('contacts');
            $table->integer('id_consignee')->unsigned()->change();
            $table->foreign('id_consignee')->references('id')->on('contacts');
            $table->integer('id_logist')->unsigned()->change();
            $table->foreign('id_logist')->references('id')->on('users');
            $table->integer('id_car')->unsigned()->change();
            $table->foreign('id_car')->references('id')->on('cars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_sale', function (Blueprint $table) {
            $table->dropForeign(['id_A']);
            $table->dropForeign(['id_B']);
            $table->dropForeign(['id_load']);
            $table->dropForeign(['id_create']);
            $table->dropForeign(['id_logist']);
            $table->dropForeign(['id_status_sale']);
            $table->dropForeign(['id_status_logist']);
        });
        Schema::table('request_purchase_products', function (Blueprint $table) {
            $table->dropForeign(['id_request']);
            $table->dropForeign(['id_product']);
        });
        Schema::table('request_purchase_accepted_products', function (Blueprint $table) {
            $table->dropForeign(['id_request']);
            $table->dropForeign(['id_product']);
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['id_driver']);
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->dropForeign(['id_A']);
            $table->dropForeign(['id_B']);
            $table->dropForeign(['id_load']);
            $table->dropForeign(['id_package']);
            $table->dropForeign(['id_shipping']);
            $table->dropForeign(['id_create']);
            $table->dropForeign(['id_logist']);
            $table->dropForeign(['id_status_purchase']);
            $table->dropForeign(['id_status_logist']);
        });
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->dropForeign(['id_shipper']);
            $table->dropForeign(['id_consignee']);
            $table->dropForeign(['id_logist']);
            $table->dropForeign(['id_car']);
        });
    }
}
