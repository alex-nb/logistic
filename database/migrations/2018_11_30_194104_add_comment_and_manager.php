<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentAndManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropForeign(['id_logist']);
            $table->dropColumn(['date_answer', 'id_purchase']);
        });
        Schema::table('registry_products', function (Blueprint $table) {
            $table->timestamp('date_answer')->nullable();
            $table->integer('id_purchase')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
