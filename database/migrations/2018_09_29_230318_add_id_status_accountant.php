<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdStatusAccountant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_accountant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
        });
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->integer('id_status_accountant')->unsigned()->nullable()->after('id');
            $table->foreign('id_status_accountant')->references('id')->on('status_accountant');
        });
        DB::table('status_accountant')->insert([
            ['name' => 'new'],
            ['name' => 'archive'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->dropForeign(['id_status_accountant']);
            $table->dropColumn('id_status_accountant');
        });
        Schema::dropIfExists('status_accountant');
    }
}
