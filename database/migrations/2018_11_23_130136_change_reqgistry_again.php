<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReqgistryAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_products', function (Blueprint $table) {
            $table->integer('id_request')->unsigned();
            $table->foreign('id_request')->references('id')->on('registry_request');
        });
        Schema::table('registry_request', function (Blueprint $table) {
            $table->dropForeign(['id_registry_prod']);
            $table->dropColumn(['id_registry_prod']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
