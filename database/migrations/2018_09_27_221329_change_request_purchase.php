<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRequestPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('request_purchase_accepted')->delete();
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->dropColumn('id_status_purchase');
            $table->dropColumn('id_status_logist');
            $table->dropColumn('id_logist');
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->string('id_request_accept')->nullable();
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->integer('id_request_accept')->unsigned()->change();
            $table->foreign('id_request_accept')->references('id')->on('request_purchase_accepted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->integer('id_request');
            $table->integer('id_status_purchase');
            $table->integer('id_status_logist');
            $table->integer('id_logist');
        });
        Schema::table('request_purchase_accepted', function (Blueprint $table) {
            $table->integer('id_request')->unsigned()->change();
            $table->foreign('id_request')->references('id')->on('request_purchase');
            $table->integer('id_status_purchase')->unsigned()->change();
            $table->foreign('id_status_purchase')->references('id')->on('status_purchase');
            $table->integer('id_status_logist')->unsigned()->change();
            $table->foreign('id_status_logist')->references('id')->on('status_logist');
            $table->integer('id_logist')->unsigned()->change();
            $table->foreign('id_logist')->references('id')->on('users');
        });
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->dropForeign(['id_request_accept']);
            $table->dropColumn('id_request_accept');
        });
    }
}
