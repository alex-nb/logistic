<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefreshTimestampAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->dateTime('date_export')->change();
            //$table->timestamp('date_export')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_purchase', function (Blueprint $table) {
            $table->timestamp('date_export')->nullable()->change();
            //$table->timestamp('date_export')->nullable()->change();
        });
    }
}
