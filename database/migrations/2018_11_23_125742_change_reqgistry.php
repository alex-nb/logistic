<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReqgistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_request', function (Blueprint $table) {
            $table->integer('id_registry_prod')->unsigned()->nullable();
            $table->foreign('id_registry_prod')->references('id')->on('registry_products');
        });
        Schema::table('registry_products', function (Blueprint $table) {
            $table->dropForeign(['id_request']);
            $table->dropColumn(['id_request']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
