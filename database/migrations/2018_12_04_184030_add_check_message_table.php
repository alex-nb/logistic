<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_read_message', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_message');
            $table->unsignedInteger('id_user');
            $table->boolean('read')->default(false);
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_message')->references('id')->on('registry_communications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_read_message', function (Blueprint $table) {
            $table->dropForeign(['id_user']);
            $table->dropForeign(['id_message']);
        });
        Schema::dropIfExists('registry_read_message');
    }
}
