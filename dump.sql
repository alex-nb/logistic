-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: logistic
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_driver` int(10) unsigned NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colour` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cars_id_driver_foreign` (`id_driver`),
  CONSTRAINT `cars_id_driver_foreign` FOREIGN KEY (`id_driver`) REFERENCES `drivers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
-- Table structure for table `drivers`
--

DROP TABLE IF EXISTS `drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date NOT NULL,
  `series` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `whom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `when` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `farms_address`
--

DROP TABLE IF EXISTS `farms_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `farms_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `packaging`
--

DROP TABLE IF EXISTS `packaging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packaging` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packaging`
--

LOCK TABLES `packaging` WRITE;
/*!40000 ALTER TABLE `packaging` DISABLE KEYS */;
INSERT INTO `packaging` VALUES (1,'Навалом'),(2,'Мешки'),(3,'Биг-беги'),(4,'Коробки'),(5,'Обрешетка');
/*!40000 ALTER TABLE `packaging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_purchase`
--

DROP TABLE IF EXISTS `request_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_purchase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_A` int(10) unsigned NOT NULL,
  `id_B` int(10) unsigned NOT NULL,
  `distance` int(11) NOT NULL,
  `tonnage` int(11) NOT NULL,
  `id_load` int(10) unsigned NOT NULL,
  `adr` int(11) NOT NULL,
  `id_package` int(10) unsigned NOT NULL,
  `pallet` tinyint(1) NOT NULL,
  `id_shipping` int(10) unsigned NOT NULL,
  `date_export` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_create` int(10) unsigned NOT NULL,
  `date_answer` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `id_logist` int(10) unsigned DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_status_purchase` int(10) unsigned NOT NULL,
  `id_status_logist` int(10) unsigned NOT NULL,
  `date_archive` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_request_accept` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_purchase_id_a_foreign` (`id_A`),
  KEY `request_purchase_id_b_foreign` (`id_B`),
  KEY `request_purchase_id_load_foreign` (`id_load`),
  KEY `request_purchase_id_package_foreign` (`id_package`),
  KEY `request_purchase_id_shipping_foreign` (`id_shipping`),
  KEY `request_purchase_id_create_foreign` (`id_create`),
  KEY `request_purchase_id_logist_foreign` (`id_logist`),
  KEY `request_purchase_id_status_purchase_foreign` (`id_status_purchase`),
  KEY `request_purchase_id_status_logist_foreign` (`id_status_logist`),
  KEY `request_purchase_id_request_accept_foreign` (`id_request_accept`),
  CONSTRAINT `request_purchase_id_a_foreign` FOREIGN KEY (`id_A`) REFERENCES `farms_address` (`id`),
  CONSTRAINT `request_purchase_id_b_foreign` FOREIGN KEY (`id_B`) REFERENCES `farms_address` (`id`),
  CONSTRAINT `request_purchase_id_create_foreign` FOREIGN KEY (`id_create`) REFERENCES `users` (`id`),
  CONSTRAINT `request_purchase_id_load_foreign` FOREIGN KEY (`id_load`) REFERENCES `type_load` (`id`),
  CONSTRAINT `request_purchase_id_logist_foreign` FOREIGN KEY (`id_logist`) REFERENCES `users` (`id`),
  CONSTRAINT `request_purchase_id_package_foreign` FOREIGN KEY (`id_package`) REFERENCES `packaging` (`id`),
  CONSTRAINT `request_purchase_id_request_accept_foreign` FOREIGN KEY (`id_request_accept`) REFERENCES `request_purchase_accepted` (`id`),
  CONSTRAINT `request_purchase_id_shipping_foreign` FOREIGN KEY (`id_shipping`) REFERENCES `shipping` (`id`),
  CONSTRAINT `request_purchase_id_status_logist_foreign` FOREIGN KEY (`id_status_logist`) REFERENCES `status_logist` (`id`),
  CONSTRAINT `request_purchase_id_status_purchase_foreign` FOREIGN KEY (`id_status_purchase`) REFERENCES `status_purchase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_purchase_accepted`
--

DROP TABLE IF EXISTS `request_purchase_accepted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_purchase_accepted` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_status_accountant` int(10) unsigned DEFAULT NULL,
  `file_shipping` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_shipper` int(10) unsigned NOT NULL,
  `comment_shipper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_consignee` int(10) unsigned NOT NULL,
  `comment_consignee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bitrix_id_manager` int(11) NOT NULL,
  `name_manager` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_one_s` int(11) NOT NULL,
  `date_start_search` timestamp NULL DEFAULT NULL,
  `id_car` int(10) unsigned DEFAULT NULL,
  `date_find_car` timestamp NULL DEFAULT NULL,
  `file_proxy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_vet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_answer_purchase` timestamp NULL DEFAULT NULL,
  `file_product` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_come_car_send` timestamp NULL DEFAULT NULL,
  `file_torg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_torg_sign` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_tnn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_answer_buh` timestamp NULL DEFAULT NULL,
  `file_shipment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_bill` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_act` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_come_car_take` timestamp NULL DEFAULT NULL,
  `date_archive` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_purchase_accepted_id_shipper_foreign` (`id_shipper`),
  KEY `request_purchase_accepted_id_consignee_foreign` (`id_consignee`),
  KEY `request_purchase_accepted_id_car_foreign` (`id_car`),
  KEY `request_purchase_accepted_id_status_accountant_foreign` (`id_status_accountant`),
  CONSTRAINT `request_purchase_accepted_id_car_foreign` FOREIGN KEY (`id_car`) REFERENCES `cars` (`id`),
  CONSTRAINT `request_purchase_accepted_id_consignee_foreign` FOREIGN KEY (`id_consignee`) REFERENCES `contacts` (`id`),
  CONSTRAINT `request_purchase_accepted_id_shipper_foreign` FOREIGN KEY (`id_shipper`) REFERENCES `contacts` (`id`),
  CONSTRAINT `request_purchase_accepted_id_status_accountant_foreign` FOREIGN KEY (`id_status_accountant`) REFERENCES `status_accountant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `request_purchase_accepted_products`
--

DROP TABLE IF EXISTS `request_purchase_accepted_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_purchase_accepted_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_request` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `request_purchase_accepted_products_id_request_foreign` (`id_request`),
  KEY `request_purchase_accepted_products_id_product_foreign` (`id_product`),
  CONSTRAINT `request_purchase_accepted_products_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`),
  CONSTRAINT `request_purchase_accepted_products_id_request_foreign` FOREIGN KEY (`id_request`) REFERENCES `request_purchase_accepted` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `request_purchase_products`
--

DROP TABLE IF EXISTS `request_purchase_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_purchase_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_request` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `request_purchase_products_id_request_foreign` (`id_request`),
  KEY `request_purchase_products_id_product_foreign` (`id_product`),
  CONSTRAINT `request_purchase_products_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`),
  CONSTRAINT `request_purchase_products_id_request_foreign` FOREIGN KEY (`id_request`) REFERENCES `request_purchase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `request_sale`
--

DROP TABLE IF EXISTS `request_sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_sale` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_A` int(10) unsigned NOT NULL,
  `id_B` int(10) unsigned NOT NULL,
  `distance` int(11) NOT NULL,
  `tonnage` int(11) NOT NULL,
  `id_load` int(10) unsigned NOT NULL,
  `id_create` int(10) unsigned NOT NULL,
  `date_answer` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `id_logist` int(10) unsigned DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_status_sale` int(10) unsigned NOT NULL,
  `id_status_logist` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_sale_id_a_foreign` (`id_A`),
  KEY `request_sale_id_b_foreign` (`id_B`),
  KEY `request_sale_id_load_foreign` (`id_load`),
  KEY `request_sale_id_create_foreign` (`id_create`),
  KEY `request_sale_id_logist_foreign` (`id_logist`),
  KEY `request_sale_id_status_sale_foreign` (`id_status_sale`),
  KEY `request_sale_id_status_logist_foreign` (`id_status_logist`),
  CONSTRAINT `request_sale_id_a_foreign` FOREIGN KEY (`id_A`) REFERENCES `farms_address` (`id`),
  CONSTRAINT `request_sale_id_b_foreign` FOREIGN KEY (`id_B`) REFERENCES `farms_address` (`id`),
  CONSTRAINT `request_sale_id_create_foreign` FOREIGN KEY (`id_create`) REFERENCES `users` (`id`),
  CONSTRAINT `request_sale_id_load_foreign` FOREIGN KEY (`id_load`) REFERENCES `type_load` (`id`),
  CONSTRAINT `request_sale_id_logist_foreign` FOREIGN KEY (`id_logist`) REFERENCES `users` (`id`),
  CONSTRAINT `request_sale_id_status_logist_foreign` FOREIGN KEY (`id_status_logist`) REFERENCES `status_logist` (`id`),
  CONSTRAINT `request_sale_id_status_sale_foreign` FOREIGN KEY (`id_status_sale`) REFERENCES `status_sale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shipping`
--

DROP TABLE IF EXISTS `shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping`
--

LOCK TABLES `shipping` WRITE;
/*!40000 ALTER TABLE `shipping` DISABLE KEYS */;
INSERT INTO `shipping` VALUES (1,'Транзит'),(2,'Доставка на склад'),(3,'Отгрузка со склада');
/*!40000 ALTER TABLE `shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_accountant`
--

DROP TABLE IF EXISTS `status_accountant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_accountant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_accountant`
--

LOCK TABLES `status_accountant` WRITE;
/*!40000 ALTER TABLE `status_accountant` DISABLE KEYS */;
INSERT INTO `status_accountant` VALUES (1,'new'),(2,'archive');
/*!40000 ALTER TABLE `status_accountant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_logist`
--

DROP TABLE IF EXISTS `status_logist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_logist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_logist`
--

LOCK TABLES `status_logist` WRITE;
/*!40000 ALTER TABLE `status_logist` DISABLE KEYS */;
INSERT INTO `status_logist` VALUES (1,'new_sale'),(2,'request_car'),(3,'search_car'),(4,'archive_complete'),(5,'delivery'),(6,'payment'),(7,'archive_purchase'),(8,'new_purchase'),(9,'archive_sale'),(10,'out');
/*!40000 ALTER TABLE `status_logist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_purchase`
--

DROP TABLE IF EXISTS `status_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_purchase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_purchase`
--

LOCK TABLES `status_purchase` WRITE;
/*!40000 ALTER TABLE `status_purchase` DISABLE KEYS */;
INSERT INTO `status_purchase` VALUES (1,'new'),(2,'get_answer'),(3,'take'),(4,'search_car'),(5,'way_sender'),(6,'way_recipient'),(7,'in_recipient'),(8,'archive_realized'),(9,'archive_unrealized'),(10,'find');
/*!40000 ALTER TABLE `status_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_sale`
--

DROP TABLE IF EXISTS `status_sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_sale` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_sale`
--

LOCK TABLES `status_sale` WRITE;
/*!40000 ALTER TABLE `status_sale` DISABLE KEYS */;
INSERT INTO `status_sale` VALUES (1,'new'),(2,'get_answer'),(3,'archive_answers'),(4,'archive_complete');
/*!40000 ALTER TABLE `status_sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_load`
--

DROP TABLE IF EXISTS `type_load`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_load` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_load`
--

LOCK TABLES `type_load` WRITE;
/*!40000 ALTER TABLE `type_load` DISABLE KEYS */;
INSERT INTO `type_load` VALUES (1,'Задняя'),(2,'Верхняя'),(3,'Боковая');
/*!40000 ALTER TABLE `type_load` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_bitrix` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_verify_token_unique` (`verify_token`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,34,'Админ',NULL,'admin@test.ru',NULL,'$2y$10$andfPn72ZPAcvuJRVws6HOZW3dsAQ1XDLgi5mOmJR.CBJFPDUcJOC','lCzmKMEKlDjf4g9Ylcg5Ny7uvywqlHIBqi8rNIQcKXOJ6bqZo1xmDsk491Je','2018-09-27 20:53:29','2018-09-27 20:53:29','active','13417436-eccd-408c-9fed-0c98ebf5d299','admin',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-01  3:49:20
