<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home')->middleware(['auth', 'role']);

Route::get('/home', 'HomeController@notRole')->name('no_role')->middleware(['auth']);

Auth::routes();

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

Route::post('/help', 'HomeController@help')->name('help');

// Download Route

Route::get('{dashbord}/{category}/{subcategory}/download/{folder}/{filename}', function($dashbord, $category, $subcategory, $folder, $filename)
{

    $file_path = storage_path()."/app/public/".$folder."/".$filename;
    //exit($file_path);
    if (file_exists($file_path))
    {
        return Response::download($file_path);
    }
    else
    {
        exit('Файла не существует.');
    }
});

Route::get('{dashbord}/{category}/download/{folder}/{filename}', function($dashbord, $category, $folder, $filename)
{

    $file_path = storage_path()."/app/public/".$folder."/".$filename;
    //exit($file_path);
    if (file_exists($file_path))
    {
        return Response::download($file_path);
    }
    else
    {
        exit('Файла не существует.');
    }
});

Route::get('{dashbord}/download/{folder}/{filename}', function($dashbord, $folder, $filename)
{

    $file_path = storage_path()."/app/public/".$folder."/".$filename;
    //exit($file_path);
    if (file_exists($file_path))
    {
        return Response::download($file_path);
    }
    else
    {
        exit('Файла не существует.');
    }
});

//Cabinet

Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'Cabinet',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');

        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
            Route::get('/', 'ProfileController@index')->name('home');
            Route::get('/edit', 'ProfileController@edit')->name('edit');
            Route::put('/update', 'ProfileController@update')->name('update');
        });
    }
);

//Admin

Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::post('/history', 'HomeController@createRecord')->name('history');
        Route::resource('users', 'UsersController');
        Route::get('/sales', 'UsersController@sales')->name('users.sales');
        Route::get('/purchase', 'UsersController@purchase')->name('users.purchase');
        Route::get('/logist', 'UsersController@logist')->name('users.logist');
        Route::get('/accountant', 'UsersController@accountant')->name('users.accountant');
        Route::post('/users/{user}/verify', 'UsersController@verify')->name('users.verify');
    }
);

//Accountant

Route::group(
    [
        'prefix' => 'accountant',
        'as' => 'accountant.',
        'namespace' => 'Accountant',
        'middleware' => ['auth', 'accountant'],
    ],
    function () {
        Route::get('/order', 'HomeController@order')->name('order.index');
        Route::post('/order/num', 'HomeController@orderNumber')->name('order.num');

        Route::get('/arrival', 'HomeController@arrival')->name('arrival.index');
        Route::post('/arrival/check', 'HomeController@checkArrival')->name('arrival.check');

        Route::get('/loading', 'HomeController@loading')->name('loading.index');
        Route::post('/loading/file', 'HomeController@loadFiles')->name('loading.file');

        Route::get('/archive', 'HomeController@archive')->name('archive.index');

        Route::post('/print', 'HomeController@print')->name('print');
    }
);

//Logist

Route::group(
    [
        'prefix' => 'logist',
        'as' => 'logist.',
        'namespace' => 'Logist',
        'middleware' => ['auth', 'logist'],
    ],
    function () {
        Route::group(
            [
                'prefix' => 'not_registry',
                'as' => 'not_registry.',
            ],
            function () {
                Route::group(
                    [
                        'prefix' => 'new',
                        'as' => 'new.',
                    ],
                    function () {
                        Route::get('/sale', 'NotRegistryLogistController@index')->name('sale');
                        Route::get('/purchase', 'NotRegistryLogistController@purchase')->name('purchase');
                        Route::get('/sale_logist', 'NotRegistryLogistForSaleController@index')->name('sale_logist');
                    }
                );
                Route::group(
                    [
                        'prefix' => 'sale',
                        'as' => 'sale.',
                    ],
                    function () {
                        Route::post('/print', 'NotRegistryLogistController@printSale')->name('print');
                    }
                );

                Route::group(
                    [
                        'prefix' => 'purchase',
                        'as' => 'purchase.',
                    ],
                    function () {
                        Route::post('/answer', 'NotRegistryLogistController@giveAnswer')->name('new.answer');
                        Route::match(['get', 'post'], '/request_car', 'NotRegistryLogistController@requestCar')->name('request_car.index');
                        Route::get('/request_car/take/{id_request}/{id_req_accept}', 'NotRegistryLogistController@startSearchCar')->name('request_car.take');
                        Route::post('/request_car/other', 'NotRegistryLogistController@other')->name('request_car.other');

                        Route::match(['get', 'post'], '/search', 'NotRegistryLogistController@search')->name('search.index');
                        Route::post('/search/driver', 'NotRegistryLogistController@findCar')->name('search.driver');

                        Route::match(['get', 'post'], '/delivery', 'NotRegistryLogistController@delivery')->name('delivery.index');
                        Route::post('/delivery/file', 'NotRegistryLogistController@loadFiles')->name('delivery.file');

                        Route::match(['get', 'post'], '/check_delivery', 'NotRegistryLogistController@checkDelivery')->name('check_delivery.index');
                        Route::post('/check_delivery/file', 'NotRegistryLogistController@checkDeliveryFiles')->name('check_delivery.file');

                        Route::match(['get', 'post'], '/payment', 'NotRegistryLogistController@payment')->name('payment.index');
                        Route::get('/payment/do/{id_request}/{id_req_accept}', 'NotRegistryLogistController@getPayment')->name('payment.do');

                        Route::match(['get', 'post'], '/archive_complete', 'NotRegistryLogistController@archiveComplete')->name('archive_complete.index');
                        Route::match(['get', 'post'], '/problem_request', 'NotRegistryLogistController@problemRequest')->name('problem_request.index');

                        Route::post('/print', 'NotRegistryLogistController@printPurchase')->name('print');
                    }
                );

                Route::group(
                    [
                        'prefix' => 'sale_logist',
                        'as' => 'sale_logist.',
                    ],
                    function () {
                        Route::post('/answer', 'NotRegistryLogistForSaleController@giveAnswer')->name('new.answer');
                        Route::match(['get', 'post'], '/request_car', 'NotRegistryLogistForSaleController@requestCar')->name('request_car.index');
                        Route::get('/request_car/take/{id_request}/{id_req_accept}', 'NotRegistryLogistForSaleController@startSearchCar')->name('request_car.take');
                        Route::post('/request_car/other', 'NotRegistryLogistForSaleController@other')->name('request_car.other');

                        Route::match(['get', 'post'], '/search', 'NotRegistryLogistForSaleController@search')->name('search.index');
                        Route::post('/search/driver', 'NotRegistryLogistForSaleController@findCar')->name('search.driver');

                        Route::match(['get', 'post'], '/delivery', 'NotRegistryLogistForSaleController@delivery')->name('delivery.index');
                        Route::post('/delivery/file', 'NotRegistryLogistForSaleController@loadFiles')->name('delivery.file');

                        Route::match(['get', 'post'], '/check_delivery', 'NotRegistryLogistForSaleController@checkDelivery')->name('check_delivery.index');
                        Route::post('/check_delivery/file', 'NotRegistryLogistForSaleController@checkDeliveryFiles')->name('check_delivery.file');

                        Route::match(['get', 'post'], '/payment', 'NotRegistryLogistForSaleController@payment')->name('payment.index');
                        Route::get('/payment/do/{id_request}/{id_req_accept}', 'NotRegistryLogistForSaleController@getPayment')->name('payment.do');

                        Route::match(['get', 'post'], '/archive_complete', 'NotRegistryLogistForSaleController@archiveComplete')->name('archive_complete.index');
                        Route::match(['get', 'post'], '/problem_request', 'NotRegistryLogistForSaleController@problemRequest')->name('problem_request.index');

                        Route::post('/print', 'NotRegistryLogistForSaleController@print')->name('print');
                    }
                );


                Route::group(
                    [
                        'prefix' => 'archive',
                        'as' => 'archive.',
                    ],
                    function () {
                        Route::match(['get', 'post'], '/sale', 'NotRegistryLogistController@archive')->name('sale');
                        Route::match(['get', 'post'], '/purchase', 'NotRegistryLogistController@archivePurchase')->name('purchase');
                        Route::get('/sale_logist', 'NotRegistryLogistForSaleController@archive')->name('sale_logist');
                    }
                );

            }
        );
        Route::group(
            [
                'prefix' => 'statistic',
                'as' => 'statistic.',
            ],
            function () {
                Route::match(['get', 'post'],'/index', 'StatisticLogistController@index')->name('index');
            }
        );
    }
);

//Sale Logist

Route::group(
    [
        'prefix' => 'sale_logist',
        'as' => 'sale_logist.',
        'namespace' => 'SaleLogist',
        'middleware' => ['auth', 'sale_logist'],
    ],
    function () {
        Route::group(
            [
                'prefix' => 'not_registry',
                'as' => 'not_registry.',
            ],
            function () {
                Route::post('/create', 'NotRegistrySaleLogistController@create')->name('new.create');
                Route::match(['get', 'post'], '/new', 'NotRegistrySaleLogistController@index')->name('new.index');

                Route::match(['get', 'post'], '/answer', 'NotRegistrySaleLogistController@answer')->name('answer.index');
                Route::post('/request_take', 'NotRegistrySaleLogistController@requestFind')->name('answer.take');
                Route::post('/answer/other', 'NotRegistrySaleLogistController@other')->name('answer.other');

                Route::match(['get', 'post'], '/take', 'NotRegistrySaleLogistController@take')->name('take.index');
                Route::match(['get', 'post'], '/search', 'NotRegistrySaleLogistController@search')->name('search.index');

                Route::match(['get', 'post'], '/find', 'NotRegistrySaleLogistController@find')->name('find.index');
                Route::post('/request_file', 'NotRegistrySaleLogistController@loadFiles')->name('find.file');

                Route::match(['get', 'post'], '/way_sender', 'NotRegistrySaleLogistController@waySender')->name('way_sender.index');
                Route::post('/put_product', 'NotRegistrySaleLogistController@putProducts')->name('way_sender.product');

                Route::match(['get', 'post'], '/way_recipient', 'NotRegistrySaleLogistController@wayRecipient')->name('way_recipient.index');
                Route::match(['get', 'post'], '/in_recipient', 'NotRegistrySaleLogistController@inRecipient')->name('in_recipient.index');
                Route::match(['get', 'post'], '/archive_realized', 'NotRegistrySaleLogistController@archiveRealized')->name('archive_realized.index');
                Route::match(['get', 'post'], '/archive_unrealized', 'NotRegistrySaleLogistController@archiveUnrealized')->name('archive_unrealized.index');
                Route::match(['get', 'post'], '/problem_request', 'NotRegistrySaleLogistController@problemRequest')->name('problem_request.index');

                Route::post('/print', 'NotRegistrySaleLogistController@print')->name('print');
            }
        );
    }
);

//Sale

Route::group(
    [
        'prefix' => 'sale',
        'as' => 'sale.',
        'namespace' => 'Sale',
        'middleware' => ['auth', 'sale'],
    ],
    function () {
        Route::group(
            [
                'prefix' => 'not_registry',
                'as' => 'not_registry.',
            ],
            function () {
                Route::match(['get', 'post'], '/requests', 'NotRegistrySaleController@requests')->name('requests.index');
                Route::post('/create', 'NotRegistrySaleController@create')->name('new.create');
                Route::get('/new', 'NotRegistrySaleController@index')->name('new.index');
                Route::get('/answer', 'NotRegistrySaleController@answer')->name('answer.index');
                Route::get('/archive_answer', 'NotRegistrySaleController@archiveAnswer')->name('archive_answer.index');
                Route::get('/archive_price', 'NotRegistrySaleController@archivePrice')->name('archive_price.index');
                Route::post('/print', 'NotRegistrySaleController@print')->name('print');
            }
        );
        Route::group(
            [
                'prefix' => 'registry',
                'as' => 'registry.',
            ],
            function () {
                Route::get('/new', 'RegistrySaleController@index')->name('new.index');
                Route::match(['get', 'post'], '/create/{id?}', 'RegistrySaleController@create')->name('new.create');

                Route::get('/answers', 'RegistrySaleController@answers')->name('answers.index');
                Route::post('/close', 'RegistrySaleController@closeRequest')->name('answers.close');
                Route::post('/communicate', 'RegistrySaleController@communicate')->name('answers.communicate');

                Route::get('/archive', 'RegistrySaleController@archive')->name('archive.index');
                Route::post('/check', 'RegistrySaleController@check')->name('check');
                Route::post('/print', 'RegistrySaleController@print')->name('print');
            }
        );
    }
);

//Purchase

Route::group(
    [
        'prefix' => 'purchase',
        'as' => 'purchase.',
        'namespace' => 'Purchase',
        'middleware' => ['auth', 'purchase'],
    ],
    function () {
        Route::group(
            [
                'prefix' => 'not_registry',
                'as' => 'not_registry.',
            ],
            function () {
                Route::post('/create', 'NotRegistryPurchaseController@create')->name('new.create');
                Route::match(['get', 'post'], '/new', 'NotRegistryPurchaseController@index')->name('new.index');

                Route::match(['get', 'post'], '/answer', 'NotRegistryPurchaseController@answer')->name('answer.index');
                Route::post('/request_take', 'NotRegistryPurchaseController@requestFind')->name('answer.take');
                Route::post('/answer/other', 'NotRegistryPurchaseController@other')->name('answer.other');

                Route::match(['get', 'post'], '/take', 'NotRegistryPurchaseController@take')->name('take.index');
                Route::match(['get', 'post'], '/search', 'NotRegistryPurchaseController@search')->name('search.index');

                Route::match(['get', 'post'], '/find', 'NotRegistryPurchaseController@find')->name('find.index');
                Route::post('/request_file', 'NotRegistryPurchaseController@loadFiles')->name('find.file');

                Route::match(['get', 'post'], '/way_sender', 'NotRegistryPurchaseController@waySender')->name('way_sender.index');
                Route::post('/put_product', 'NotRegistryPurchaseController@putProducts')->name('way_sender.product');

                Route::match(['get', 'post'], '/way_recipient', 'NotRegistryPurchaseController@wayRecipient')->name('way_recipient.index');
                Route::match(['get', 'post'], '/in_recipient', 'NotRegistryPurchaseController@inRecipient')->name('in_recipient.index');
                Route::match(['get', 'post'], '/archive_realized', 'NotRegistryPurchaseController@archiveRealized')->name('archive_realized.index');
                Route::match(['get', 'post'], '/archive_unrealized', 'NotRegistryPurchaseController@archiveUnrealized')->name('archive_unrealized.index');
                Route::match(['get', 'post'], '/problem_request', 'NotRegistryPurchaseController@problemRequest')->name('problem_request.index');

                Route::get('autocomplete', 'NotRegistryPurchaseController@autocomplete')->name('autocomplete');

                Route::post('/print', 'NotRegistryPurchaseController@print')->name('print');
            }
        );
        Route::group(
            [
                'prefix' => 'registry',
                'as' => 'registry.',
            ],
            function () {
                Route::get('/new', 'RegistryPurchaseController@index')->name('new.index');
                Route::match(['get', 'post'], '/answer/{id?}', 'RegistryPurchaseController@giveAnswer')->name('new.answer');
                Route::match(['get', 'post'],'/answers', 'RegistryPurchaseController@answers')->name('answers.index');
                Route::post('/communicate', 'RegistryPurchaseController@communicate')->name('answers.communicate');
                Route::match(['get', 'post'], '/archive', 'RegistryPurchaseController@archive')->name('archive.index');
                Route::post('/check', 'RegistryPurchaseController@check')->name('check');
                Route::post('/print', 'RegistryPurchaseController@print')->name('print');
            }
        );
    }
);
